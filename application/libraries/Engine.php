<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Engine {

	private $str="";
	private $params=array();
	private $content="";
	private $filename="";
	private $urisegments="";
	private $namesplit=array();
	private $iscrypt=false;
	private $maincontent="engine-content";
	private $formid="myform";
	private $configurations = array();
	

	public function __construct($parameters){
		$this->params = $parameters;
		$this->ci = &get_instance();
		$this->filename = site_url($this->ci->router->fetch_directory().$this->ci->router->fetch_class());
		$this->urisegments=$this->ci->uri->uri_to_assoc(4);
		$this->configurations = $this->ci->db->get("configurations")->row_array();
		if( isset($this->params['urisegments']) ){
			$this->urisegments = $this->params['urisegments']; 
		}
		$this->iscrypt = $this->ci->config->item("encrypt_url");
		if( isset($this->params["maincontent"]) ){
			$this->maincontent = $this->params["maincontent"] . "-content";
		}
		if( isset($this->params["filename"]) ){
			$this->filename = $this->params["filename"];
		}
		if( isset($this->params["formid"]) ){
			$this->formid = "form_" . $this->params["formid"];
		} else {
			$this->formid = "form_" . $this->ci->router->fetch_class();
		}
	}

	public function mainfunction(){
		// Change Variable [home] and [name]
		$header = $this->ci->config->item("header_container");
		$header = str_replace("[home]", site_url(), $header);
		$header = str_replace("[name]", $this->params['name'], $header);
		if( !isset($this->params['simpleform'])){
			$this->str = $header
		  . $this->params["content"]
			. @$this->params['jsinclude']
			. $this->ci->config->item("footer_container");
		} else {
			$this->str = $this->params["content"];
		}
		$this->str = "<div id='".$this->maincontent."'>".$this->str."</div>";
	}

	public function browse(){
		$p_search = isset($_POST["p_search"]) ? $_POST["p_search"] : "";
		$this->params['content'] = "";
		$_primary="";
		foreach(array_keys($this->params['fieldselect']) as $_cols){
			if(@$this->params['fieldselect'][$_cols]['type']=="primarykey"){
				$_primary=$_cols;
				break;
			}
		}
		$command=explode(",",$this->params['command']);
		$_cmdtext="";
		$_cmdtext.=(in_array("browse",$command)) ? "<a title='Refresh' role='button' href='javascript:loadcontent(\"".$this->maincontent."\",\"".$this->filename."\");' class='btn btn-brand btn-elevate btn-circle btn-icon'><i class='la la-refresh'></i></a>" : '' ;
		$_cmdtext.=(in_array("add",$command)) ? "<a title='Tambah' role='button' href='javascript:loadcontent(\"".$this->maincontent."\",\"".$this->filename."/add/\");' class='btn btn-brand btn-elevate btn-circle btn-icon'><i class='la la-plus'></i></a>" : '' ;
		$_cmdtext.=(in_array("import",$command)) ? "<form method='post' id='importForm'><input type='hidden' name='truncate' id='truncate' class='d-none' value='0'><input type='file' name='importFile' id='importFile' accept='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel, application/wps-office.xlsx' class='d-none' /></form><a title='Import' role='button' href='#' onclick='$(\"#importFile\").click();' class='btn btn-brand btn-elevate btn-circle btn-icon'><i class='la la-upload'></i></a>" : '' ;
		$_cmdtext.=(in_array("generate",$command)) ? "<a title='Generate' role='button' href='javascript:loadcontent(\"".$this->maincontent."\",\"".$this->filename."/generate/\");' class='btn btn-brand btn-elevate btn-circle btn-icon'><i class='la la-cog'></i></a>" : '' ;
		$_cmdtext.=(in_array("search",$command)) ? "<a title='Search' title='search' href='javascript:opensearch();' class='btn btn-brand btn-elevate btn-circle btn-icon btn-search'><i class='la la-search'></i></a>" : '' ;

		if(in_array("import",$command)){
			$this->params['content'] .="
				<script type='text/javascript'>
					$(function(){
						$('#importFile').change(function(){
							bootbox.confirm('Apakah akan mengosongi data terlebih dahulu ?', function(e){
								if(e == true){
									$('#truncate').val('1');
								}
								$('#importForm').submit();
							});
						});

						$('#importForm').on('submit', function(event){
							event.preventDefault();
							$('#".$this->maincontent."').html('<div class=\"text-center\"><img src=\"' + base_url + 'assets/img/spin.gif\" /></div>');
							$.ajax({  
                url:'".$this->filename."/import',  
                method:'POST',  
                data:new FormData(this), 
                contentType:false,  
                processData:false,  
                success:function(data){  
              		bootbox.alert(data, function(){
										loadcontent(\"".$this->maincontent."\",\"".$this->filename."\");
              		});
                } 
           		});  
						});
					});
				</script>		
			";
		}
		$alert = "";
		if(isset($this->params['alert'])){
			$alert = '<div class="alert alert-'.@$this->params["alert"]["type"].'">
				<div class="alert-text">
					'.@$this->params['alert']['message'].'
				</div>
			</div>';
		}

		$search = "";
		if(isset($this->params['search']) && count($this->params['search'])>0){
			$search.="<div class='searchpanel'><form class='form search-form' id='searchform'><div class='row'>";
			$counter=1;
			foreach(array_keys($this->params['search']) as $_search){
				$addon = "";
				if(isset($this->params['search'][$_search]['help'])){
					$addon = "<div class='input-group-append'><span class='input-group-text'>".$this->params['fieldadd'][$_keyadd]['help']."</span></div>";
				}
				$_type=(!isset($this->params['search'][$_search]['type'])) ? "text" : $this->params['search'][$_search]['type'] ;
				$dateclass="";
				$append="";
				$value="";
				if(isset($_POST[$_search])){
					$value = $_POST[$_search];
				}
				else {
					if(isset($this->params['search'][$_search]['value'])){
						$value = $this->params['search'][$_search]['value'];
					}
				}

				if($_type=="dropdownquery"){
					$option="";
					$selected="";
					$optgroup="";
					foreach($this->params['search'][$_search]['sourcequery'] as $dropdown){
						if(isset($opdata['labeldt'])){
							if($optgroup!=$opdata['labeldt']){
								$option.="<optgroup label='".$opdata['labeldt']."'>";
								$optgroup=$opdata['labeldt'];
							}
						}
						$selected = ($value==$dropdown['keydt']) ? "selected" : null;
						$option.="<option ".$selected." value='".$dropdown['keydt']."'>".$dropdown['valuedt']."</option>";
					}
					$search.="<div class='form-group col-md-4 mb-3'>
						<label for='".$_search."'>".$this->ci->lang->line($_search)."</label>
						<div class='".@$this->params['search'][$_search]['class']."'>
							<div class='input-group'>
								<select name='".$_search."' id='".$_search."' class='form-control'>
									".$option."
								</select>
								".$addon."
							</div>
						</div>
					</div>";
				}
				elseif($_type=="dropdownarray"){
					$option="";
					foreach(array_keys($this->params['search'][$_search]['sourcearray']) as $opdata){
						$selected=($opdata==$value) ? "selected" : null ;
						$option.="<option ".$selected." value='".$opdata."'>".$this->params['search'][$_search]['sourcearray'][$opdata]."</option>";
					}
					$search.="<div class='form-group col-md-4 mb-3'>
						<label for='".$_search."'>".$this->ci->lang->line($_search)."</label>
						<div class='".@$this->params['search'][$_search]['class']."'>
							<div class='input-group'>
								<select name='".$_search."' id='".$_search."' class='form-control'>
									".$option."
								</select>
								".$addon."
							</div>
						</div>
					</div>";
				}
				else {
					// $dateclass = "";
					if($_type=="date"){
						$_type="text";
						$dateclass="date-picker";
						$append="<div class='input-group-append'><span class='input-group-text'><i class='la la-calendar'></i></span></div>";
					}
					elseif($_type=="time"){
						$dateclass="time-picker";
						$append="<div class='input-group-append'><span class='input-group-text'><i class='la la-clock-o'></i></span></div>";
					}
					$search.="<div class='form-group col-md-4 mb-3'>
							<label for='".$_search."'>".$this->ci->lang->line($_search)." : </label>
							<div class='".$dateclass." ".@$this->params['search'][$_search]['class']."'>
								<div class='input-group'>
									<input value='".$value."' type='".$_type."' name='".$_search."' id='".$_search."' placeholder='".@$this->params['search'][$_search]['placeholder']."' ".@$this->params['search'][$_search]['is_required']." class='form-control' maxlength='".@$this->params['search'][$_search]['maxlength']."' />".$append."
									".$addon."
								</div>
							</div>
						</div>";
				}

			}
			$search.="<div class='form-group col-md-2' style='margin-top:40px;'><button type='submit' class='btn btn-primary btn-sm'> Cari </button> <button type='button' onclick='loadcontent(\"".$this->maincontent."\",\"".$this->filename."\");' class='btn btn-danger btn-sm'>".$this->ci->lang->line("reset")."</button></div>";
			$search.='</div></form></div>
			<script type="text/javascript">
				$(function(){
					$(".select2 > .input-group > select").select2({
						width: "100%",
						placeholder: $(this).attr("placeholder"),
						allowClear: Boolean($(this).data("allow-clear")),
					});
					$(".date-picker input").datepicker({
						format: "yyyy-mm-dd",
						autoclose: true,
					});
					$("#searchform").submit(function(){
						$("#'.$this->maincontent.'").html("<div class=\"text-center spin-loading\"><img src=\"" + base_url + "assets/img/spin.gif\" /></div>");
						var datapost=$(this).serialize();
						loadpost("'.$this->maincontent.'","'.$this->filename.'",datapost);
						return false;
					});
				});
			</script>
			';

		}

		$this->params['content'] .='
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="kt-font-brand flaticon-list"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						'.$this->params['name'].'
					</h3>
				</div>
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<div class="kt-portlet__head-actions">
							'.$_cmdtext.'
						</div>
					</div>
				</div>
			</div>
			<div class="kt-portlet__body">
    	'.$search.'
    	<div id="alert">'.$alert.'</div>
      <div class="table-responsive">
				<table id="dataTable_'.$this->maincontent.'" class="table table-bordered">';
		$this->params['content'].='<thead><tr>';
		$columns = "";
		foreach(array_keys($this->params['fieldselect']) as $keys){
			if(!@$this->params['fieldselect'][$keys]['hidden']){
				$class = "";
				if(
					@$this->params['fieldselect'][$keys]['type'] == "number"
					|| @$this->params['fieldselect'][$keys]['type'] == "decimal"
				){
					$class = "text-right";
				} else {
					$class = @$this->params['fieldselect'][$keys]['class'];
				}

				if($keys=="SEQ"){
					$this->params['content'].='<th class="center sorting_disabled" style="width:20px;">No</th>';
					$columns.='{ "data": "'.$keys.'", "orderable": false, "searchable": false, "class" : "'.$class.'" },
					';
				}
				elseif($keys=="#"){
					$this->params['content'].='<th class="center sorting_disabled" style="width:20px;">
							<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid kt-checkbox--brand"><input type="checkbox" value="" class="m-group-checkable checkbox-header" id="checkbox-header"><span></span></label>
					</th>';
					$columns.='{ "data": "'.$keys.'", "orderable": false, "searchable": false, "class" : "'.$class.'" },
					';
				}
				else {
					$icon=isset($this->params['fieldselect'][$keys]['icon']) ? '<i class="'.$this->params['fieldselect'][$keys]['icon'].'"></i>' : null ;
					$this->params['content'].='<th class="'.@$this->params['fieldselect'][$keys]['class'].'" style="width:'.@$this->params['fieldselect'][$keys]['width'].';">'.$icon.@$this->ci->lang->line($keys).'</th>';
					$columns.='{ "data": "'.$keys.'", "orderable": true, "class" : "'.$class.'" },
					';
				}
			}
		}

		if((in_array("preview",$command)) ||
			(in_array("edit",$command)) ||
			(in_array("delete",$command)) ||
			(in_array("printreceipt",$command)) ||
			(in_array("detail",$command)) ||
			(in_array("process",$command)))
		{
			$this->params['content'].='<th class="sorting_disabled" style="width: 80px;">Action</th>';
			$columns.='{ "data": "action", "orderable": false, "searchable": false }';
		}

		$this->params['content'].='</tr></thead><tbody>';
		$foot = "";
		if( isset($this->params['sum-column']) ){
			// Get Total
			$sqlTotal = $this->params['query-total'] . $p_search;
			$queryTotal = $this->ci->db->query($sqlTotal)->row();
			$total = $queryTotal->TOTAL;
			$foot = "<tfoot>
            <tr>
                <th colspan='".(count($this->params['fieldselect'])-2)."' style='text-align:right'>Subtotal :</th>
                <th style='text-align:right'></th>
                <th style='text-align:right'></th>
            </tr>
            <tr>
                <th colspan='".(count($this->params['fieldselect'])-2)."' style='text-align:right'>Total : </th>
                <th style='text-align:right'>".number_format($total,0,',','.')."</th>
                <th style='text-align:right'></th>
            </tr>
        </tfoot>";
		}
		$this->params['content'].='</tbody>'.$foot.'</table>';
		
		$_cmdbuttom="";
		$_cmdbuttom.=(in_array("updateall",$command)) ? "<a role='button' href='javascript:updateall(\"".$this->maincontent."\",\"".$this->filename."/updateall/\",$(\".checkbox-data\"));' class='btn btn-brand btn-elevate'><i class='la la-edit'></i> ".(isset($this->params['updateall']['caption']) ? $this->params['updateall']['caption'] : "Update Checked")."</a>" : '' ;
		$_cmdbuttom.=(in_array("deleteall",$command)) ? "<a role='button' href='javascript:deleteall(\"".$this->maincontent."\",\"".$this->filename."/deleteall/\",$(\".checkbox-data\"));' class='btn btn-danger btn-elevate'><i class='la la-trash'></i> ".(isset($this->params['deleteall']['caption']) ? $this->params['deleteall']['caption'] : "Delete Checked")."</a>" : '' ;
		$_cmdbuttom.=(in_array("printall",$command)) ? "<a role='button' href='javascript:printall(\"".$this->maincontent."\",\"".$this->filename."/printall/\",$(\".checkbox-data\"));' class='btn btn-brand btn-elevate'><i class='la la-print'></i> Print Checked</a>" : '' ;
		$this->params['content'].= '<div class="btn btn-group">'.$_cmdbuttom.'</div>';

		$export = "";
		$footer = "";
		if( in_array("export",$command) ){
			$export = '"dom": "Bflrtip",
		        "buttons": [
            	"copy", "csv", "excel", "pdf", "print", "colvis"
        		]';
		}
		if( isset($this->params['sum-column']) ){
			$footer = '"footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === "string" ?
                    i.replace(/[\$.]/g, "")*1 :
                    typeof i === "number" ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( '.$this->params['sum-column'].' )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( '.$this->params['sum-column'].', { page: "current"} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( '.(count($this->params['fieldselect'])-2).' ).footer() ).html(
                pageTotal.toLocaleString("id-ID") //+ " ( Total : " + total.toLocaleString("id-ID") + ")"
            );
        }';
		}
		$this->params['content'].='</div>
		</div>

		<script type="text/javascript">
			$(function(){
		    $("#dataTable_'.$this->maincontent.'").DataTable( {
		        "processing": true,
		        "responsive": true,
		        "serverSide": true,
		        "ajax": {
		        	"url" : "'.site_url( $this->ci->router->fetch_directory() . $this->ci->router->fetch_class() ).'/getData/",
		        	"type" : "POST",
		        	"data" : {
		        		filter : "'.$p_search.'"
		        	},
		        	"error": function (xhr, error, thrown) {
	              bootbox.alert(xhr.responseText);
	            }
		        },
		        "columns": [
		            '.$columns.'
		        ],
		        "oLanguage": {
    					"sSearch": "Cari: ",
    					"sSearchPlaceholder": "Ketik untuk mencari",
	    				"sLengthMenu": "Tampilkan _MENU_ data",
	    				"sProcessing": "<img style=\"width: 50px;\" src=\"'.base_url().'assets/img/spin.gif\" /><br />Memuat Data ...."
						},
						"autoWidth": false,
						'.$export.'
						'.$footer.'
		    });

				$("#checkbox-header").on("click",function(){
					if($("#checkbox-header").is(":checked")){
						$(".checkbox-data").prop("checked",true);
					}
					else{
						$(".checkbox-data").prop("checked",false);
					}
				});
			});
		</script>
		';
		$this->mainfunction();
		return $this->str;
		// echo $this->str;
	}

	public function getData(){
		$months = array (
	    1 => 'Januari',
	    2 => 'Februari',
	    3 => 'Maret',
	    4 => 'April',
	    5 => 'Mei',
	    6 => 'Juni',
	    7 => 'Juli',
	    8 => 'Agustus',
	    9 => 'September',
	    10 => 'Oktober',
	    11 => 'November',
	    12 => 'Desember'
		);
		$sql = "";
		$filter = @$_POST["filter"];
		if( !isset($this->params['sql']) ){
			
			$field = "";
			foreach(array_keys($this->params['fieldselect']) as $keys){
				if( $keys != "SEQ" && $keys != "#"){
					$field .= $keys . ",";
				}
			}

			$field = substr($field,0, strlen($field)-1);
			$sql = "SELECT ".$field." FROM ".$this->params['table'] . " " . $filter;
		} else {
			$sql = "SELECT * FROM ( ".$this->params['sql']. " " . $filter ." ) as A";
		}
		$count_all = $this->ci->db->query( $sql )->num_rows();
		$post = $this->ci->input->post();
		// Add Filter data
		$value = @$post['search']['value'];
		$_primary = "";
		foreach(array_keys($this->params['fieldselect']) as $_cols){
			if(@$this->params['fieldselect'][$_cols]['type']=="primarykey"){
				$_primary=$_cols;
				break;
			}
		}
		if($value != ""){
			$sql .= " WHERE ";
			foreach(array_keys($this->params['fieldselect']) as $s_keys){
				if( $s_keys != 'SEQ' && $s_keys != '#' ){
					$sql .= $s_keys . " LIKE '%".$this->ci->db->escape_like_str($value)."%' OR ";
				}
			}
			$sql = substr($sql,0, (strlen($sql) - 3) );
			$count_all = $this->ci->db->query( $sql )->num_rows();
		}

		// Order Data
		if( @$post['order'][0]['column'] ){
			// Get Column Name
			$fieldselect = array();
			foreach($this->params['fieldselect'] as $key => $value){
				$fieldselect[$key] = $this->params['fieldselect'][$key];
			}
			$orderColumn = array_keys(array_slice($fieldselect, $post['order'][0]['column'], 1));
			$ordercol = $orderColumn[0];
			$sql .= " ORDER BY ".$ordercol. " " . $post['order'][0]['dir'] . " ";
		} else {
			if( isset($this->params['order']) ){
				$sql .= " ORDER BY " . $this->params['order'];
			}
		}
		// else {
		// 	$sql .= " ORDER BY ".$_primary." ASC";
		// }
		// Add Limit Data
		if( !isset($post['start']) || !isset($post['length']) ){
			$post['start'] = 1;
			$post['length'] = 20;
			$post['draw'] = TRUE;
		}
		
		$sql .= " LIMIT " . $post['start'] . "," . $post['length'];
		$query = $this->ci->db->query( $sql )->result_array();
		$count_filtered = $count_all;

		$seq = $post['start'];
		$datalist = array();
		$command=explode(",",$this->params['command']);

		// Start the Loop
		foreach($query as $data){
			$seq++;
			$mylist = array();
			$sum = array();
			foreach(array_keys($this->params['fieldselect']) as $keys){
				if($keys=="SEQ"){
					$mylist[$keys] = number_format($seq,0) . ".";
				} elseif($keys=="#"){
					$mylist[$keys] = '<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid kt-checkbox--brand"><input type="checkbox" value="" class="m-checkable checkbox-data" id="check-'.$_primary."-".$data[$_primary].'"><span></span></label>';
				
				}
				elseif(!@$this->params['fieldselect'][$keys]['hidden']){
					$_value=@$data[$keys];
					switch(@$this->params['fieldselect'][$keys]['type']){
						case "date":
							if( @$data[$keys] != "" ){
								$_value=date("d",strtotime(@$data[$keys])) . " " . $months[ date("n",strtotime(@$data[$keys])) ] . " " . date("Y",strtotime(@$data[$keys]));
							}
							break;
						case "datetime":
							if($_value != ""){
								$_value=date("d M Y - H:i:s",strtotime(@$data[$keys]));
							}
							break;
						case "number":
							$_value=number_format(@$data[$keys],0,",",".");
							break;
						case "decimal":
							$_value=number_format(@$data[$keys],2,",",".");
							break;
						case "dropdownarray":
							$_value=@$this->params['fieldselect'][$keys]['sourcearray'][$data[$keys]];
							break;
						case "image":
							if(@$data[$keys] == "") $data[$keys] = "noimage.png";
							$_value="<img src='".base_url()."uploads/".@$data[$keys]."' class='thumbnail' style='width:100%;' />";
							break;
						case "download":
							if(@$data[$keys] != "" && file_exists("./uploads/".$data[$keys]) != "") {
								$_value="<a href='".base_url()."uploads/".@$data[$keys]."' target='_blank' class='btn btn-sm btn-block btn-primary' title='download'><i class='la la-download'></i> Download</a>";
							} else {
								$_value = "<span class='btn btn-danger'>No File Available</span>";
							}
							break;
					}
					// $_value = nl2br($_value);
					$mylist[$keys] = @$this->params['fieldselect'][$keys]['ltag'].$_value.@$this->params['fieldselect'][$keys]['rtag'];
				}

				$urlparams = "pk/".$_primary."/valpk/".urlencode($data[$_primary]);
				if( $this->iscrypt == TRUE ){
					// $urlparams = $this->bcrypt->encode($urlparams);
				}

				$_cmddetail="";
				$_cmddetail.=(in_array("preview",$command)) ? "<a class='btn btn-sm btn-clean btn-icon btn-icon-md' data-rel='tooltip' href='".$this->params['previewurl'] . $data[$this->params['previewfield']] ."' target='blank'><i class='la la-search'></i></a>" : '' ;
				$_cmddetail.=(in_array("edit",$command)) ? "<a class='btn btn-sm btn-clean btn-icon btn-icon-md' data-rel='tooltip' href='javascript:loadcontent(\"".$this->maincontent."\",\"".$this->filename."/edit/".$urlparams."\");' title='Edit'><i class='la la-edit'></i></a>" : '' ;
				$_cmddetail.=(in_array("printreceipt",$command)) ? "<a class='btn btn-sm btn-clean btn-icon btn-icon-md' data-rel='tooltip' href='javascript:loadmodal(\"".$this->filename."/printreceipt/pk/".$_primary."/valpk/".urlencode($data[$_primary])."\", \"print\");' title='Cetak'><i class='la la-print'></i></a>" : '' ;
				$_cmddetail.=(in_array("detail",$command)) ? "<a class='btn btn-sm btn-clean btn-icon btn-icon-md' data-rel='tooltip' href='javascript:loadcontent(\"".$this->maincontent."\",\"".$this->filename."/browse_detail/pk/".$_primary."/valpk/".urlencode($data[$_primary])."\");' title='Detail'><i class='la la-list'></i></a>" : '' ;
				$_cmddetail.=(in_array("process",$command)) ? "<a class='btn btn-sm btn-clean btn-icon btn-icon-md' data-rel='tooltip' href='javascript:loadcontent(\"".$this->maincontent."\",\"".$this->filename."/process/pk/".$_primary."/valpk/".urlencode($data[$_primary])."\");' title='Proses'><i class='la la-gear'></i></a>" : '' ;
				$_cmddetail.=(in_array("delete",$command)) ? "<a class='btn btn-sm btn-clean btn-icon btn-icon-md' data-rel='tooltip' href='javascript:deletedata(\"".$this->maincontent."\",\"".$this->filename."/delete/".$urlparams."\");' title='Delete'><i class='la la-trash'></i></a>" : '' ;

				if( 
						(in_array("preview",$command)) || 
						(in_array("edit",$command)) || 
						(in_array("delete",$command)) || 
						(in_array("printreceipt",$command)) || 
						(in_array("detail",$command)) || 
						(in_array("process",$command))
					){
					$mylist['action'] = '<div class="btn-group" role="group">'.$_cmddetail.'</div>';
					// $mylist['action'] = "";
				}
			}
			$datalist[] = (object) $mylist;
			// print_r($datalist);exit;
		}

		$output = (object) array(
			"draw" => $post['draw'],
			"recordsTotal" => $count_all,
			"recordsFiltered" => $count_filtered,
			"data" => $datalist
			// "error" => $sql
		);
		return json_encode($output);
	}

	public function add(){
		$post = $this->ci->input->post();
		$command=explode(",",$this->params['command']);
		$_cmdtext="";
		$_cmdtext.=(in_array("browse",$command)) ? "<a title='Refresh' href='javascript:loadcontent(\"".$this->maincontent."\",\"".$this->filename."/browse\");' class='btn btn-brand btn-elevate btn-circle btn-icon'><i class='la la-refresh'></i></a>" : '' ;
		$alert = "";
		$valid = true;

		$primary = "";
		foreach($this->params['fieldadd'] as $key => $value){
			if(@$value['type'] == "primarykey" ){
				$primary = $key;
			}
		}

		if(isset($this->params['alert'])){
			$alert = '<div class="alert alert-'.@$this->params["alert"]["type"].'">
				<div class="alert-text">
					'.@$this->params['alert']['message'].'
				</div>
			</div>';
		}

		$this->params['content']='
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="kt-font-brand flaticon-plus"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						Add '.$this->params['name'].'
					</h3>
				</div>
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<div class="kt-portlet__head-actions">
							'.$_cmdtext.'
						</div>
					</div>
				</div>
			</div>
			<div class="kt-portlet__body">';
		$this->params['content'].= "<div id='alert'>" . $alert . "</div>";
		if( isset($this->params['view']) ){
			$this->params['data'] = (isset($this->params['data'])) ? $this->params['data'] : array();
			$this->params['content'].= $this->ci->load->view($this->params['view'], $this->params['data'], TRUE);
		} else {
			$this->params['content'].= "<form class='form-horizontal' role='form' id='".$this->formid."' enctype='multipart/formdata'>";
			foreach(array_keys($this->params['fieldadd']) as $_keyadd){
				$addon = "";
				$rsign = "";
				$disabled = "";
				$validation_message = "";
				if(isset($this->params['fieldadd'][$_keyadd]['help'])){
					$addon = "<div class='input-group-append'><span class='input-group-text'>".$this->params['fieldadd'][$_keyadd]['help']."</span></div>";
				}
				$isreadonly=(@$this->params['fieldadd'][$_keyadd]['readonly']==true) ? "readonly" : null;
				$isdisabled=(@$this->params['fieldadd'][$_keyadd]['disabled']==true) ? "disabled" : null;

				// Form Validation
				if( count($post) > 0){
					$this->params['fieldadd'][$_keyadd]['value'] = set_value($_keyadd);
				}
				if( isset($this->params['fieldadd'][$_keyadd]['validation']) ){
					$this->ci->form_validation->set_rules( $_keyadd, $this->ci->lang->line($_keyadd), $this->params['fieldadd'][$_keyadd]['validation'] );
					if(@$this->params['fieldadd'][$_keyadd]['type'] == "primarykey"){
						$this->ci->form_validation->set_rules( $_keyadd, $this->ci->lang->line($_keyadd), "is_unique[".$this->params['table'].".".$_keyadd."]" );
					}
					$arr_validation = explode("|", $this->params['fieldadd'][$_keyadd]['validation']);
					if( in_array("required", $arr_validation)) {
						$this->params['fieldadd'][$_keyadd]['is_required'] = "required";
					}
					if( !$this->ci->form_validation->run() ){ // Validation Error
						$validation_message = form_error($_keyadd);
						$valid = false;
					}
				}

				if(@$this->params['fieldadd'][$_keyadd]['is_required'] == true){
					$rsign = "<span class='requiredsign'>*</span>";
				}

				if(@$this->params['fieldadd'][$_keyadd]['hidden']==true){
					$this->params['content'].="<input type='hidden' value='".@$this->params['fieldadd'][$_keyadd]['value']."' name='".$_keyadd."' id='".$_keyadd."'/>";
				}
				else {
					$_type=(!isset($this->params['fieldadd'][$_keyadd]['type'])) ? "text" : $this->params['fieldadd'][$_keyadd]['type'] ;
					if($_type=="primarykey"){
						$_type="text";
					}

					if($_type=="textarea"){
						$this->params['content'].="
						<div class='form-group row'>
							<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd) . $rsign ."</label>
							<div class='".@$this->params['fieldadd'][$_keyadd]['class']."'>
								<div class='input-group'>
									<textarea ".$isreadonly." ".$isdisabled." name='".$_keyadd."' id='".$_keyadd."' placeholder='".@$this->params['fieldadd'][$_keyadd]['placeholder']."' ".@$this->params['fieldadd'][$_keyadd]['is_required']." class='form-control' maxlength='".@$this->params['fieldadd'][$_keyadd]['maxlength']."'>".@$this->params['fieldadd'][$_keyadd]['value']."</textarea>
									".$addon."
								</div>
								".$validation_message."
							</div>
						</div>";
					}
					elseif( $_type == "separator"){
						$this->params['content'].="
							<div class='form-separator'>".$this->ci->lang->line($_keyadd)."</div>
						";
					}
					elseif($_type=="wysiwyg"){
						$this->params['content'].="
						<div class='form-group row'>
							<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd) . $rsign ."</label>
							<div class='".@$this->params['fieldadd'][$_keyadd]['class']."'>
								<div class='input-group'>
									<textarea ".$isreadonly." ".$isdisabled." name='".$_keyadd."' id='".$_keyadd."' placeholder='".@$this->params['fieldadd'][$_keyadd]['placeholder']."' ".@$this->params['fieldadd'][$_keyadd]['is_required']." class='form-control' maxlength='".@$this->params['fieldadd'][$_keyadd]['maxlength']."' novalidate>".@$this->params['fieldadd'][$_keyadd]['value']."</textarea>
									".$addon."
								</div>
								".$validation_message."
							</div>
						</div>
						<script type='text/javascript'>
							$(document).ready(function(){
								var editor_id = '".$_keyadd."';
								tinymce.EditorManager.execCommand('mceRemoveEditor',true, editor_id);
								tinymce.EditorManager.execCommand('mceAddEditor',true, editor_id);
							});
						</script>";
					}
					elseif($_type=="dropdownquery"){
						$option="";
						$optgroup="";
						$multiple = "";
						$addButton = "";
						foreach($this->params['fieldadd'][$_keyadd]['sourcequery'] as $opdata){
							if(isset($opdata['labeldt'])){
								if($optgroup!=$opdata['labeldt']){
									$option.="<optgroup label='".$opdata['labeldt']."'>";
									$optgroup=$opdata['labeldt'];
								}
							}
							$selected = ($opdata['keydt']==@$this->params['fieldadd'][$_keyadd]['value']) ? "selected" : null ;
							$option.="<option ".$selected." value='".$opdata['keydt']."'>".$opdata['valuedt']."</option>";
						}
						$name = "name='".$_keyadd."'";
						if( @$this->params['fieldadd'][$_keyadd]['multiple'] ){
							$multiple = "multiple='multiple'";
							$name = "name='".$_keyadd."[]'";
						}

						if( @$this->params['fieldadd'][$_keyadd]['allowadd'] == true || @$this->params['fieldadd'][$_keyadd]['allowedit'] == true){
							$addButton = "<div class='input-group-append'>";
							$titleType = "add";
							if( @$this->params['fieldadd'][$_keyadd]['allowadd'] == true ){
								$titleType = "add";
								$addButton .= "
									<button class='btn btn-primary' type='button' id='add_".$_keyadd."'  data-toggle='modal' data-target='#modal_".$_keyadd."'><i class='la la-plus'></i> </button>
									<script type='text/javascript'>
										$(function(){
											$('#add_".$_keyadd."').click(function(){
												$('#modal_body_".$_keyadd."').load('".$this->params['fieldadd'][$_keyadd]['addurl']."');
											});
										});
						      </script>
									";
							}
							if( @$this->params['fieldadd'][$_keyadd]['allowedit'] == true ){
								$titleType = "edit";
								$addButton .= "
									<button class='btn btn-primary' type='button' id='edit_".$_keyadd."'  data-toggle='modal' data-target='#modal_".$_keyadd."'><i class='la la-edit'></i> </button>
									<script type='text/javascript'>
										$(function(){
											$('#edit_".$_keyadd."').click(function(){
												$('#modal_body_".$_keyadd."').load('".$this->params['fieldadd'][$_keyadd]['editurl']."/edit/pk/".$_keyadd."ID/valpk/' + $('#".$_keyadd."').val());
											});
										});
						      </script>
									";
							}
							$addButton .= "</div>
							<!-- Modal -->
							<div class='modal fade' id='modal_".$_keyadd."' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
							  <div class='modal-dialog modal-lg' role='document'>
							    <div class='modal-content'>
							      <div class='modal-header'>
							        <h5 class='modal-title' id='exampleModalLabel'>".$this->ci->lang->line($titleType)." ".$this->ci->lang->line($_keyadd)."</h5>
							        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
							          <span aria-hidden='true'>&times;</span>
							        </button>
							      </div>
							      <div class='modal-body' id='modal_body_".$_keyadd."'>
							        
							      </div>
							      <div class='modal-footer'>
							        <button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>
							      </div>
							    </div>
							  </div>
							</div>
							";
						}

						$this->params['content'].="
						<div class='form-group row'>
							<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd). $rsign . "</label>
							<div class='".@$this->params['fieldadd'][$_keyadd]['class']."'>
								<div class='input-group'>
									<select  ".$isreadonly." ".$isdisabled." ".$multiple." ".$name." id='".$_keyadd."' class='form-control' ".@$this->params['fieldadd'][$_keyadd]['is_required'].">
										".$option."
									</select>
									".$addon." ".$addButton."
								</div>
								".$validation_message."
							</div>
						</div>";
						// if( $multiple != "" ){
						// 	$this->params['content'] .= "
						// 		<script type='text/javascript'>
						// 		$(function() {
						//         $('#".$_keyadd."').multipleSelect({
						//             width: '100%'
						//         });
						//     });
						// 	</script>";
						// }
					}
					elseif($_type=="dropdownarray"){
						$option="";
						$multiple="";
						foreach(array_keys($this->params['fieldadd'][$_keyadd]['sourcearray']) as $opdata){
							$selected=($opdata==@$this->params['fieldadd'][$_keyadd]['value']) ? "selected" : null ;
							$option.="<option ".$selected." value='".$opdata."'>".$this->params['fieldadd'][$_keyadd]['sourcearray'][$opdata]."</option>";
						}
						$name = "name='".$_keyadd."'";
						if( @$this->params['fieldadd'][$_keyadd]['multiple'] ){
							$multiple = "multiple='multiple'";
							$name = "name='".$_keyadd."[]'";
						}
						$this->params['content'].="
						<div class='form-group row'>
							<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd) . $rsign ."</label>
							<div class='".@$this->params['fieldadd'][$_keyadd]['class']."'>
								<div class='input-group'>
									<select  ".$isreadonly." ".$isdisabled." ".$multiple." ".$name." id='".$_keyadd."' class='form-control' ".@$this->params['fieldadd'][$_keyadd]['is_required'].">
										".$option."
									</select>
									".$addon."
								</div>
								".$validation_message."
							</div>
						</div>";
						/*if( $multiple != "" ){
							$this->params['content'] .= "
								<script type='text/javascript'>
								$(function() {
						        $('#".$_keyadd."').multipleSelect({
						            width: '100%'
						        });
						    });
							</script>";
						}*/
					}
					elseif($_type=="checkgroup"){
						$option="";
						foreach($this->params['fieldadd'][$_keyadd]['sourcequery'] as $opdata){
							$selected=($opdata['keydt']==@$this->params['fieldadd'][$_keyadd]['value']) ? "checked" : null ;
							$option.="<label><input class='form-control' type='checkbox' name='CHECK_".$opdata['keydt']."' ".$selected.">
							<span class='lbl' style='margin-left:5px;'> ".$opdata['valuedt']." </span></label>";
						}
						$this->params['content'].="
						<div class='form-group row'>
							<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($this->ci->lang->line($_keyadd)) . $rsign ."</label>
							<div class='".@$this->params['fieldadd'][$_keyadd]['class']."'>
								<div class='input-group'>
									".$option."
									".$addon."
								</div>
								".$validation_message."
							<?div>
						</div>";
					}
					elseif($_type=="file"){
						$img = "<img src='' class='thumbnail' style='width:100%;' />";
						$this->params['content'].="<div class='form-group row'>
							<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd). $rsign . "</label>
							<div class='".@$this->params['fieldadd'][$_keyadd]['class']."'>
								<div class='input-group'>
									<input class='custom-file-input' ".@$this->params['fieldadd'][$_keyadd]['required']." type='file' accept='application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf, image/*, video/*' id='FILE_".$_keyadd."' name='FILE_".$_keyadd."' value='' />".$addon."
									<label class='custom-file-label' for='FILE_".$_keyadd."'></label>
									<input type='hidden' id='".$_keyadd."' name='".$_keyadd."' value='' />
								</div>
								".$validation_message."
								<div class='img-container' id='IMG_".$_keyadd."'>".$img."</div>
							</div>
							</div>
							<script type='text/javascript'>
								$('#FILE_".$_keyadd."').on('change', function(){
									// $('#IMG_".$_keyadd."').remove();

									var reader = new FileReader();
							    reader.onload = function (e) {
							    	if( e.target.result.substring(5,14) == 'video/mp4' || e.target.result.substring(5,14) == 'video/avi' ){
							    		var loaded = '<video width=\"100%\" height=\"400px\" controls><source src=\"' + e.target.result + '\" type=\"' + e.target.result.substring(5,14) + '\">Browser Anda tidak support dengan pemutar video.</video>';
							    		$('#IMG_".$_keyadd."').html( loaded );
							    	} else if(e.target.result.substring(5,20) == 'application/pdf') {
							    		var loaded = '<object data=\"' + e.target.result + '\" type=\"application/pdf\" width=\"100%\" height=\"600px\"><p><b>Gagal Memuat PDF</b>: Browser Ini tidak mendukung penampil PDF.</p></object>';
											$('#IMG_".$_keyadd."').html( loaded );
							    	} else {
						    			$('#IMG_".$_keyadd."').html('<img class=\"thumbnail\" src=\"' + e.target.result + '\" style=\"width:100%;\" />');
							    	}
							    };

							    // read the image file as a data URL.
							    reader.readAsDataURL(this.files[0]);

									ajaxupload('".$_keyadd."');
									return false;
								});
							</script>
							";
					}
					elseif($_type == "map"){
						$default_position = $this->configurations['MAP_DEFAULT_LOCATION'];
						$latlng = explode(",", $default_position);
						$default_lat = $latlng[0];
						$default_lng = $latlng[1];
						$addressChain = "";
						if( isset($this->params['fieldadd'][$_keyadd]['address_id'] )){
							$id = $this->params['fieldadd'][$_keyadd]['address_id'];
							$addressChain = "
							var input = document.getElementById('".$id."');
							var autocomplete = new google.maps.places.Autocomplete(input);
							autocomplete.bindTo('bounds', map);
							autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
							
							/*var infowindow = new google.maps.InfoWindow();
			        var infowindowContent = document.getElementById('infowindow-content');
			        infowindow.setContent(infowindowContent);
			        var marker = new google.maps.Marker({
			          map: map,
			          anchorPoint: new google.maps.Point(0, -29)
			        });*/

			        autocomplete.addListener('place_changed', function() {
		          
		          /*infowindow.close();*/

			          marker.setVisible(false);
			          var place = autocomplete.getPlace();
			          if (!place.geometry) {
			            // User entered the name of a Place that was not suggested and
			            // pressed the Enter key, or the Place Details request failed.
			            window.alert('No details available for input: ' + place.name);
			            return;
			          }

		          // If the place has a geometry, then present it on a map.

		          if (place.geometry.viewport) {
		            map.fitBounds(place.geometry.viewport);
		          } else {
		            map.setCenter(place.geometry.location);
		            map.setZoom(17);  // Why 17? Because it looks good.
		          }

		          marker.setPosition(place.geometry.location);
		          marker.setVisible(true);
		          var lat = place.geometry.location.lat();
		          var lng = place.geometry.location.lng();
							$('#".$_keyadd."').val(eval(lat) + ',' + eval(lng));

		          var address = '';
		          if (place.address_components) {
		            address = [
		              (place.address_components[0] && place.address_components[0].short_name || ''),
		              (place.address_components[1] && place.address_components[1].short_name || ''),
		              (place.address_components[2] && place.address_components[2].short_name || '')
		            ].join(' ');
		          }

		          /*
		          infowindowContent.children['place-icon'].src = place.icon;
		          infowindowContent.children['place-name'].textContent = place.name;
		          infowindowContent.children['place-address'].textContent = address;
		          infowindow.open(map, marker);
		          */
		        });
							";
						}
						$this->params['content'].="<div class='form-group row'>
							<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd). $rsign . "</label>
							<div class='".@$this->params['fieldadd'][$_keyadd]['class']."'>
								<div class='input-group'>
									<input type='text' value='".$default_lat.",".$default_lng."' class='form-control' id='".$_keyadd."' name='".$_keyadd."' value='' />
								</div>
								".$validation_message."
								<div class='map-container' id='MAP_".$_keyadd."' style='height:300px;'></div>
								<p class='text-primary mt-2 text-center'>Geser Titik Merah untuk Mendapatkan lokasi Koordinat</p>
							</div>
							</div>
							<script type='text/javascript'>
								var map, infoWindow;
								".$_keyadd."_initMap();
								function ".$_keyadd."_initMap() {
									var location = { lat: ".$default_lat.", lng: ".$default_lng." };
							    var map = new google.maps.Map(document.getElementById('MAP_".$_keyadd."'), {
							      zoom: 18,
							      center: location
							    });

							    ".$addressChain."

							    // Add a marker at the center of the map.
							    var marker = new google.maps.Marker({
							      position: location,
							      title: 'Pilih Lokasi',
							      map: map,
							      draggable: true,
							   		animation: google.maps.Animation.DROP
							    });
							    google.maps.event.addListener(marker, 'dragend', function (event) {
							    	var lat = this.getPosition().lat();
							    	var lng = this.getPosition().lng();
							    	map.setCenter({lat: eval(lat),lng: eval(lng)});
								    $('#".$_keyadd."').val(eval(lat) + ',' + eval(lng));
									});
							  }
							</script>";
					}
					else {
						$step = "";
						$dateclass="";
						$append="";
						$js="";
						$min= isset($this->params['fieldadd'][$_keyadd]['min']) ? " min='".$this->params['fieldadd'][$_keyadd]['min']."' " : null;
						$max= isset($this->params['fieldadd'][$_keyadd]['max']) ? " max='".$this->params['fieldadd'][$_keyadd]['max']."' " : null;
						$currencyAdd = "";
						$currencyClass = "";
						if($_type=="date"){
							$_type="text";
							$dateclass="date-picker";
							$append="<div class='input-group-append'><span class='input-group-text'><i class='la la-calendar'></i></span></div>";
							// if(count($post) > 0){
							// 	if($post[$_keyadd] == ""){
							// 		unset($post[$_keyadd]);
							// 	}
							// }
						}
						elseif($_type=="time"){
							$_type="text";
							$dateclass="time-picker";
							$append="<div class='input-group-append'><span class='input-group-text'><i class='la la-clock-o'></i></span></div>
								<script type='text/javascript'>
									$('#".$_keyadd.", #".$_keyadd."_modal').timepicker({
					            minuteStep: 1,
					            defaultTime: '',
					            showSeconds: true,
					            showMeridian: false,
					            snapToStep: true
					        });
								</script>
							";
						}
						elseif($_type=="datetime"){
							$_type="text";
							$dateclass="datetime-picker";
							$append="<div class='input-group-append'><span class='input-group-text'><i class='la la-calendar'></i></span></div>";
						}
						elseif($_type=="decimal"){
							$_type="number";
							$step = "step='0.01'";
						}
						elseif($_type=="currency"){
							$_type="number";
							$step = "step='0.01'";
							$currencyAdd = " data-number-to-fixed='2' data-number-stepfactor='100'";
							$currencyClass = " currency";
						}
						elseif($_type=="tagsinput"){
							$js = "<script type='text/javascript'>
								$('#".$_keyadd."').tagsinput();
							</script>";
							$_type = "text";
						}
						$this->params['content'].="<div class='form-group row'>
							<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd) . $rsign ."</label>
							<div class='".$dateclass." ".@$this->params['fieldadd'][$_keyadd]['class']."'>
								<div class='input-group'>
									<input ".$min." ".$max." ".$isreadonly." ".$isdisabled." value='".@$this->params['fieldadd'][$_keyadd]['value']."' type='".$_type."' ".$currencyAdd." ".$step." name='".$_keyadd."' id='".$_keyadd."' placeholder='".@$this->params['fieldadd'][$_keyadd]['placeholder']."' ".@$this->params['fieldadd'][$_keyadd]['is_required']." ".$isreadonly." class='form-control ".$currencyClass."' maxlength='".@$this->params['fieldadd'][$_keyadd]['maxlength']."' />".$append."
									".$addon."
								</div>
								".$validation_message."
							</div>
						</div>" . $js;
					}
				}
			}
			if( !isset($this->params['showcontrol']) || $this->params['showcontrol'] != false ){
				$this->params['content'].="<div class='form-group row'>
					<div class='offset-md-2 col-md-6'>
						<button class='btn btn-pill btn-primary' type='submit'><i class='la la-save'></i>".$this->ci->lang->line("submit")."</button>
						<button class='btn btn-pill btn-danger' type='reset'><i class='la la-refresh'></i>".$this->ci->lang->line("reset")."</button>
					</div>
				</div>";
			}
			$this->params['content'].="</form>";
			$this->params['content'].='</div>';
			

		}
		$this->params['content'].='<script type="text/javascript">
			$(document).ready(function(){
				$(this).updatePolyfill();
				webshim.activeLang("id");
				webshims.setOptions("forms-ext", {
				    replaceUI: "auto",
				    types: "number"
				});
				webshims.polyfill("forms forms-ext");

				$(".select2 > .input-group > select").select2({
					placeholder: $(this).attr("placeholder"),
					allowClear: Boolean($(this).data("allow-clear")),
				});
				$(".date-picker input").datepicker({
					format: "yyyy-mm-dd",
					autoclose: true,
				});
				$(".datetime-picker input").datetimepicker();

				$("#'.$this->formid.'").submit(function(){
					$("#'.$this->maincontent.'").html("<div class=\"text-center spin-loading\"><img src=\"" + base_url + "assets/img/spin.gif\" /></div>");
  				var datapost=$(this).serialize();
					var target="'.$this->filename.'/add/";
					loadpost("'.$this->maincontent.'", target, datapost);
					return false;
				});
			});
		</script>
		';
		// Inserting Data POst
		if( count($post) > 0 && $valid == true){
			foreach($this->params['fieldadd'] as $_keyadd => $_params){
				if(@$_params['type'] == "separator"){
					unset($post[$_keyadd]);
				}
				if(@$_params['disabled'] == true){
					unset($post[$_keyadd]);
				}
			}
			if( isset($this->params['unset_post']) ){
				$this->params['unset_post'] = explode(",", $this->params['unset_post']);
				foreach( $this->params['unset_post'] as $unset){
					unset($post[$unset]);
				}
			}
			$ins = $this->ci->db->insert($this->params['table'],$post);
			if( $ins ){
				$this->params['content'] = '<script type="text/javascript">
					loadcontent("'.$this->maincontent.'","'.$this->filename.'/add/", true);
				</script>';
				echo "Library : " . $this->ci->db->insert_id() . "<br />";
			}
			else {
				$error = $this->ci->db->error();
				$this->params['content'] .= '<script type="text/javascript">
					bootbox.alert("Gagal Insert karena : <br /><pre>'.$error['message'].'</pre>" );
				</script>';
			}
		}
		$this->mainfunction($this->params);
		return $this->str;
	}

	public function edit(){
		$post = $this->ci->input->post();
		if(isset($this->params['urisegments']['pk'])){
			$this->urisegments['pk'] = $this->params['urisegments']['pk'];
			$this->urisegments['valpk'] = $this->params['urisegments']['valpk'];
		}
		$command=explode(",",$this->params['command']);
		$_cmdtext="";
		$_cmdtext.=(in_array("browse",$command)) ? "<a title='Refresh' href='javascript:loadcontent(\"".$this->maincontent."\",\"".$this->filename."\");' class='btn btn-brand btn-elevate btn-circle btn-icon'><i class='la la-refresh'></i></a>" : '' ;

		$valid = true;

		$alert = "";
		if(isset($this->params['alert'])){
			$alert = '<div class="alert alert-'.@$this->params["alert"]["type"].'">
				<div class="alert-text">
					'.@$this->params['alert']['message'].'
				</div>
			</div>';
		}
		$keySelect = array();
		foreach(array_keys($this->params['fieldadd']) as $_keyadd){
			if(@$this->params['fieldadd'][$_keyadd]['type'] != "separator"){
				$keySelect[$_keyadd] = $this->params['fieldadd'][$_keyadd];
			}
		}

		$query="SELECT ".implode(", ",array_keys($keySelect))." FROM ".$this->params['table'];
		if(isset($this->urisegments['pk']) && isset($this->urisegments['valpk'])){
			$query.=" WHERE ".$this->urisegments['pk']."='".urldecode($this->urisegments['valpk'])."'";
		}
		if(isset($this->params['sqlupdate'])){
			$query = $this->params['sqlupdate'];
			$query.=" WHERE ".$this->urisegments['pk']."='".urldecode($this->urisegments['valpk'])."'";
		}

		$query=$this->ci->db->query($query)->result_array();
		$data=$query[0];
		$this->params['content']='
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="kt-font-brand flaticon-edit"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						Edit '.$this->params['name'].'
					</h3>
				</div>
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<div class="kt-portlet__head-actions">
							'.$_cmdtext.'
						</div>
					</div>
				</div>
			</div>
			<div class="kt-portlet__body">';
		$this->params['content'].= "<div id='alert'>" . $alert . "</div>";
		if( isset($this->params['view']) ){
			$this->params['data'] = (isset($this->params['data'])) ? $this->params['data'] : array();
			$this->params['content'].= $this->ci->load->view($this->params['view'], $this->params['data'], TRUE);
		} else {
			$this->params['content'].= "<form class='form-horizontal' id='".$this->formid."'>";
			foreach(array_keys($this->params['fieldadd']) as $_keyadd){
				$addon = "";
				$rsign = "";
				$validation_message = "";

				// Form Validation
				if( count($post) > 0){
					$data[$_keyadd] = set_value($_keyadd);
				}
				if( isset($this->params['fieldadd'][$_keyadd]['validation']) ){
					$this->ci->form_validation->set_rules( $_keyadd, $this->ci->lang->line($_keyadd), $this->params['fieldadd'][$_keyadd]['validation'] );
					$arr_validation = explode("|", $this->params['fieldadd'][$_keyadd]['validation']);
					if( in_array("required", $arr_validation)) {
						$this->params['fieldadd'][$_keyadd]['is_required'] = "required";
					}
					if( !$this->ci->form_validation->run() ){ // Validation Error
						$validation_message = form_error($_keyadd);
						$valid = false;
					}
				}

				if(@$this->params['fieldadd'][$_keyadd]['is_required'] == true){
					$rsign = "<span class='requiredsign'>*</span>";
				}

				if(isset($this->params['fieldadd'][$_keyadd]['help'])){
					$addon = "<div class='input-group-append'><span class='input-group-text'>".$this->params['fieldadd'][$_keyadd]['help']."</span></div>";
				}
				$isreadonly=(@$this->params['fieldadd'][$_keyadd]['readonly']==true) ? "readonly" : null;
				$isdisabled=(@$this->params['fieldadd'][$_keyadd]['disabled']==true) ? "disabled" : null;
				if(@$this->params['fieldadd'][$_keyadd]['hidden']==true){
					$this->params['content'].="<input type='hidden' name='".$_keyadd."' id='".$_keyadd."' value='".$data[$_keyadd]."' />";
				}
				else {
					$_type=(!isset($this->params['fieldadd'][$_keyadd]['type'])) ? "text" : $this->params['fieldadd'][$_keyadd]['type'] ;
					$isdisabled=($_type=="primarykey") ? "readonly" : $isdisabled;
					if($_type=="primarykey"){
						$_type="text";
					}
					if($_type=="dropdown"){

					}
					elseif($_type=="textarea"){
						$this->params['content'].="<div class='form-group row'>
							<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd). $rsign . "</label>
							<div class='".@$this->params['fieldadd'][$_keyadd]['class']."'>
								<div class='input-group'>
									<textarea ".$isreadonly." ".$isdisabled."  name='".$_keyadd."' id='".$_keyadd."' placeholder='".@$this->params['fieldadd'][$_keyadd]['placeholder']."' ".@$this->params['fieldadd'][$_keyadd]['is_required']." class='form-control' maxlength='".@$this->params['fieldadd'][$_keyadd]['maxlength']."'>".htmlentities($data[$_keyadd], ENT_QUOTES)."</textarea>
									".$addon."
								</div>
								".$validation_message."
							</div>
						</div>";
					}
					elseif( $_type == "separator"){
						$this->params['content'].="
							<div class='form-separator'>".$this->ci->lang->line($_keyadd)."</div>
						";
					}
					elseif($_type=="wysiwyg"){
						$this->params['content'].="<div class='form-group row'>
							<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd). $rsign . "</label>
							<div class='".@$this->params['fieldadd'][$_keyadd]['class']."'>
								<div class='input-group'>
									<textarea ".$isreadonly." ".$isdisabled."  name='".$_keyadd."' id='".$_keyadd."' placeholder='".@$this->params['fieldadd'][$_keyadd]['placeholder']."' ".@$this->params['fieldadd'][$_keyadd]['is_required']." class='form-control' maxlength='".@$this->params['fieldadd'][$_keyadd]['maxlength']."' novalidate>".htmlentities($data[$_keyadd], ENT_QUOTES)."</textarea>
									".$addon."
								</div>
								".$validation_message."
							</div>
						</div>
						<script type='text/javascript'>
							$(document).ready(function(){
								var editor_id = '".$_keyadd."';
								tinymce.EditorManager.execCommand('mceRemoveEditor',true, editor_id);
								tinymce.EditorManager.execCommand('mceAddEditor',true, editor_id);
							});
						</script>
						";
					}
					elseif($_type=="dropdownquery"){
						$option="";
						$selected="";
						$optgroup="";
						$multiple = "";

						$multiplevalues = array();
						$addButton = "";
						$name = "name='".$_keyadd."'";
						if( @$this->params['fieldadd'][$_keyadd]['multiple'] ){
							$multiple = "multiple='multiple'";
							$name = "name='".$_keyadd."[]'";
							// $multiplevalues = json_decode($data[$_keyadd]);
							$multiplevalues = explode(",",$data[$_keyadd]);
						}

						foreach($this->params['fieldadd'][$_keyadd]['sourcequery'] as $opdata){

							if(isset($opdata['labeldt'])){
								if($optgroup!=$opdata['labeldt']){
									$option.="<optgroup label='".$opdata['labeldt']."'>";
									$optgroup=$opdata['labeldt'];
								}
							}
							if($multiple != ""){
								$selected = @in_array($opdata['keydt'], $multiplevalues) ? "selected" : null;
							} else {
								$selected = ($data[$_keyadd]==$opdata['keydt']) ? "selected" : null;
							}
							$option.="<option ".$selected." value='".$opdata['keydt']."'>".$opdata['valuedt']."</option>";

						}

						if( @$this->params['fieldadd'][$_keyadd]['allowadd'] == true || @$this->params['fieldadd'][$_keyadd]['allowedit'] == true){
							$addButton = "<div class='input-group-append'>";
							$titleType = "add";
							if( @$this->params['fieldadd'][$_keyadd]['allowadd'] == true ){
								$titleType = "add";
								$addButton .= "
									<button class='btn btn-primary' type='button' id='add_".$_keyadd."'  data-toggle='modal' data-target='#modal_".$_keyadd."'><i class='la la-plus'></i> </button>
									<script type='text/javascript'>
										$(function(){
											$('#add_".$_keyadd."').click(function(){
												$('#modal_body_".$_keyadd."').load('".$this->params['fieldadd'][$_keyadd]['addurl']."');
											});
										});
						      </script>
									";
							}
							if( @$this->params['fieldadd'][$_keyadd]['allowedit'] == true ){
								$titleType = "edit";
								$addButton .= "
									<button class='btn btn-primary' type='button' id='edit_".$_keyadd."'  data-toggle='modal' data-target='#modal_".$_keyadd."'><i class='la la-edit'></i> </button>
									<script type='text/javascript'>
										$(function(){
											$('#edit_".$_keyadd."').click(function(){
												$('#modal_body_".$_keyadd."').load('".$this->params['fieldadd'][$_keyadd]['editurl']."/edit/pk/".$_keyadd."ID/valpk/' + $('#".$_keyadd."').val());
											});
										});
						      </script>
									";
							}
							$addButton .= "</div>
							<!-- Modal -->
							<div class='modal fade' id='modal_".$_keyadd."' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
							  <div class='modal-dialog modal-lg' role='document'>
							    <div class='modal-content'>
							      <div class='modal-header'>
							        <h5 class='modal-title' id='exampleModalLabel'>".$this->ci->lang->line($titleType)." ".$this->ci->lang->line($_keyadd)."</h5>
							        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
							          <span aria-hidden='true'>&times;</span>
							        </button>
							      </div>
							      <div class='modal-body' id='modal_body_".$_keyadd."'>
							        
							      </div>
							      <div class='modal-footer'>
							        <button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>
							      </div>
							    </div>
							  </div>
							</div>
							";
						}
						$this->params['content'].="<div class='form-group row'>
							<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd) . $rsign ."</label>
							<div class='".@$this->params['fieldadd'][$_keyadd]['class']."'>
								<div class='input-group'>
									<select data-value='".$data[$_keyadd]."' ".$isreadonly." ".$isdisabled."  ".$multiple." ".$name." id='".$_keyadd."' class='form-control' ".@$this->params['fieldadd'][$_keyadd]['is_required'].">
										".$option."
									</select>
									".$addon." ".$addButton."
								</div>
								".$validation_message."
							</div>
						</div>";
						// if( $multiple != "" ){
						// 	$this->params['content'] .= "
						// 		<script type='text/javascript'>
						// 		$(function() {
						//         $('#".$_keyadd."').multipleSelect({
						//             width: '100%'
						//         });
						//     });
						// 	</script>";
						// }
					}
					elseif($_type=="dropdownarray"){
						$option="";
						$multiple="";

						$multiplevalues = array();
						$name = "name='".$_keyadd."'";
						if( @$this->params['fieldadd'][$_keyadd]['multiple'] ){
							$multiple = "multiple='multiple'";
							$name = "name='".$_keyadd."[]'";
							// $multiplevalues = json_decode($data[$_keyadd]);
							$multiplevalues = explode(",",$data[$_keyadd]);
						}
						foreach(array_keys($this->params['fieldadd'][$_keyadd]['sourcearray']) as $opdata){
							if($multiple != ""){
								$selected = @in_array($opdata, $multiplevalues) ? "selected" : null;
							} else {
								$selected = ($data[$_keyadd]==$opdata) ? "selected" : null;
							}
							$option.="<option ".$selected." value='".$opdata."'>".$this->params['fieldadd'][$_keyadd]['sourcearray'][$opdata]."</option>";

						}
						$this->params['content'].="<div class='form-group row'>
							<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd)."</label>
							<div class='".@$this->params['fieldadd'][$_keyadd]['class']."'>
								<div class='input-group'>
									<select data-value='".$data[$_keyadd]."' ".$isreadonly." ".$isdisabled."  name='".$_keyadd."' id='".$_keyadd."' class='form-control' ".@$this->params['fieldadd'][$_keyadd]['is_required'].">
										".$option."
									</select>
									".$addon."
								</div>
							</div>
						</div>";
						// if( $multiple != "" ){
						// 	$this->params['content'] .= "
						// 		<script type='text/javascript'>
						// 		$(function() {
						//         $('#".$_keyadd."').multipleSelect({
						//             width: '100%'
						//         });
						//     });
						// 	</script>";
						// }
					}
					elseif($_type=="checkgroup"){
						$option="";
						$arrdata=explode(",",$data[$_keyadd]);
						foreach($this->params['fieldadd'][$_keyadd]['sourcequery'] as $opdata){
							$selected=(array_search($opdata['keydt'],$arrdata)) ? "checked" : null ;
							$option.="<label><input type='checkbox' class='form-control' name='CHECK_".$opdata['keydt']."' ".$selected.">
							<span class='lbl' style='margin-left:5px;'> ".$opdata['valuedt']." </span></label>";
						}
						$this->params['content'].="<div class='form-group row'>
							<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd). $rsign . "</label>
							<div class='".@$this->params['fieldadd'][$_keyadd]['class']."'>
								<div class='input-group'>
									".$option."
									".$addon."
								</div>
								".$validation_message."
							</div>
						</div>";
					}
					elseif($_type=="file"){

						$img = "<img src='".base_url("uploads/noimage.png")."' class='thumbnail' style='width:100%;' />";
						if($data[$_keyadd] <> ""){
							$img = "<img src='".base_url("uploads/" . $data[$_keyadd])."' class='thumbnail' style='width:100%;' />";
						}
						$extension = substr($data[$_keyadd],-3);
						if( $extension == "pdf"){
							$img = '<object data="'.base_url("uploads/" . $data[$_keyadd]).'" type="application/pdf" width="100%" height="100%">
										   <p><b>Gagal Memuat PDF</b>: Browser Ini tidak mendukung penampil PDF. Silahkan Download File Untuk melihatnya : <a target="_blank" href="'.base_url("uploads/" . $data[$_keyadd]).'">Download PDF</a>.</p>
										</object><style>.img-container{height: 300px}</style>';
						}
						if( $extension == "mp4" || $extension == "avi"){
							$img = '<video width="100%" height="400px" controls>
										  <source src="'.base_url("uploads/" . $data[$_keyadd]).'" type="video/'.$extension.'">
											Browser Anda tidak support dengan pemutar video.
										</video>';
						}
						$this->params['content'].="<div class='form-group row'>
							<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd). $rsign . "</label>
							<div class='".@$this->params['fieldadd'][$_keyadd]['class']."'>
								<div class='input-group'>
									<input class='custom-file-input' ".@$this->params['fieldadd'][$_keyadd]['required']." type='file' accept='application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf, image/*, video/*' id='FILE_".$_keyadd."' name='FILE_".$_keyadd."' value='".$data[$_keyadd]."' />".$addon."
									<label class='custom-file-label' for='FILE_".$_keyadd."'></label>
									<input type='hidden' id='".$_keyadd."' name='".$_keyadd."' value='".$data[$_keyadd]."' />
								</div>
								".$validation_message."
								<div class='img-container' id='IMG_".$_keyadd."'>".$img."</div>
							</div>
							</div>
							<script type='text/javascript'>
								$('#FILE_".$_keyadd."').on('change', function(){
									// $('#IMG_".$_keyadd."').remove();
									var reader = new FileReader();
							    reader.onload = function (e) {
							    	if( e.target.result.substring(5,14) == 'video/mp4' || e.target.result.substring(5,14) == 'video/avi' ){
							    		var loaded = '<video width=\"100%\" height=\"400px\" controls><source src=\"' + e.target.result + '\" type=\"' + e.target.result.substring(5,14) + '\">Browser Anda tidak support dengan pemutar video.</video>';
							    		$('#IMG_".$_keyadd."').html( loaded );
							    	} else if(e.target.result.substring(5,20) == 'application/pdf') {
							    		var loaded = '<object data=\"' + e.target.result + '\" type=\"application/pdf\" width=\"100%\" height=\"600px\"><p><b>Gagal Memuat PDF</b>: Browser Ini tidak mendukung penampil PDF.</p></object>';
											$('#IMG_".$_keyadd."').html( loaded );
							    	} else {
						    			$('#IMG_".$_keyadd."').html('<img class=\"thumbnail\" src=\"' + e.target.result + '\" style=\"width:100%;\" />');
							    	}
							    };

							    // read the image file as a data URL.
							    reader.readAsDataURL(this.files[0]);

									ajaxupload('".$_keyadd."');
									return false;
								});
							</script>
							";
					}
					elseif($_type == "map"){
						$default_position = ($data[$_keyadd] == "") ? $this->configurations['MAP_DEFAULT_LOCATION'] : $data[$_keyadd];
						$latlng = explode(",", $default_position);
						$default_lat = $latlng[0];
						$default_lng = $latlng[1];
						$addressChain = "";
						if( isset($this->params['fieldadd'][$_keyadd]['address_id'] )){
							$id = $this->params['fieldadd'][$_keyadd]['address_id'];
							$addressChain = "
							var input = document.getElementById('".$id."');
							var autocomplete = new google.maps.places.Autocomplete(input);
							autocomplete.bindTo('bounds', map);
							autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
							
							/*var infowindow = new google.maps.InfoWindow();
			        var infowindowContent = document.getElementById('infowindow-content');
			        infowindow.setContent(infowindowContent);
			        var marker = new google.maps.Marker({
			          map: map,
			          anchorPoint: new google.maps.Point(0, -29)
			        });*/

			        autocomplete.addListener('place_changed', function() {
		          
		          /*infowindow.close();*/

			          marker.setVisible(false);
			          var place = autocomplete.getPlace();
			          if (!place.geometry) {
			            // User entered the name of a Place that was not suggested and
			            // pressed the Enter key, or the Place Details request failed.
			            window.alert('No details available for input: ' + place.name);
			            return;
			          }

		          // If the place has a geometry, then present it on a map.

		          if (place.geometry.viewport) {
		            map.fitBounds(place.geometry.viewport);
		          } else {
		            map.setCenter(place.geometry.location);
		            map.setZoom(17);  // Why 17? Because it looks good.
		          }

		          marker.setPosition(place.geometry.location);
		          marker.setVisible(true);
		          var lat = place.geometry.location.lat();
		          var lng = place.geometry.location.lng();
							$('#".$_keyadd."').val(eval(lat) + ',' + eval(lng));

		          var address = '';
		          if (place.address_components) {
		            address = [
		              (place.address_components[0] && place.address_components[0].short_name || ''),
		              (place.address_components[1] && place.address_components[1].short_name || ''),
		              (place.address_components[2] && place.address_components[2].short_name || '')
		            ].join(' ');
		          }

		          /*
		          infowindowContent.children['place-icon'].src = place.icon;
		          infowindowContent.children['place-name'].textContent = place.name;
		          infowindowContent.children['place-address'].textContent = address;
		          infowindow.open(map, marker);
		          */
		        });
							";
						}
						$this->params['content'].="<div class='form-group row'>
							<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd). $rsign . "</label>
							<div class='".@$this->params['fieldadd'][$_keyadd]['class']."'>
								<div class='input-group'>
									<input type='text' class='form-control' id='".$_keyadd."' name='".$_keyadd."' value='".$default_position."' />
								</div>
								".$validation_message."
								<div class='map-container' id='MAP_".$_keyadd."' style='height:300px;'></div>
							</div>
							</div>
							<script type='text/javascript'>
								var map, infoWindow;
								".$_keyadd."_initMap();
								function ".$_keyadd."_initMap() {
									var location = { lat: ".$default_lat.", lng: ".$default_lng." };
							    var map = new google.maps.Map(document.getElementById('MAP_".$_keyadd."'), {
							      zoom: 18,
							      center: location
							    });

							    ".$addressChain."

							    // Add a marker at the center of the map.
							    var marker = new google.maps.Marker({
							      position: location,
							      title: 'Pilih Lokasi',
							      map: map,
							      draggable: true,
							   		animation: google.maps.Animation.DROP
							    });
							    google.maps.event.addListener(marker, 'dragend', function (event) {
							    	var lat = this.getPosition().lat();
							    	var lng = this.getPosition().lng();
								    $('#".$_keyadd."').val(eval(lat) + ',' + eval(lng));
									});
							  }
							</script>";
					}
					else {
						$step = "";
						$dateclass="";
						$append="";
						$value=htmlentities($data[$_keyadd], ENT_QUOTES);
						$js="";
						$min= isset($this->params['fieldadd'][$_keyadd]['min']) ? " min='".$this->params['fieldadd'][$_keyadd]['min']."' " : null;
						$max= isset($this->params['fieldadd'][$_keyadd]['max']) ? " max='".$this->params['fieldadd'][$_keyadd]['max']."' " : null;
						$currencyAdd = "";
						$currencyClass = "";
						if($_type=="date"){
							$_type="text";
							$dateclass="date-picker";
							$append="<span class='input-group-append'><span class='input-group-text'><i class='la la-calendar'></i></span></span>";
							if(count($post) > 0){
								if(@$post[$_keyadd] == ""){
									unset($post[$_keyadd]);
								}
							}
						}
						elseif($_type=="time"){
							$_type="text";
							$dateclass="time-picker";
							$append="<span class='input-group-append'><span class='input-group-text'><i class='la la-clock-o'></i></span></span>
									<script type='text/javascript'>
										$('#".$_keyadd.", #".$_keyadd."_modal').timepicker({
						            minuteStep: 1,
						            defaultTime: '',
						            showSeconds: true,
						            showMeridian: false,
						            snapToStep: true
						        });
									</script>
							";
						}
						elseif($_type=="datetime"){
							$_type="text";
							$dateclass="datetime-picker";
							$append="<span class='input-group-append'><span class='input-group-text'><i class='la la-calendar'></i></span></span>";
						}
						elseif($_type=="password"){
							$value="";
						}
						elseif($_type=="decimal"){
							$_type="number";
							$step = "step='0.01'";
						}
						elseif($_type=="currency"){
							$_type="number";
							$step = "step='0.01'";
							$currencyAdd = " data-number-to-fixed='2' data-number-stepfactor='100'";
							$currencyClass = " currency";
						}
						elseif($_type=="tagsinput"){
							$js = "<script type='text/javascript'>
								$('#".$_keyadd."').tagsinput();
							</script>";
							$_type = "text";
						}
						$this->params['content'].="<div class='form-group row'>
							<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd). $rsign . "</label>
							<div class='".$dateclass." ".@$this->params['fieldadd'][$_keyadd]['class']."'>
								<div class='input-group'>
									<input ".$min." ".$max." ".$isreadonly." ".$isdisabled." value='".$value."' type='".$_type."' ".$step." name='".$_keyadd."' id='".$_keyadd."' placeholder='".@$this->params['fieldadd'][$_keyadd]['placeholder']."' ".@$this->params['fieldadd'][$_keyadd]['is_required']." class='form-control ".$currencyClass."' ".$currencyAdd." maxlength='".@$this->params['fieldadd'][$_keyadd]['maxlength']."' />".$append."
									".$addon."
								</div>
								".$validation_message."
							</div>
						</div>" . $js;
					}
				}
			}
			if( !isset($this->params['showcontrol']) || $this->params['showcontrol'] != false ){
			$this->params['content'].="<div class='form-group row'>
				<div class='offset-md-2 col-md-6'>
					<button class='btn btn-pill btn-primary' type='submit'><i class='la la-save'></i>".$this->ci->lang->line("submit")."</button>
						<button class='btn btn-pill btn-danger' type='reset'><i class='la la-refresh'></i>".$this->ci->lang->line("reset")."</button>
				</div>
			</div>";
			}
			$this->params['content'].="</form>";
		}
		$this->params['content'].='</div>
		<script type="text/javascript">
			$(document).ready(function(){
				$(this).updatePolyfill();
				webshim.activeLang("id");
				webshims.setOptions("forms-ext", {
				    replaceUI: "auto",
				    types: "number",
				    // basePath: "./assets/admin/js/shims/"
				});
				webshims.polyfill("forms forms-ext");

				$(".select2 > .input-group > select").select2({
					width: "100%",
					placeholder: $(this).attr("placeholder"),
					allowClear: Boolean($(this).data("allow-clear")),
				});
				$(".date-picker input").datepicker({
					format: "yyyy-mm-dd",
					autoclose: true,
				});
				$(".datetime-picker input").datetimepicker();
				$("#'.$this->formid.'").submit(function(){
					$("#'.$this->maincontent.'").html("<div class=\"text-center spin-loading\"><img src=\"" + base_url + "assets/img/spin.gif\" /></div>");
  				var datapost=$(this).serialize();
					var target="'.$this->filename.'/edit/pk/'.@$this->urisegments['pk'].'/valpk/'.@urlencode($this->urisegments['valpk']).'";
					loadpost("'.$this->maincontent.'", target, datapost);
					return false;
				});
			});
		</script>
		';
		// Updating Data POst
		if( count($post) > 0 && $valid == true){
			foreach($this->params['fieldadd'] as $_keyadd => $_params){
				if(@$_params['type'] == "separator"){
					unset($post[$_keyadd]);
				}
				if(@$_params['disabled'] == true){
					unset($post[$_keyadd]);
				}
			}
			if( isset($this->params['unset_post']) ){
				$this->params['unset_post'] = explode(",", $this->params['unset_post']);
				foreach( $this->params['unset_post'] as $unset){
					unset($post[$unset]);
				}
			}
			// print_r($this->urisegments);exit;
			$upd=$this->ci->db->update($this->params['table'],$post,array($this->urisegments['pk'] => urldecode($this->urisegments['valpk'])));
			if( $upd == 1 ){
				$this->params['content'] = '<script type="text/javascript">
					loadcontent("'.$this->maincontent.'","'.$this->filename.'/", true);
				</script>';
			}
		}
		$this->mainfunction($this->params);
		return $this->str;
	}

	public function report(){
		$months = $this->ci->config->item("months");
		$_primary="";
		foreach(array_keys($this->params['fieldselect']) as $_cols){
			if(@$this->params['fieldselect'][$_cols]['type']=="primarykey"){
				$_primary=$_cols;
				break;
			}
		}
		$command=explode(",",$this->params['command']);
		$_cmdtext="";
		$_cmdtext.=(in_array("browse",$command)) ? "<a title='Refresh' href='javascript:loadcontent(\"".$this->maincontent."\",\"".$this->filename."\");' class='btn btn-brand btn-elevate btn-circle btn-icon btn-browse'><i class='la la-refresh'></i></a>" : '' ;
		$_cmdtext.=(in_array("search",$command)) ? "<a title='Search' href='javascript:opensearch();' class='btn btn-brand btn-elevate btn-circle btn-icon btn-search'><i class='la la-search'></i></a>" : '' ;
		$_cmdtext.=(in_array("print",$command)) ? "<a title='Print' id='btn-print' href='javascript:print(\"printarea\");' class='btn btn-brand btn-elevate btn-circle btn-icon btn-print'><i class='la la-print'></i></a>" : '' ;
		$_cmdtext.=(in_array("excel",$command)) ? "<a title='Excel' id='btn-excel' href='#' class='btn btn-brand btn-elevate btn-circle btn-icon btn-excel'><i class='la la-file-excel-o'></i></a>" : '' ;


		$search="";
		if(isset($this->params['search']) && count($this->params['search'])>0){
			$search.="<div class='searchpanel'><form class='form search-form' id='searchform'><div class='row'>";
			$counter=1;
			foreach(array_keys($this->params['search']) as $_search){
				$addon = "";
				if(isset($this->params['search'][$_search]['help'])){
					$addon = "<div class='input-group-append'><span class='input-group-text'>".$this->params['fieldadd'][$_keyadd]['help']."</span></div>";
				}
				$_type=(!isset($this->params['search'][$_search]['type'])) ? "text" : $this->params['search'][$_search]['type'] ;
				$dateclass="";
				$append="";
				$value="";
				if(isset($_POST[$_search])){
					$value = $_POST[$_search];
				}
				else {
					if(isset($this->params['search'][$_search]['value'])){
						$value = $this->params['search'][$_search]['value'];
					}
				}

				if($_type=="dropdownquery"){
					$option="";
					$selected="";
					$optgroup="";
					foreach($this->params['search'][$_search]['sourcequery'] as $dropdown){
						if(isset($opdata['labeldt'])){
							if($optgroup!=$opdata['labeldt']){
								$option.="<optgroup label='".$opdata['labeldt']."'>";
								$optgroup=$opdata['labeldt'];
							}
						}
						$selected = ($value==$dropdown['keydt']) ? "selected" : null;
						$option.="<option ".$selected." value='".$dropdown['keydt']."'>".$dropdown['valuedt']."</option>";
					}
					$search.="<div class='form-group col-md-4 mb-3'>
						<label for='".$_search."'>".$this->ci->lang->line($_search)."</label>
						<div class='".@$this->params['search'][$_search]['class']."'>
							<div class='input-group'>
								<select name='".$_search."' id='".$_search."' class='form-control'>
									".$option."
								</select>
								".$addon."
							</div>
						</div>
					</div>";
				}
				elseif($_type=="dropdownarray"){
					$option="";
					foreach(array_keys($this->params['search'][$_search]['sourcearray']) as $opdata){
						$selected=($opdata==$value) ? "selected" : null ;
						$option.="<option ".$selected." value='".$opdata."'>".$this->params['search'][$_search]['sourcearray'][$opdata]."</option>";
					}
					$search.="<div class='form-group col-md-4 mb-3'>
						<label for='".$_search."'>".$this->ci->lang->line($_search)."</label>
						<div class='".@$this->params['search'][$_search]['class']."'>
							<div class='input-group'>
								<select name='".$_search."' id='".$_search."' class='form-control'>
									".$option."
								</select>
								".$addon."
							</div>
						</div>
					</div>";
				}
				else {
					// $dateclass = "";
					if($_type=="date"){
						$_type="text";
						$dateclass="date-picker";
						$append="<div class='input-group-append'><span class='input-group-text'><i class='la la-calendar'></i></span></div>";
					}
					elseif($_type=="time"){
						$dateclass="time-picker";
						$append="<div class='input-group-append'><span class='input-group-text'><i class='la la-clock-o'></i></span></div>";
					}
					$search.="<div class='form-group col-md-4 mb-3'>
							<label for='".$_search."'>".$this->ci->lang->line($_search)." : </label>
							<div class='".$dateclass." ".@$this->params['search'][$_search]['class']."'>
								<div class='input-group'>
									<input value='".$value."' type='".$_type."' name='".$_search."' id='".$_search."' placeholder='".@$this->params['search'][$_search]['placeholder']."' ".@$this->params['search'][$_search]['is_required']." class='form-control' maxlength='".@$this->params['search'][$_search]['maxlength']."' />".$append."
									".$addon."
								</div>
							</div>
						</div>";
				}

			}
			$search.="<div class='form-group col-md-2' style='margin-top:40px;'><button type='submit' class='btn btn-primary btn-sm'> Cari </button> <button type='button' onclick='loadcontent(\"".$this->maincontent."\",\"".$this->filename."\");' class='btn btn-danger btn-sm'>".$this->ci->lang->line("reset")."</button></div>";
			$search.='</div></form></div>
			<script type="text/javascript">
				$(function(){
					$(".select2 > .input-group > select").select2({
						width: "100%",
						placeholder: $(this).attr("placeholder"),
						allowClear: Boolean($(this).data("allow-clear")),
					});
					$(".date-picker input").datepicker({
						format: "yyyy-mm-dd",
						autoclose: true,
					});
					$("#searchform").submit(function(){
						$("#'.$this->maincontent.'").html("<div class=\"text-center spin-loading\"><img src=\"" + base_url + "assets/img/spin.gif\" /></div>");
						var datapost=$(this).serialize();
						loadpost("'.$this->maincontent.'","'.$this->filename.'",datapost);
						return false;
					});
				});
			</script>
			';

		}
		$this->params['content']='
			<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="kt-font-brand flaticon-list"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						'.$this->params['name'].'
					</h3>
				</div>
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<div class="kt-portlet__head-actions">
							'.$_cmdtext.'
						</div>
					</div>
				</div>
			</div>
			<div class="kt-portlet__body">
		'.$search.'
		<div class="table-responsive">
		<div id="printarea">';



			$query=$this->ci->db->query($this->params['sql'])->result_array();

			if(isset($this->params['view'])){
				$data['params'] = $this->params;
				$data['query'] = $query;
				$this->params['content'].= $this->ci->load->view( $this->params['view'], $data, true);
			} else {
				$logo = (isset($this->params['logo']) ? "<img src='".$this->params['logo']."' style='width:100px' />" : null);
				$this->params['content'].= '
				<table id="table-header" class="table no-border" border="0" cellpadding="5" cellspacing="0">
					<tr>
						<td style="width:100px;">
							'.$logo.'
						</td>
						<td>
							<h4>'.@$this->params["title"].' '.@$this->params["titleaddon"].'</h4>
							<h4 style="text-align:center;margin:1px;padding:1px;line-height:inherit;">'.@$this->params["kop1"].'</h4>
							<h3 style="text-align:center;margin:1px;padding:1px;line-height:inherit;">'.@$this->params["kop2"].'</h3>
							<p style="text-align:center;margin:1px;padding:1px;line-height:inherit;">'.nl2br(@$this->params["kopaddress"]).'</p>
						</td>
					</tr>
				</table>
				<hr style="border:1px solid #666;" />';
				$this->params['content'].= '<table id="table-print" class="table-print table table-striped table-bordered"><thead>';

				if(!isset($this->params['head'])){
					$this->params['content'].= '<tr>';
					foreach(array_keys($this->params['fieldselect']) as $keys){

						if($keys=="SEQ"){
							$this->params['content'].='<th class="center sorting_disabled" style="width:20px;">No</th>';
						}
						elseif(!@$this->params['fieldselect'][$keys]['hidden']){
							$icon=isset($this->params['fieldselect'][$keys]['icon']) ? '<i class="'.$this->params['fieldselect'][$keys]['icon'].'"></i>' : null ;
							$width=isset($this->params['fieldselect'][$keys]['width']) ? "width:".$this->params['fieldselect'][$keys]['width'].";" : null ;
							$title = isset($this->params['fieldselect'][$keys]['title']) ? $this->params['fieldselect'][$keys]['title'] : $this->ci->lang->line($keys) ;
							$this->params['content'].='<th style="font-size:10pt;'.$width.'" class="'.@$this->params['fieldselect'][$keys]['class'].'" style="width:'.@$this->params['fieldselect'][$keys]['width'].';">'.$icon." ".$title.'</th>';
						}
					}
					$this->params['content'].='</tr>';
				} else {
					$this->params['content'].= $this->params['head'];
				}

				if( @$this->params['colnumber']==true ){
					$colnumber = 1;
					foreach(array_keys($this->params['fieldselect']) as $keys){
						if(!@$this->params['fieldselect'][$keys]['hidden']){
							$this->params['content'].='<th style="text-align:center;">'.$colnumber.'</th>';
							$colnumber++;
						}
					}
				}

				$this->params['content'].='</thead><tbody>';

				if(count($query) > 0){
					$seq=0;
					$sum = array();
					foreach($query as $data){
						$seq++;
						$this->params['content'].="<tr>";
						foreach(array_keys($this->params['fieldselect']) as $keys){
							if($keys=="SEQ"){
								$this->params['content'].='<td class="center '.@$this->params['fieldselect'][$keys]['class'].'">'.number_format($seq,0).'.</th>';
							}
							elseif(!@$this->params['fieldselect'][$keys]['hidden']){
								$_value=@$data[$keys];
								switch(@$this->params['fieldselect'][$keys]['type']){
									case "image":
										$_value="<img style='width:100%;' src='".base_url("uploads")."/".@$data[$keys]."'/>";
										break;
									case "date":
										if(@$data[$keys] != ""){
											$_value=date("d",strtotime(@$data[$keys])) . " " . $months[ date("n",strtotime(@$data[$keys])) ] . " " . date("Y",strtotime(@$data[$keys]));
										}
										break;
									case "datetime":
										if($_value != ""){
											$_value=date("d",strtotime(@$data[$keys])) . " " . $months[ date("n",strtotime(@$data[$keys])) ] . " " . date("Y",strtotime(@$data[$keys])) . date("H:i:s", strtotime(@$data[$keys]));
										}
										break;
									case "number":
										$this->params['fieldselect'][$keys]['class'] .= " text-right";
										$_value=number_format(@$data[$keys],0,",",".");
										break;
									case "decimal":
										$this->params['fieldselect'][$keys]['class'] .= " text-right";
										$_value=number_format(@$data[$keys],2,",",".");
										break;
									case "dropdownarray":
										$_value=@$this->params['fieldselect'][$keys]['sourcearray'][$data[$keys]];
										break;
								}
								$_value = nl2br($_value);
								if(isset($this->params['groupcol'])){
									if(in_array($keys,$this->params['groupcol'])){
										if(@$groupcol[$keys]['data']!=$_value){
											$this->params['content'].='<td class="'.@$this->params['fieldselect'][$keys]['class'].'" width="'.@$this->params['fieldselect'][$keys]['width'].'" align="'.@$this->params['fieldselect'][$keys]['align'].'">'.@$this->params['fieldselect'][$keys]['ltag'].$_value.@$this->params['fieldselect'][$keys]['rtag'].'</td>';
											$groupcol[$keys]['data']=$_value;
										}
										else {
											$this->params['content'].='<td class="'.@$this->params['fieldselect'][$keys]['class'].'" width="'.@$this->params['fieldselect'][$keys]['width'].'" align="'.@$this->params['fieldselect'][$keys]['align'].'"></td>';
										}
									}
									else {
										$this->params['content'].='<td class="'.@$this->params['fieldselect'][$keys]['class'].'" width="'.@$this->params['fieldselect'][$keys]['width'].'" align="'.@$this->params['fieldselect'][$keys]['align'].'">'.@$this->params['fieldselect'][$keys]['ltag'].$_value.@$this->params['fieldselect'][$keys]['rtag'].'</td>';
									}
								}
								else {
									$this->params['content'].='<td class="'.@$this->params['fieldselect'][$keys]['class'].'" width="'.@$this->params['fieldselect'][$keys]['width'].'" align="'.@$this->params['fieldselect'][$keys]['align'].'">'.@$this->params['fieldselect'][$keys]['ltag'].$_value.@$this->params['fieldselect'][$keys]['rtag'].'</td>';
								}

								if(@$this->params['fieldselect'][$keys]['sum']){
									if(!isset($sum[$keys]['value'])){
										$sum[$keys]['value'] = $data[$keys];
										$sum[$keys]['type'] = isset($this->params['fieldselect'][$keys]['type']) ? $this->params['fieldselect'][$keys]['type'] : "number";
									} else {
										$sum[$keys]['value'] += $data[$keys];
									}
								}
							}
						}
						$this->params['content'].='</tr>';

					}

					if( count($sum) > 0 ){
						$this->params['content'].='<tr>';
						foreach(array_keys($this->params['fieldselect']) as $keys){
							$finalvalue = array();
							if(!@$this->params['fieldselect'][$keys]['hidden']){
								if(isset($this->params['fieldselect'][$keys]['sum'])){
									if($sum[$keys]['type'] == "number"){
										$finalvalue[$keys] = number_format($sum[$keys]['value'], 0,",",".");
									} elseif($sum[$keys]['type'] == "decimal"){
										$finalvalue[$keys] = number_format($sum[$keys]['value'], 2,",",".");
									}
									$this->params['content'].='<td class="bolder '.@$this->params['fieldselect'][$keys]['class'].'" width="'.@$this->params['fieldselect'][$keys]['width'].'" align="'.@$this->params['fieldselect'][$keys]['align'].'">'.$finalvalue[$keys].'</td>';
								} else {
									$this->params['content'].='<td class="bolder '.@$this->params['fieldselect'][$keys]['class'].'" width="'.@$this->params['fieldselect'][$keys]['width'].'" align="'.@$this->params['fieldselect'][$keys]['align'].'">&nbsp;</td>';
								}
							}
						}
						$this->params['content'].='</tr>';
					}


				} else {
					$this->params['content'].='<table class="table"><tr><td colspan="'.count($this->params['fieldselect']).'">Tidak ada data</td></tr>';
				}

				$this->params['content'].='</table>';
				if( isset($this->params['ttd1jabatan']) || isset($this->params['ttd2jabatan']) || isset($this->params['ttdcity']) ){
					$this->params['content'].='
					<br />
					<table class="table table-borderless text-center">
						<tr>
							<td style="width:50%"></td>
							<td style="width:50%">'.(isset($this->params['ttdcity']) ? $this->params['ttdcity'] . "," : null).' '.@$this->params['ttddate'].'</td>
						</td>
						<tr>
							<td style="width:50%">'.(@$this->params['ttd1jabatan'] != "" ? $this->params['ttd1jabatan'] . "," : null).'<br /><br /><br /><br /></td>
							<td style="width:50%">'.(@$this->params['ttd2jabatan'] != "" ? $this->params['ttd2jabatan'] . "," : null).'<br /><br /><br /><br /></td>
						</tr>
						<tr>
							<td style="width:50%"><strong><u>'.@$this->params['ttd1name'].'</u></strong><br />'.(isset($this->params['ttd1nip']) ? ''.@$this->params['ttd1nip'] : "") . '</td>
							<td style="width:50%"><strong><u>'.@$this->params['ttd2name'].'</u></strong><br />'.(isset($this->params['ttd2nip']) ? ''.@$this->params['ttd2nip'] : "").'</td>
						</tr>
					</table>';
				}
				$this->params['content'].='</div></div>';
			}
			if( in_array("excel",$command) ){
				$this->params['content'] .= "<script type='text/javascript'>
					var ExportButtons = document.getElementById( 'table-print' );
				  var instance = new TableExport(ExportButtons, {
				    formats: ['xlsx'],
				    exportButtons: false,
				    filename: '".$this->params['name']."',
				  });
				  var exportData = instance.getExportData()['table-print']['xlsx'];
				  var XLSbutton = document.getElementById('btn-excel');
				  XLSbutton.addEventListener('click', function (e) {
				      instance.export2file(exportData.data, exportData.mimeType, exportData.filename, exportData.fileExtension);
				  });
				</script>";
			}
			if( isset($this->params['jsinclude']) ){
				$this->params['content'] .= $this->params['jsinclude'];
			}

			$this->mainfunction($this->params);
			return $this->str;
	}

	public function chart(){
		$command=explode(",",$this->params['command']);
		$_cmdtext="";
		$_cmdtext.=(in_array("browse",$command)) ? "<a title='Refresh' href='javascript:loadcontent(\"".$this->maincontent."\",\"".$this->filename."\");' class='btn btn-primary'><i class='icon-white icon icon-refresh'></i></a></li>" : '' ;
		$_cmdtext.=(in_array("print",$command)) ? "<a title='Print' href='javascript:print(\"printarea\");' class='btn btn-primary'><i class='icon-white icon icon-print'></i></a></li>" : '' ;
		$_cmdtext.=(in_array("search",$command)) ? "<a title='Search' href='javascript:opensearch();' class='btn btn-primary'><i class='icon-white icon icon-search'></i></a></li>" : '' ;

		$search="";
		if(count(@$this->params['search'])>0){
			$search.="<form class='form-horizontal' id='searchform' style='margin-top:10px;'>
				<div class='row'>";
			$counter=1;
			foreach(array_keys($this->params['search']) as $_search){
				$_type=(!isset($this->params['search'][$_search]['type'])) ? "text" : $this->params['search'][$_search]['type'] ;
				$dateclass="";
				$append="";
				$value=@$_POST[$_search];
				if($_type=="dropdownquery"){
					$option="";
					$selected="";
					$optgroup="";
					foreach($this->params['search'][$_search]['sourcequery'] as $dropdown){
						if(isset($opdata['labeldt'])){
							if($optgroup!=$opdata['labeldt']){
								$option.="<optgroup label='".$opdata['labeldt']."'>";
								$optgroup=$opdata['labeldt'];
							}
						}
						$selected = (@$_POST[$_search]==$dropdown['keydt']) ? "selected" : null;
						$option.="<option ".$selected." value='".$dropdown['keydt']."'>".$dropdown['valuedt']."</option>";
					}
					$search.="<div class='form-group col-md-6'>
						<label class='col-md-2' for='".$_search."'>".$this->ci->lang->line($_search)."</label>
						<div class='controls'>
							<select name='".$_search."' id='".$_search."' class='".@$this->params['search'][$_search]['class']."'>
								".$option."
							</select>
							<span class='help-inline'>".@$this->params['search'][$_search]['help']."</span>
						</div>
					</div>";
				}
				elseif($_type=="dropdownarray"){
					$option="";
					foreach(array_keys($this->params['search'][$_search]['sourcearray']) as $opdata){
						$selected=($opdata==@$_POST[$_search]) ? "selected" : null ;
						$option.="<option ".$selected." value='".$opdata."'>".$this->params['search'][$_search]['sourcearray'][$opdata]."</option>";
					}
					$search.="<div class='form-group col-md-6'>
						<label class='col-md-2' for='".$_search."'>".$this->ci->lang->line($_search)."</label>
						<div class='controls'>
							<select name='".$_search."' id='".$_search."' class='".@$this->params['search'][$_search]['class']."'>
								".$option."
							</select>
							<span class='help-inline'>".@$this->params['search'][$_search]['help']."</span>
						</div>
					</div>";
				}
				else {
					if($_type=="date"){
					$_type="text";
					$dateclass="date-picker";
					$append="<span class='add-on'><span class='input-group-text'><i class='la la-calendar'></i></span></span>";
					}
					elseif($_type=="time"){
						$dateclass="time-picker";
						$append="<span class='add-on'><span class='input-group-text'><i class='la la-clock-o'></i></span></span>";
					}
					$value = (@$_POST[$_search]!="" ) ? $_POST[$_search] : $value ;
					$search.="<div class='form-group col-md-6'>
							<label class='col-md-2' for='".$_search."'>".$this->ci->lang->line($_search)."</label>
							<div class='controls'>
								<input value='".$value."' type='".$_type."' name='".$_search."' id='".$_search."' placeholder='".@$this->params['search'][$_search]['placeholder']."' ".@$this->params['search'][$_search]['is_required']." class='input ".$dateclass." ".@$this->params['search'][$_search]['class']."' maxlength='".@$this->params['search'][$_search]['maxlength']."' />".$append."
								<span class='help-inline'>".@$this->params['search'][$_search]['help']."</span>
							</div>
						</div>";
				}
				if($counter==2){
					$search.="</div><div class='row'>";
					$counter=0;
				}
				$counter++;
			}
			$search.="<div class='form-group col-md-2' style='float:right;'><button type='submit' class='btn btn-primary btn-small'> Cari </button> <button type='button' onclick='loadcontent(\"".$this->maincontent."\",\"".$this->filename."\");' class='btn btn-danger btn-small'>".$this->ci->lang->line("reset")."</button></div>";
			$search.='</div></form>
			<script type="text/javascript">
				$(function(){
					$(".select2 > .input-group > select").select2({
						width: "100%",
						placeholder: $(this).attr("placeholder"),
						allowClear: Boolean($(this).data("allow-clear")),
					});
					$(".date-picker").datepicker({
						format: "yyyy-mm-dd",
					}).next().on(ace.click_event, function(){
						$(this).prev().focus();
					});
					$("#searchform").submit(function(){
						$("#'.$this->maincontent.'").html("<div class=\'loader\'><img src=\''.base_url().'assets/img/spin.gif\' align=\'center\' /></div> <h3 style=\'text-align:center;color:#799d23;\'> Mencari ...</h3>");
						var datapost=$(this).serialize();
						loadpost("'.$this->maincontent.'","'.$this->filename.'",datapost);
						return false;
					});
				});
			</script>
			';

		}
		$this->params['content']='
		<div class="row">
			<div class="table-header" style="overflow:hidden;height:auto;">
				<div class="col-md-9">Data '.$this->params["name"].'</div>
				<div class="col-md-3">
					<ul class="command">
						'.$_cmdtext.'
					</ul>
				</div>
			</div>
			<div class="searchpanel" style="overflow:hidden;height:auto;">
				'.$search.'
			</div>
			<div id="printarea">
				<h4>'.@$this->params["title"].' '.@$this->params["titleaddon"].'</h3>
				<h4 style="text-align:center;margin:1px;padding:1px;line-height:inherit;">'.@$this->params["kop1"].'</h3><h3 style="text-align:center;margin:1px;padding:1px;line-height:inherit;">'.@$this->params["kop2"].'</h3>
				<h6 style="text-align:center;margin:1px;padding:1px;line-height:inherit;">'.nl2br(@$this->params["kopaddress"]).'</h5>
				<hr style="border:1px solid #666;" />';
				$this->params['content'].='<script type="text/javascript">
					var chart;
					var chartData;
					$(function(){
						$.getJSON("'.$this->params["chart"]["chartdata"].'",function(chartData){
							// SERIAL CHART
							chart = new AmCharts.AmSerialChart();
							chart.dataProvider = chartData;
							chart.categoryField = "'.$this->params["chart"]["x"].'";
							chart.startDuration = 1;

							// AXES
							// category
							var categoryAxis = chart.categoryAxis;
							categoryAxis.labelRotation = '.$this->params['chart']['labelrotation'].';
							categoryAxis.gridPosition = "start";

							// value
							var valueAxis = new AmCharts.ValueAxis();
							valueAxis.title = "'.$this->ci->lang->line($this->params["chart"]["y"]).'";
							valueAxis.dashLength = 5;
							chart.addValueAxis(valueAxis);

							// GRAPH
							var graph = new AmCharts.AmGraph();
							graph.valueField = "'.$this->params["chart"]["y"].'";
							graph.balloonText = "[[category]]: <b>[[value]]</b>";
							graph.type = "'.$this->params['chart']['type'].'";
							graph.lineAlpha = 0;
							graph.fillAlphas = 0.8;
							chart.addGraph(graph);

							// CURSOR
							var chartCursor = new AmCharts.ChartCursor();
							chartCursor.cursorAlpha = 0;
							chartCursor.zoomable = false;
							chartCursor.categoryBalloonEnabled = false;
							chart.addChartCursor(chartCursor);
							chart.creditsPosition = "top-right";
							chart.write("chartdiv");
						});
					});
				</script>
				<style media="printer">
					#chartdiv{
						width : 100%;
						margin-right : 20px;
					}
				</style>
				<div id="chartdiv" style="height: 400px;"></div>
			</div>';
		$this->mainfunction($this->params);
		return $this->str;
	}

	public function header_slave(){
		$header="";
		$query= "SELECT * FROM (".$this->params['sqlmaster'].") as ".$this->params['table'];
		$query.=" WHERE ".$this->params['table'].".".$this->params['primarykeymaster']."='".$this->params['valprimarykeymaster']."'";
		$qry=$this->ci->db->query($query)->result_array();
		$qry=$qry[0];
		foreach(array_keys($this->params['fieldselectedmaster']) as $_keys ){
			if(@$this->params['fieldselectedmaster'][$_keys]['type']=="number"){
				$qry[$_keys]=number_format($qry[$_keys],0,",",".");
			}
			elseif(@$this->params['fieldselectedmaster'][$_keys]['type']=="decimal"){
				$qry[$_keys]=number_format($qry[$_keys],2,",",".");
			}
			elseif(@$this->params['fieldselectedmaster'][$_keys]['type']=="date"){
				$qry[$_keys]=date("d M Y",strtotime($qry[$_keys]));
			}
			elseif(@$this->params['fieldselectedmaster'][$_keys]['type']=="image"){
				$qry[$_keys]="<img src='".base_url("uploads/" . $qry[$_keys])."' />";
			}
			elseif(@$this->params['fieldselectedmaster'][$_keys]['type']=="dropdownarray"){
				$array = $this->params['fieldselectedmaster'][$_keys]['sourcearray'];
				$qry[$_keys]=$array[$qry[$_keys]];
			}
			$header.="<tr>
				<td style='width:150px;'><strong>".@$this->ci->lang->line($_keys)."</strong></td>
				<td style='width:2px;'><strong>:</strong></td>
				<td>".@$this->params['fieldselectedmaster'][$_keys]['ltag'].$qry[$_keys].@$this->params['fieldselectedmaster'][$_keys]['rtag']."</td>
			</tr>";
		}
		return $header;
	}

	public function browse_detail(){
		$command=explode(",",$this->params['command']);
		$_cmdtext="";
		$_cmdtext.=(in_array("browse",$command)) ? "<a title='Refresh' role='button' href='javascript:loadcontent(\"".$this->maincontent."\",\"".$this->filename."\");' class='btn btn-brand btn-elevate btn-circle btn-icon'><i class='la la-refresh'></i></a>" : '' ;

		$slaveheader=$this->header_slave();
		$this->params['content']='
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="kt-font-brand flaticon-list"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						'.$this->params['name'].'
					</h3>
				</div>
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<div class="kt-portlet__head-actions">
							'.$_cmdtext.'
						</div>
					</div>
				</div>
			</div>
			<div class="kt-portlet__body">
    	<div class="table-responsive">
			<table class="table table-condensed table-border slave-header">'.$slaveheader.'</table>';
		$_primary="";
		$_foreign="";
		foreach(array_keys($this->params['fieldselectslave']) as $_cols){
			if(@$this->params['fieldselectslave'][$_cols]['type']=="primarykey"){
				$_primary=$_cols;
			}
			if(@$this->params['fieldselectslave'][$_cols]['type']=="foreignkey"){
				$_foreign=$_cols;
			}
		}
		$command=explode(",",$this->params['slavecommand']);
		$_cmdtext="";
		$_cmdtext.=(in_array("add",$command)) ? "<a title='Tambah' role='button' href='javascript:loadcontent(\"".$this->maincontent."\",\"".$this->filename."/add_detail/pk/".$this->params['primarykeymaster']."/valpk/".$this->params['valprimarykeymaster']."\");' class='btn btn-brand btn-elevate btn-circle btn-icon btn-success'><i class='la la-plus'></i></a>" : '' ;
		$_cmdtext.=(in_array("browse",$command)) ? "<a title='Refresh' role='button' href='javascript:loadcontent(\"".$this->maincontent."\",\"".$this->filename."/browse_detail/pk/".$this->params['primarykeymaster']."/valpk/".$this->params['valprimarykeymaster']."\");' class='btn btn-brand btn-elevate btn-circle btn-icon btn-success'><i class='la la-refresh'></i></a>" : '' ;
		$_cmdtext.=(in_array("print",$command)) ? "<a title='Print' role='button' href='javascript:print(\"printarea\");' class='btn btn-brand btn-elevate btn-circle btn-icon btn-success'><i class='la la-print'></i></a>" : '' ;

		$this->params['content'].='
			<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="kt-font-brand flaticon-list"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						'.$this->params['slavename'].'
					</h3>
				</div>
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<div class="kt-portlet__head-actions">
							'.$_cmdtext.'
						</div>
					</div>
				</div>
			</div>
			<div class="kt-portlet__body">';
			
			$this->params['sqlslave']= "SELECT * FROM (".$this->params['sqlslave'].") as ".$this->params['table-slave']."  WHERE ".$this->params['table-slave'].".".$_foreign."='".$this->params['valprimarykeymaster']."'";
			$query=$this->ci->db->query($this->params['sqlslave'])->result_array();
    	
    	if( isset($this->params['view']) ){
    		$this->data['query'] = $query;
    		$this->params['content'].= $this->ci->load->view($this->params['view'], $this->data, TRUE);
    	} else {
    		$this->params['content'].='<div class="table-responsive" id="printarea">
				<table id="dataTable" class="table table-striped table-bordered table-hover table-condensed">';
				$this->params['content'].='<thead><tr>';

				foreach(array_keys($this->params['fieldselectslave']) as $keys){
					$class = "";
					if(
						@$this->params['fieldselectslave'][$keys]['type'] == "number"
						|| @$this->params['fieldselectslave'][$keys]['type'] == "decimal"
					){
						$class = "text-right";
					} else {
						$class = @$this->params['fieldselectslave'][$keys]['class'];
					}

					if($keys=="SEQ"){
						$this->params['content'].='<th class="center sorting_disabled" style="width:20px;">No</th>';
					}
					elseif( $keys == "#" ){
						$this->params['content'].='<th class="center sorting_disabled" style="width:20px;">
								<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid kt-checkbox--brand"><input type="checkbox" value="" class="m-checkable checkbox-header" id="checkbox-header"><span></span></label>
						</th>';
					}
					elseif(@$this->params['fieldselectslave'][$keys]['type']=="checkbox"){
						$this->params['content'].='<th class="center sorting_disabled" style="width:20px;">
							<label>
								<input type="checkbox" />
								<span class="lbl"></span>
							</label>
						</th>';
					}
					elseif(!@$this->params['fieldselectslave'][$keys]['hidden']){
						$icon=isset($this->params['fieldselectslave'][$keys]['icon']) ? '<i class="'.$this->params['fieldselectslave'][$keys]['icon'].'"></i>' : null ;
						$this->params['content'].='<th class="'.@$this->params['fieldselectslave'][$keys]['class'].'" style="width:'.@$this->params['fieldselectslave'][$keys]['width'].';">'.$icon.@$this->ci->lang->line($keys).'</th>';
					}
				}
				if((in_array("preview",$command)) || (in_array("edit",$command)) || (in_array("delete",$command)) || (in_array("printreceipt",$command)) || (in_array("detail",$command)) || (in_array("process",$command))){
					$this->params['content'].='<th class="sorting_disabled" style="width: 80px;">Action</th>';
				}
				$this->params['content'].='</tr></thead><tbody>';
				$seq=0;
				foreach($query as $data){
					$seq++;
					$this->params['content'].="<tr>";
					foreach(array_keys($this->params['fieldselectslave']) as $keys){
						if($keys=="SEQ"){
							$this->params['content'].= '<td class="center '.@$this->params['fieldselectslave'][$keys]['class'].'">'.$seq.'.</th>';
						}
						elseif($keys =="#" ){
							$this->params['content'].= '<td class="center"><label class="kt-checkbox kt-checkbox--single kt-checkbox--solid kt-checkbox--brand"><input type="checkbox" value="" class="m-checkable checkbox-data" id="check-'.$_primary."-".$data[$_primary].'"><span></span></label></td>';
						}
						elseif(@$this->params['fieldselectslave'][$keys]['type']=="checkbox"){
							$this->params['content'].='<td class="center">
								<label>
									<input type="checkbox" />
									<span class="lbl"></span>
								</label>
							</td>';
						}
						elseif(!@$this->params['fieldselectslave'][$keys]['hidden']){
							$_value=@$data[$keys];
							switch(@$this->params['fieldselectslave'][$keys]['type']){
								case "date":
									if( @$data[$keys] != "" ){
										$_value=date("d M Y",strtotime(@$data[$keys]));
									}
									break;
								case "datetime":
									if($_value != ""){
										$_value=date("d M Y - H:i:s",strtotime(@$data[$keys]));
									}
									break;
								case "number":
									$_value=number_format(@$data[$keys],0,",",".");
									$this->params['fieldselectslave'][$keys]['align'] = isset($this->params['fieldselectslave'][$keys]['align']) ? $this->params['fieldselectslave'][$keys]['align'] : "right";
									break;
								case "decimal":
									$_value=number_format(@$data[$keys],2,",",".");
									$this->params['fieldselectslave'][$keys]['align'] = isset($this->params['fieldselectslave'][$keys]['align']) ? $this->params['fieldselectslave'][$keys]['align'] : "right";
									break;
								case "dropdownarray":
									$_value=@$this->params['fieldselectslave'][$keys]['sourcearray'][$data[$keys]];
									break;
								case "image":
									if(@$data[$keys] == "") $data[$keys] = "noimage.png";
									$_value="<img src='".base_url()."uploads/".@$data[$keys]."' class='thumbnail' style='width:100%;' />";
									break;
								case "download":
									if(@$data[$keys] != "" && file_exists("./uploads/".$data[$keys]) != "") {
										$_value="<a href='".base_url()."uploads/".@$data[$keys]."' target='_blank' class='btn btn-sm btn-block btn-primary' title='download'><i class='la la-download'></i> Download</a>";
									} else {
										$_value = "<span class='m-badge m-badge--danger m-badge--dot'></span> <span class='m--font-bold m--font-danger'>No File Available</span>";
									}
									break;
							}
							$_value = @$this->params['fieldselectslave'][$keys]['ltag'] . $_value . @$this->params['fieldselectslave'][$keys]['rtag'];
							$this->params['content'].='<td class="'.@$this->params['fieldselectslave'][$keys]['class'].'" width="'.@$this->params['fieldselectslave'][$keys]['width'].'" align="'.@$this->params['fieldselectslave'][$keys]['align'].'">'.$_value.'</td>';
						}
					}
					$_cmddetail="";
					$_cmddetail.=(in_array("preview",$command)) ? "<a title='Preview' class='btn btn-sm btn-clean btn-icon btn-icon-md' href='uploads/".$data["FILENAME"]."' target='blank'><i class='la la-zoom-in'></i></a>" : '' ;
					$_cmddetail.=(in_array("edit",$command)) ? "<a title='Edit' class='btn btn-sm btn-clean btn-icon btn-icon-md' href='javascript:loadcontent(\"".$this->maincontent."\",\"".$this->filename."/edit_detail/pk/".$_primary."/valpk/".urlencode($data[$_primary])."/fk/".$_foreign."/valfk/".urlencode($data[$_foreign])."\");'><i class='la la-edit'></i></a>" : '' ;
					$_cmddetail.=(in_array("delete",$command)) ? "<a title='Delete' class='btn btn-sm btn-clean btn-icon btn-icon-md m-btn--label-danger' href='javascript:deletedata(\"".$this->maincontent."\",\"".$this->filename."/delete_detail/pk/".$_primary."/valpk/".urlencode($data[$_primary])."/fk/".$_foreign."/valfk/".urlencode($data[$_foreign])."\");'><i class='la la-trash'></i></a>" : '' ;
					$_cmddetail.=(in_array("printreceipt",$command)) ? "<a title='Print' class='btn btn-sm btn-clean btn-icon btn-icon-md' href='javascript:loadmodal(\"".$this->filename."/print_detail/pk/".$_primary."/valpk/".urlencode($data[$_primary])."\",\"print\");'><i class='la la-print'></i></a>" : '' ;

					if((in_array("preview",$command)) || (in_array("edit",$command)) || (in_array("delete",$command)) || (in_array("print",$command)) || (in_array("detail",$command)) || (in_array("process",$command))){
						$this->params['content'].='
							<td class="td-actions" style="width:100px;">
								<div class="action-buttons">
									<div class="btn-group">
									'.$_cmddetail.'
									</div>
								</div>

							</td>';
						}
						$this->params['content'].='</tr>';
				}
				$this->params['content'].='</table></div></div></div>';

		}

		$_cmdbuttom="";
		$_cmdbuttom.=(in_array("updateall",$command)) ? "<a role='button' href='javascript:updateall(\"".$this->maincontent."\",\"".$this->filename."/updateall_slave/pk/".$this->params['primarykeymaster']."/valpk/".$this->params['valprimarykeymaster']."/\",$(\".checkbox-data\"));' class='btn btn-brand btn-elevate'><i class='la la-edit'></i> ".(isset($this->params['updateall']['caption']) ? $this->params['updateall']['caption'] : "Update Checked")."</a>" : '' ;
		$_cmdbuttom.=(in_array("deleteall",$command)) ? "<a role='button' href='javascript:deleteall(\"".$this->maincontent."\",\"".$this->filename."/deleteall_slave/pk/".$this->params['primarykeymaster']."/valpk/".$this->params['valprimarykeymaster']."/\",$(\".checkbox-data\"));' class='btn btn-danger btn-elevate'><i class='la la-trash'></i> ".(isset($this->params['deleteall']['caption']) ? $this->params['deleteall']['caption'] : "Delete Checked")."</a>" : '' ;
		$_cmdbuttom.=(in_array("printall",$command)) ? "<a role='button' href='javascript:printall(\"".$this->maincontent."\",\"".$this->filename."/printall_slave/pk/".$this->params['primarykeymaster']."/valpk/".$this->params['valprimarykeymaster']."/\",$(\".checkbox-data\"));' class='btn btn-brand btn-elevate'><i class='la la-print'></i> Print Checked</a>" : '' ;
		$this->params['content'].= '<div class="btn btn-group">'.$_cmdbuttom.'</div>';


		$this->params['content'].='</div><script type="text/javascript">
			$(function(){
				$("#dataTable").dataTable({
					"processing": true,
					"oLanguage": {
  					"sSearch": "",
  					"sSearchPlaceholder": "Search",
    				"sLengthMenu": "<span>_MENU_</span>",
    				"sProcessing": "<img style=\"width: 50px;\" src=\"'.base_url().'assets/img/spin.gif\" /><br />Memuat Data ...."
					},
					"autoWidth": false
				});

				$("#checkbox-header").on("click",function(){
					if($("#checkbox-header").is(":checked")){
						$(".checkbox-data").prop("checked",true);
					}
					else{
						$(".checkbox-data").prop("checked",false);
					}
				});
			});
		</script>
		';
		$this->mainfunction($this->params);
		return $this->str;
	}

	public function add_detail(){
		$post = $this->ci->input->post();
		$command=explode(",",$this->params['command']);
		$_cmdtext="";
		$_cmdtext.=(in_array("browse",$command)) ? "<a title='Refresh' role='button' href='javascript:loadcontent(\"".$this->maincontent."\",\"".$this->filename."\");' class='btn btn-brand btn-elevate btn-circle btn-icon'><i class='la la-refresh'></i></a></li>" : '' ;
		$valid = true;
		$slaveheader=$this->header_slave();
		$this->params['content']='
			<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="kt-font-brand flaticon-list"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						'.$this->params['name'].'
					</h3>
				</div>
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<div class="kt-portlet__head-actions">
							'.$_cmdtext.'
						</div>
					</div>
				</div>
			</div>
			<div class="kt-portlet__body">
				<table class="table table-condensed table-border slave-header">'.$slaveheader.'</table>';
		$_primary="";
		$_foreign="";
		foreach(array_keys($this->params['fieldeditslave']) as $_cols){
			if(@$this->params['fieldeditslave'][$_cols]['type']=="primarykey"){
				$_primary=$_cols;
			}
			if(@$this->params['fieldeditslave'][$_cols]['type']=="foreignkey"){
				$_foreign=$_cols;
			}
		}
		$command=explode(",",$this->params['slavecommand']);
		$_cmdtext="";
		$_cmdtext.=(in_array("browse",$command)) ? "<a title='Refresh' role='button' href='javascript:loadcontent(\"".$this->maincontent."\",\"".$this->filename."/browse_detail/pk/".$this->params['primarykeymaster']."/valpk/".$this->params['valprimarykeymaster']."\");' class='btn btn-brand btn-elevate btn-circle btn-icon btn-success'><i class='la la-refresh'></i></a>" : '' ;
		//$_cmdtext.=(in_array("add",$command)) ? "<li class='command-list'><a href='javascript:loadcontent(\"".$this->maincontent."\",\"".$this->filename."/add_detail/\");' class='btn btn-primary'><i class='icon-white icon icon-plus'></i></a></li>" : '' ;

		$this->params['content'].='
			<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="kt-font-brand flaticon-plus"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						Add '.$this->params['slavename'].'
					</h3>
				</div>
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<div class="kt-portlet__head-actions">
							'.$_cmdtext.'
						</div>
					</div>
				</div>
			</div>
			<div class="kt-portlet__body">
				<form class="form-horizontal" id="'.$this->formid.'">';
		foreach(array_keys($this->params['fieldeditslave']) as $_keyadd){
			$validation_message = "";
			$rsign = "";
			// Form Validation
			if( count($post) > 0){
				$this->params['fieldeditslave'][$_keyadd]['value'] = set_value($_keyadd);
			}
			if( isset($this->params['fieldeditslave'][$_keyadd]['validation']) ){
				$this->ci->form_validation->set_rules( $_keyadd, $this->ci->lang->line($_keyadd), $this->params['fieldeditslave'][$_keyadd]['validation'] );
				$arr_validation = explode("|", $this->params['fieldeditslave'][$_keyadd]['validation']);
				if( in_array("required", $arr_validation)) {
					$this->params['fieldeditslave'][$_keyadd]['is_required'] = "required";
				}
				if( !$this->ci->form_validation->run() ){ // Validation Error
					$validation_message = form_error($_keyadd);
					$valid = false;
				}
			}

			if(@$this->params['fieldeditslave'][$_keyadd]['is_required'] == true){
				$rsign = "<span class='requiredsign'>*</span>";
			}

			if(@$this->params['fieldeditslave'][$_keyadd]['hidden']==true){
				$this->params['content'].="<input type='hidden' value='".@$this->params['fieldeditslave'][$_keyadd]['value']."' name='".$_keyadd."' id='".$_keyadd."'/>";
			} else {
				$addon = "";
				if(isset($this->params['fieldeditslave'][$_keyadd]['help'])){
					// $addon = "<span class='input-group-append'>".$this->params['fieldeditslave'][$_keyadd]['help']."</span>";
					$addon = "<div class='input-group-append'><span class='input-group-text'>".$this->params['fieldeditslave'][$_keyadd]['help']."</span></div>";
				}
				$isreadonly=(@$this->params['fieldeditslave'][$_keyadd]['readonly']==true) ? "readonly" : null;
				$isdisabled=(@$this->params['fieldeditslave'][$_keyadd]['disabled']==true) ? "disabled" : null;
				$_type=(!isset($this->params['fieldeditslave'][$_keyadd]['type'])) ? "text" : $this->params['fieldeditslave'][$_keyadd]['type'] ;
				if($_type=="primarykey"){
					$_type="text";
				}

				if($_type=="textarea"){
					$this->params['content'].="
					<div class='form-group row'>
						<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd) . $rsign ."</label>
						<div class='".@$this->params['fieldeditslave'][$_keyadd]['class']."'>
							<div class='input-group'>
								<textarea ".$isreadonly." ".$isdisabled."  name='".$_keyadd."' id='".$_keyadd."' placeholder='".@$this->params['fieldeditslave'][$_keyadd]['placeholder']."' ".@$this->params['fieldeditslave'][$_keyadd]['is_required']." class='form-control' maxlength='".@$this->params['fieldeditslave'][$_keyadd]['maxlength']."'>".@$this->params['fieldeditslave'][$_keyadd]['value']."</textarea>
								".$addon."
							</div>
							".$validation_message."
						</div>
					</div>";
				}
				elseif( $_type == "separator"){
					$this->params['content'].="
						<div class='form-separator'>".$this->ci->lang->line($_keyadd)."</div>
					";
				}
				elseif($_type=="wysiwyg"){
					$this->params['content'].="
					<div class='form-group row'>
						<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd) . $rsign . "</label>
						<div class='".@$this->params['fieldeditslave'][$_keyadd]['class']."'>
							<div class='input-group'>
								<textarea ".$isreadonly." ".$isdisabled."  name='".$_keyadd."' id='".$_keyadd."' placeholder='".@$this->params['fieldeditslave'][$_keyadd]['placeholder']."' ".@$this->params['fieldeditslave'][$_keyadd]['is_required']." class='form-control' maxlength='".@$this->params['fieldeditslave'][$_keyadd]['maxlength']."' novalidate>".@$this->params['fieldeditslave'][$_keyadd]['value']."</textarea>
								".$addon."
							</div>
							".$validation_message."
						</div>
					</div>
					<script type='text/javascript'>
						$(document).ready(function(){
							var editor_id = '".$_keyadd."';
							tinymce.EditorManager.execCommand('mceRemoveEditor',true, editor_id);
							tinymce.EditorManager.execCommand('mceAddEditor',true, editor_id);
						});
					</script>";
				}
				elseif($_type=="dropdownquery"){
					$option="";
					$optgroup="";
					$multiple="";
					foreach($this->params['fieldeditslave'][$_keyadd]['sourcequery'] as $opdata){
						if(isset($opdata['labeldt'])){
							if($optgroup!=$opdata['labeldt']){
								$option.="<optgroup label='".$opdata['labeldt']."'>";
								$optgroup=$opdata['labeldt'];
							}
						}
						$selected=($opdata['keydt']==@$this->params['fieldeditslave'][$_keyadd]['value']) ? "selected" : null ;
						$option.="<option ".$selected." value='".$opdata['keydt']."'>".$opdata['valuedt']."</option>";
					}
					$name = "name='".$_keyadd."'";
					if( @$this->params['fieldeditslave'][$_keyadd]['multiple'] ){
						$multiple = "multiple='multiple'";
						$name = "name='".$_keyadd."[]'";
					}
					$this->params['content'].="
					<div class='form-group row'>
						<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd) . $rsign ."</label>
						<div class='".@$this->params['fieldeditslave'][$_keyadd]['class']."'>
							<div class='input-group'>
								<select ".$isreadonly." ".$isdisabled."  ".$multiple." ".$name." id='".$_keyadd."' class='form-control' ".@$this->params['fieldeditslave'][$_keyadd]['is_required'].">
									".$option."
								</select>
								".$addon."
							</div>
							".$validation_message."
						</div>
					</div>";
					// if( $multiple != "" ){
					// 	$this->params['content'] .= "
					// 		<script type='text/javascript'>
					// 		$(function() {
					//         $('#".$_keyadd."').multipleSelect({
					//             width: '100%'
					//         });
					//     });
					// 	</script>";
					// }

				}
				elseif($_type=="dropdownarray"){
					$option="";
					$multiple="";
					foreach(array_keys($this->params['fieldeditslave'][$_keyadd]['sourcearray']) as $opdata){
						$selected=($opdata==@$this->params['fieldeditslave'][$_keyadd]['value']) ? "selected" : null ;
						$option.="<option ".$selected." value='".$opdata."'>".$this->params['fieldeditslave'][$_keyadd]['sourcearray'][$opdata]."</option>";
					}
					$name = "name='".$_keyadd."'";
					if( @$this->params['fieldeditslave'][$_keyadd]['multiple'] ){
						$multiple = "multiple='multiple'";
						$name = "name='".$_keyadd."[]'";
					}
					$this->params['content'].="
					<div class='form-group row'>
						<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd) . $rsign . "</label>
						<div class='".@$this->params['fieldeditslave'][$_keyadd]['class']."'>
							<div class='input-group'>
								<select ".$isreadonly." ".$isdisabled."  ".$multiple." ".$name." id='".$_keyadd."' class='form-control' ".@$this->params['fieldeditslave'][$_keyadd]['is_required'].">
									".$option."
								</select>
								".$addon."
							</div>
							".$validation_message."
						</div>
					</div>";
					// if( $multiple != "" ){
					// 	$this->params['content'] .= "
					// 		<script type='text/javascript'>
					// 		$(function() {
					//         $('#".$_keyadd."').multipleSelect({
					//             width: '100%'
					//         });
					//     });
					// 	</script>";
					// }
				}
				elseif($_type=="checkgroup"){
					$option="";
					foreach($this->params['fieldeditslave'][$_keyadd]['sourcequery'] as $opdata){
						$selected=($opdata['keydt']==@$this->params['fieldeditslave'][$_keyadd]['value']) ? "checked" : null ;
						$option.="<label><input class='form-control' type='checkbox' name='CHECK_".$opdata['keydt']."' ".$selected.">
						<span class='lbl' style='margin-left:5px;'> ".$opdata['valuedt']." </span></label>";
					}
					$this->params['content'].="
					<div class='form-group row'>
						<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($this->ci->lang->line($_keyadd)) . $rsign ."</label>
						<div class='".@$this->params['fieldeditslave'][$_keyadd]['class']."'>
							<div class='input-group'>
								".$option."
								".$addon."
							</div>
							".$validation_message."
						<?div>
					</div>";
				}
				elseif($_type=="file"){
					$img = "<img src='' class='thumbnail' style='width:100%;' />";
					$this->params['content'].="<div class='form-group row'>
						<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd) . $rsign . "</label>
						<div class='".@$this->params['fieldeditslave'][$_keyadd]['class']."'>
							<div class='input-group'>
								<input class='custom-file-input' ".@$this->params['fieldeditslave'][$_keyadd]['required']." type='file' accept='application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf, image/*, video/*' id='FILE_".$_keyadd."' name='FILE_".$_keyadd."' value='' />".$addon."
								<label class='custom-file-label' for='FILE_".$_keyadd."'></label>
								<input type='hidden' id='".$_keyadd."' name='".$_keyadd."' value='' />
							</div>
							<div class='img-container' id='IMG_".$_keyadd."'>".$img."</div>
						</div>
						</div>
						<script type='text/javascript'>
							$('#FILE_".$_keyadd."').on('change', function(){
								// $('#IMG_".$_keyadd."').remove();
								var reader = new FileReader();
						    reader.onload = function (e) {
						    	if( e.target.result.substring(5,14) == 'video/mp4' || e.target.result.substring(5,14) == 'video/avi' ){
						    		var loaded = '<video width=\"100%\" height=\"400px\" controls><source src=\"' + e.target.result + '\" type=\"' + e.target.result.substring(5,14) + '\">Browser Anda tidak support dengan pemutar video.</video>';
						    		$('#IMG_".$_keyadd."').html( loaded );
						    	} else if(e.target.result.substring(5,20) == 'application/pdf') {
						    		var loaded = '<object data=\"' + e.target.result + '\" type=\"application/pdf\" width=\"100%\" height=\"600px\"><p><b>Gagal Memuat PDF</b>: Browser Ini tidak mendukung penampil PDF.</p></object>';
										$('#IMG_".$_keyadd."').html( loaded );
						    	} else {
					    			$('#IMG_".$_keyadd."').html('<img class=\"thumbnail\" src=\"' + e.target.result + '\" style=\"width:100%;\" />');
						    	}
						    };

						    // read the image file as a data URL.
						    reader.readAsDataURL(this.files[0]);

								ajaxupload('".$_keyadd."');
								return false;
							});
						</script>
						";
				}
				elseif($_type == "map"){
					$default_position = $this->configurations['MAP_DEFAULT_LOCATION'];
					$latlng = explode(",", $default_position);
					$default_lat = $latlng[0];
					$default_lng = $latlng[1];
					$addressChain = "";
						if( isset($this->params['fieldadd'][$_keyadd]['address_id'] )){
							$id = $this->params['fieldadd'][$_keyadd]['address_id'];
							$addressChain = "
							var input = document.getElementById('".$id."');
							var autocomplete = new google.maps.places.Autocomplete(input);
							autocomplete.bindTo('bounds', map);
							autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
							
							/*var infowindow = new google.maps.InfoWindow();
			        var infowindowContent = document.getElementById('infowindow-content');
			        infowindow.setContent(infowindowContent);
			        var marker = new google.maps.Marker({
			          map: map,
			          anchorPoint: new google.maps.Point(0, -29)
			        });*/

			        autocomplete.addListener('place_changed', function() {
		          
		          /*infowindow.close();*/

			          marker.setVisible(false);
			          var place = autocomplete.getPlace();
			          if (!place.geometry) {
			            // User entered the name of a Place that was not suggested and
			            // pressed the Enter key, or the Place Details request failed.
			            window.alert('No details available for input: ' + place.name);
			            return;
			          }

		          // If the place has a geometry, then present it on a map.

		          if (place.geometry.viewport) {
		            map.fitBounds(place.geometry.viewport);
		          } else {
		            map.setCenter(place.geometry.location);
		            map.setZoom(17);  // Why 17? Because it looks good.
		          }

		          marker.setPosition(place.geometry.location);
		          marker.setVisible(true);
		          var lat = place.geometry.location.lat();
		          var lng = place.geometry.location.lng();
							$('#".$_keyadd."').val(eval(lat) + ',' + eval(lng));

		          var address = '';
		          if (place.address_components) {
		            address = [
		              (place.address_components[0] && place.address_components[0].short_name || ''),
		              (place.address_components[1] && place.address_components[1].short_name || ''),
		              (place.address_components[2] && place.address_components[2].short_name || '')
		            ].join(' ');
		          }

		          /*
		          infowindowContent.children['place-icon'].src = place.icon;
		          infowindowContent.children['place-name'].textContent = place.name;
		          infowindowContent.children['place-address'].textContent = address;
		          infowindow.open(map, marker);
		          */
		        });
							";
						}
					$this->params['content'].="<div class='form-group row'>
						<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd). $rsign . "</label>
						<div class='".@$this->params['fieldeditslave'][$_keyadd]['class']."'>
							<div class='input-group'>
								<input type='text' value='".$default_lat.",".$default_lng."' class='form-control' id='".$_keyadd."' name='".$_keyadd."' value='' />
							</div>
							".$validation_message."
							<div class='map-container' id='MAP_".$_keyadd."' style='height:300px;'></div>
							<p class='text-primary mt-2 text-center'>Geser Titik Merah untuk Mendapatkan lokasi Koordinat</p>
						</div>
						</div>
						<script type='text/javascript'>
							var map, infoWindow;
							".$_keyadd."_initMap();
							function ".$_keyadd."_initMap() {
								var location = { lat: ".$default_lat.", lng: ".$default_lng." };
						    var map = new google.maps.Map(document.getElementById('MAP_".$_keyadd."'), {
						      zoom: 18,
						      center: location
						    });

						    ".$addressChain."

						    // Add a marker at the center of the map.
						    var marker = new google.maps.Marker({
						      position: location,
						      title: 'Pilih Lokasi',
						      map: map,
						      draggable: true,
						   		animation: google.maps.Animation.DROP
						    });
						    google.maps.event.addListener(marker, 'dragend', function (event) {
						    	var lat = this.getPosition().lat();
						    	var lng = this.getPosition().lng();
							    $('#".$_keyadd."').val(eval(lat) + ',' + eval(lng));
								});
						  }
						</script>";
				}
				else {
					$step = "";
					$dateclass="";
					$append="";
					$js="";
					$min= isset($this->params['fieldeditslave'][$_keyadd]['min']) ? " min='".$this->params['fieldeditslave'][$_keyadd]['min']."' " : null;
					$max= isset($this->params['fieldeditslave'][$_keyadd]['max']) ? " max='".$this->params['fieldeditslave'][$_keyadd]['max']."' " : null;
					$currencyAdd = "";
					$currencyClass = "";
					if($_type=="date"){
						$_type="text";
						$dateclass="date-picker";
						$append="<div class='input-group-append'><span class='input-group-text'><i class='la la-calendar'></i></span></div>";
						if(count($post) > 0){
							if($post[$_keyadd] == ""){
								unset($post[$_keyadd]);
							}
						}
					}
					elseif($_type=="time"){
						$_type="text";
						$dateclass="time-picker";
						$append="<div class='input-group-append'><span class='input-group-text'><i class='la la-clock-o'></i></span></div>
							<script type='text/javascript'>
								$('#".$_keyadd.", #".$_keyadd."_modal').timepicker({
				            minuteStep: 1,
				            defaultTime: '',
				            showSeconds: true,
				            showMeridian: false,
				            snapToStep: true
				        });
							</script>
						";
					}
					elseif($_type=="datetime"){
						$_type="text";
						$dateclass="datetime-picker";
						$append="<div class='input-group-append'><span class='input-group-text'><i class='la la-calendar'></i></span></div>";
					}
					elseif($_type=="decimal"){
						$_type="number";
						$step = "step='0.01'";
					}
					elseif($_type=="currency"){
						$_type="number";
						$step = "step='0.01'";
						$currencyAdd = " data-number-to-fixed='2' data-number-stepfactor='100'";
						$currencyClass = " currency";
					}
					elseif($_type=="tagsinput"){
						$js = "<script type='text/javascript'>
							$('#".$_keyadd."').tagsinput();
						</script>";
						$_type = "text";
					}
					$this->params['content'].="<div class='form-group row'>
						<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd) . $rsign . "</label>
						<div class='".$dateclass." ".@$this->params['fieldeditslave'][$_keyadd]['class']."'>
							<div class='input-group'>
								<input ".$currencyAdd." ".$min." ".$max." ".$isreadonly." ".$isdisabled."  value='".@$this->params['fieldeditslave'][$_keyadd]['value']."' type='".$_type."' ".$step." name='".$_keyadd."' id='".$_keyadd."' placeholder='".@$this->params['fieldeditslave'][$_keyadd]['placeholder']."' ".@$this->params['fieldeditslave'][$_keyadd]['is_required']." ".$isreadonly." class='form-control ".$currencyClass."' maxlength='".@$this->params['fieldeditslave'][$_keyadd]['maxlength']."' />".$append."
								".$addon."
							</div>
							".$validation_message."
						</div>
					</div>" . $js;
				}
			}
		}
		$this->params['content'].="<div class='form-group row'>
			<div class='offset-md-2 col-md-6'>
				<button class='btn btn-pill btn-primary' type='submit'><i class='la la-save'></i>".$this->ci->lang->line("submit")."</button>
					<button class='btn btn-pill btn-danger' type='reset'><i class='la la-refresh'></i>".$this->ci->lang->line("reset")."</button>
			</div>
		</div>";

		$this->params['content'].="</form></div></div>";
		$this->params['content'].='</table>';
		$this->params['content'].='</div></div>
		<script type="text/javascript">
			$(document).ready(function(){
				$(this).updatePolyfill();
				webshim.activeLang("id");
				webshims.setOptions("forms-ext", {
				    replaceUI: "auto",
				    types: "number"
				});
				webshims.polyfill("forms forms-ext");

				$(".select2 > .input-group > select").select2({
					width: "100%",
					placeholder: $(this).attr("placeholder"),
					allowClear: Boolean($(this).data("allow-clear")),
				});
				$(".date-picker input").datepicker({
					format: "yyyy-mm-dd",
					autoclose: true,
				});
				$(".datetime-picker input").datetimepicker();

				$("#'.$this->formid.'").submit(function(){
					$("#'.$this->maincontent.'").html("<div class=\"text-center spin-loading\"><img src=\"" + base_url + "assets/img/spin.gif\" /></div>");
  				var datapost=$(this).serialize();
					var target="'.$this->filename.'/add_detail/pk/'.$this->params['primarykeymaster'].'/valpk/'.$this->params['valprimarykeymaster'].'/";
					loadpost("'.$this->maincontent.'", target, datapost);
					return false;
				});
			});
		</script>
		';
		// Inserting Data POst
		if( count($post) > 0 && $valid == true){
			foreach($this->params['fieldeditslave'] as $_keyadd => $_params){
				if(@$_params['type'] == "separator"){
					unset($post[$_keyadd]);
				}
				if(@$_params['disabled'] == true){
					unset($post[$_keyadd]);
				}
			}
			if( isset($this->params['unset_post']) ){
				$this->params['unset_post'] = explode(",", $this->params['unset_post']);
				foreach( $this->params['unset_post'] as $unset){
					unset($post[$unset]);
				}
			}
			$ins = $this->ci->db->insert($this->params['table-slave'],$post);
			if( $ins == 1 ){
				$this->params['content'] = '<script type="text/javascript">
					loadcontent("'.$this->maincontent.'","'.$this->filename.'/add_detail/pk/'.$this->params['primarykeymaster'].'/valpk/'.$this->params['valprimarykeymaster'].'/", true);
				</script>';
			}
		}
		$this->mainfunction($this->params);
		return $this->str;
	}

	public function edit_detail(){
		$post = $this->ci->input->post();
		$valid = true;
		$command=explode(",",$this->params['command']);
		$_cmdtext="";
		$_cmdtext.=(in_array("browse",$command)) ? "<a title='Refresh' role='button' href='javascript:loadcontent(\"".$this->maincontent."\",\"".$this->filename."\");' class='btn btn-brand btn-elevate btn-circle btn-icon'><i class='la la-refresh'></i></a>" : '' ;
		$slaveheader=$this->header_slave();
		$this->params['content']='
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="kt-font-brand flaticon-list"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						'.$this->params['name'].'
					</h3>
				</div>
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<div class="kt-portlet__head-actions">
							'.$_cmdtext.'
						</div>
					</div>
				</div>
			</div>
			<div class="kt-portlet__body">
			<table class="table table-condensed table-border slave-header">'.$slaveheader.'</table>';
		$_primary="";
		$_foreign="";
		foreach(array_keys($this->params['fieldeditslave']) as $_cols){
			if(@$this->params['fieldeditslave'][$_cols]['type']=="primarykey"){
				$_primary=$_cols;
			}
			if(@$this->params['fieldeditslave'][$_cols]['type']=="foreignkey"){
				$_foreign=$_cols;
			}
		}
		$command=explode(",",$this->params['slavecommand']);
		$_cmdtext="";
		$_cmdtext.=(in_array("browse",$command)) ? "<a title='Refresh' role='button' href='javascript:loadcontent(\"".$this->maincontent."\",\"".$this->filename."/browse_detail/pk/".$this->params['primarykeymaster']."/valpk/".$this->params['valprimarykeymaster']."\");' class='btn btn-brand btn-elevate btn-circle btn-icon btn-success'><i class='la la-refresh'></i></a>" : '' ;
		$keySelect = array();
		foreach(array_keys($this->params['fieldeditslave']) as $_keyadd){
			if( @$this->params['fieldeditslave'][$_keyadd]['type'] != "separator"){
				$keySelect[$_keyadd] = $this->params['fieldeditslave'][$_keyadd];
			}
		}
		$query="SELECT ".implode(", ",array_keys($keySelect))." FROM ".$this->params['table-slave'];
		if(isset($this->urisegments['pk']) && isset($this->urisegments['valpk'])){
			$query.=" WHERE ".$this->urisegments['pk']."='".urldecode($this->urisegments['valpk'])."'";
		}
		$query=$this->ci->db->query($query)->result_array();
		$data=$query[0];
		$this->params['content'].='
		<div class="kt-portlet kt-portlet--mobile">
			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="kt-font-brand flaticon-edit"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						Edit '.$this->params['slavename'].'
					</h3>
				</div>
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<div class="kt-portlet__head-actions">
							'.$_cmdtext.'
						</div>
					</div>
				</div>
			</div>
			<div class="kt-portlet__body">
    <form class="form-horizontal" id="'.$this->formid.'">';
		foreach(array_keys($this->params['fieldeditslave']) as $_keyadd){
			$addon = "";
			$rsign = "";
			$validation_message = "";
			// Form Validation
			if( count($post) > 0){
				$data[$_keyadd] = set_value($_keyadd);
			}
			if( isset($this->params['fieldeditslave'][$_keyadd]['validation']) ){
				$this->ci->form_validation->set_rules( $_keyadd, $this->ci->lang->line($_keyadd), $this->params['fieldeditslave'][$_keyadd]['validation'] );
				$arr_validation = explode("|", $this->params['fieldeditslave'][$_keyadd]['validation']);
				if( in_array("required", $arr_validation)) {
					$this->params['fieldeditslave'][$_keyadd]['is_required'] = "required";
				}
				if( !$this->ci->form_validation->run() ){ // Validation Error
					$validation_message = form_error($_keyadd);
					$valid = false;
				}
			}
			if(isset($this->params['fieldeditslave'][$_keyadd]['help'])){
				// $addon = "<span class='input-group-append'>".$this->params['fieldeditslave'][$_keyadd]['help']."</span>";
				$addon = "<div class='input-group-append'><span class='input-group-text'>".$this->params['fieldeditslave'][$_keyadd]['help']."</span></div>";
			}
			if(@$this->params['fieldeditslave'][$_keyadd]['is_required'] == true){
				$rsign = "<span class='requiredsign'>*</span>";
			}
			$isreadonly=(@$this->params['fieldeditslave'][$_keyadd]['readonly']==true) ? "readonly" : null;
			$isdisabled=(@$this->params['fieldeditslave'][$_keyadd]['disabled']==true) ? "disabled" : null;
			if(@$this->params['fieldeditslave'][$_keyadd]['hidden']==true){
				$this->params['content'].="<input type='hidden' name='".$_keyadd."' id='".$_keyadd."' value='".$data[$_keyadd]."' />";
			}
			else {
				$_type=(!isset($this->params['fieldeditslave'][$_keyadd]['type'])) ? "text" : $this->params['fieldeditslave'][$_keyadd]['type'] ;
				$isreadonly=($_type=="primarykey") ? "readonly" : $isreadonly;
				if($_type=="primarykey"){
					$_type="text";
				}
				if($_type=="dropdown"){

				}
				elseif( $_type == "separator"){
					$this->params['content'].="
						<div class='form-separator'>".$this->ci->lang->line($_keyadd)."</div>
					";
				}
				elseif($_type=="textarea"){
					$this->params['content'].="<div class='form-group row'>
						<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd). $rsign . "</label>
						<div class='".@$this->params['fieldeditslave'][$_keyadd]['class']."'>
							<div class='input-group'>
								<textarea ".$isreadonly." ".$isdisabled."  name='".$_keyadd."' id='".$_keyadd."' placeholder='".@$this->params['fieldeditslave'][$_keyadd]['placeholder']."' ".@$this->params['fieldeditslave'][$_keyadd]['is_required']." class='form-control' maxlength='".@$this->params['fieldeditslave'][$_keyadd]['maxlength']."'>".htmlentities($data[$_keyadd], ENT_QUOTES)."</textarea>
								".$addon."
							</div>
						</div>
					</div>";
				}
				elseif($_type=="wysiwyg"){
					$this->params['content'].="<div class='form-group row'>
						<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd) . $rsign."</label>
						<div class='".@$this->params['fieldeditslave'][$_keyadd]['class']."'>
							<div class='input-group'>
								<textarea ".$isreadonly." ".$isdisabled."  name='".$_keyadd."' id='".$_keyadd."' placeholder='".@$this->params['fieldeditslave'][$_keyadd]['placeholder']."' ".@$this->params['fieldeditslave'][$_keyadd]['is_required']." class='form-control' maxlength='".@$this->params['fieldeditslave'][$_keyadd]['maxlength']."' novalidate>".htmlentities($data[$_keyadd], ENT_QUOTES)."</textarea>
								".$addon."
							</div>
						</div>
					</div>
					<script type='text/javascript'>
						$(document).ready(function(){
							var editor_id = '".$_keyadd."';
							tinymce.EditorManager.execCommand('mceRemoveEditor',true, editor_id);
							tinymce.EditorManager.execCommand('mceAddEditor',true, editor_id);
						});
					</script>
					";
				}
				elseif($_type=="dropdownquery"){
					$option="";
					$selected="";
					$optgroup="";
					$multiple="";
					
					$multiplevalues = array();
					$name = "name='".$_keyadd."'";
					if( @$this->params['fieldeditslave'][$_keyadd]['multiple'] ){
						$multiple = "multiple='multiple'";
						$name = "name='".$_keyadd."[]'";
						// $multiplevalues = json_decode($data[$_keyadd]);
						$multiplevalues = explode(",",$data[$_keyadd]);
					}
					foreach($this->params['fieldeditslave'][$_keyadd]['sourcequery'] as $opdata){
						
						if(isset($opdata['labeldt'])){
							if($optgroup!=$opdata['labeldt']){
								$option.="<optgroup label='".$opdata['labeldt']."'>";
								$optgroup=$opdata['labeldt'];
							}
						}
						if($multiple != ""){
							$selected = @in_array($opdata['keydt'], $multiplevalues) ? "selected" : null;
						} else {
							$selected = ($data[$_keyadd]==$opdata['keydt']) ? "selected" : null;
						}
						$option.="<option ".$selected." value='".$opdata['keydt']."'>".$opdata['valuedt']."</option>";
					}

					$this->params['content'].="<div class='form-group row'>
						<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd) . $rsign ."</label>
						<div class='".@$this->params['fieldeditslave'][$_keyadd]['class']."'>
							<div class='input-group'>
								<select  data-value='".$data[$_keyadd]."' ".$isreadonly." ".$isdisabled."  name='".$_keyadd."' id='".$_keyadd."' class='form-control' ".@$this->params['fieldeditslave'][$_keyadd]['is_required'].">
									".$option."
								</select>
								".$addon."
							</div>
						</div>
					</div>";
					// if( $multiple != "" ){
					// 	$this->params['content'] .= "
					// 		<script type='text/javascript'>
					// 		$(function() {
					//         $('#".$_keyadd."').multipleSelect({
					//             width: '100%'
					//         });
					//     });
					// 	</script>";
					// }
				}
				elseif($_type=="dropdownarray"){
					$option="";
					$multiple="";
					$multiplevalues = array();
					$name = "name='".$_keyadd."'";
					if( @$this->params['fieldeditslave'][$_keyadd]['multiple'] ){
						$multiple = "multiple='multiple'";
						$name = "name='".$_keyadd."[]'";
						// $multiplevalues = json_decode($data[$_keyadd]);
						$multiplevalues = explode(",",$data[$_keyadd]);
					}
					foreach(array_keys($this->params['fieldeditslave'][$_keyadd]['sourcearray']) as $opdata){
						if($multiple != ""){
							$selected = @in_array($opdata, $multiplevalues) ? "selected" : null;
						} else {
							$selected = ($data[$_keyadd]==$opdata) ? "selected" : null;
						}
						$option.="<option ".$selected." value='".$opdata."'>".$this->params['fieldeditslave'][$_keyadd]['sourcearray'][$opdata]."</option>";
					}
					$this->params['content'].="<div class='form-group row'>
						<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd) . $rsign ."</label>
						<div class='".@$this->params['fieldeditslave'][$_keyadd]['class']."'>
							<div class='input-group'>
								<select data-value='".$data[$_keyadd]."' ".$isreadonly." ".$isdisabled."  ".$multiple." ".$name." id='".$_keyadd."' class='form-control' ".@$this->params['fieldeditslave'][$_keyadd]['is_required'].">
									".$option."
								</select>
								".$addon."
							</div>
						</div>
					</div>";
					if( $multiple != "" ){
						$this->params['content'] .= "
							<script type='text/javascript'>
							$(function() {
					        $('#".$_keyadd."').multipleSelect({
					            width: '100%'
					        });
					    });
						</script>";
					}
				}
				elseif($_type=="checkgroup"){
					$option="";
					$arrdata=explode(",",$data[$_keyadd]);
					foreach($this->params['fieldeditslave'][$_keyadd]['sourcequery'] as $opdata){
						$selected=(array_search($opdata['keydt'],$arrdata)) ? "checked" : null ;
						$option.="<label><input type='checkbox' class='form-control' name='CHECK_".$opdata['keydt']."' ".$selected.">
						<span class='lbl' style='margin-left:5px;'> ".$opdata['valuedt']." </span></label>";
					}
					$this->params['content'].="<div class='form-group row'>
						<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd) . $rsign ."</label>
						<div class='".@$this->params['fieldeditslave'][$_keyadd]['class']."'>
							<div class='input-group'>
								".$option."
								".$addon."
							</div>
						</div>
					</div>";
				}
				elseif($_type=="file"){
					$img = "<img src='".base_url("uploads/noimage.png")."' class='thumbnail' style='width:100%;' />";
					if($data[$_keyadd] <> ""){
						$img = "<img src='".base_url("uploads/" . $data[$_keyadd])."' class='thumbnail' style='width:100%;' />";
					}
					$extension = substr($data[$_keyadd],-3);
					if( $extension == "pdf"){
						$img = '<object data="'.base_url("uploads/" . $data[$_keyadd]).'" type="application/pdf" width="100%" height="100%">
									   <p><b>Gagal Memuat PDF</b>: Browser Ini tidak mendukung penampil PDF. Silahkan Download File Untuk melihatnya : <a target="_blank" href="'.base_url("uploads/" . $data[$_keyadd]).'">Download PDF</a>.</p>
									</object><style>.img-container{height: 300px}</style>';
					}
					if( $extension == "mp4" || $extension == "avi"){
						$img = '<video width="100%" height="400px" controls>
									  <source src="'.base_url("uploads/" . $data[$_keyadd]).'" type="video/'.$extension.'">
										Browser Anda tidak support dengan pemutar video.
									</video>';
					}
					$this->params['content'].="<div class='form-group row'>
						<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd) . $rsign ."</label>
						<div class='".@$this->params['fieldeditslave'][$_keyadd]['class']."'>
							<div class='input-group'>
								<input class='custom-file-input' ".@$this->params['fieldeditslave'][$_keyadd]['required']." type='file' accept='application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf, image/*, video/*' id='FILE_".$_keyadd."' name='FILE_".$_keyadd."' value='".$data[$_keyadd]."' />".$addon."
								<label class='custom-file-label' for='FILE_".$_keyadd."'></label>
								<input type='hidden' id='".$_keyadd."' name='".$_keyadd."' value='".$data[$_keyadd]."' />
							</div>
							<div class='img-container' id='IMG_".$_keyadd."'>".$img."</div>
						</div>
						</div>
						<script type='text/javascript'>
							$('#FILE_".$_keyadd."').on('change', function(){
								// $('#IMG_".$_keyadd."').remove();
								var reader = new FileReader();
						    reader.onload = function (e) {
						    	if( e.target.result.substring(5,14) == 'video/mp4' || e.target.result.substring(5,14) == 'video/avi' ){
						    		var loaded = '<video width=\"100%\" height=\"400px\" controls><source src=\"' + e.target.result + '\" type=\"' + e.target.result.substring(5,14) + '\">Browser Anda tidak support dengan pemutar video.</video>';
						    		$('#IMG_".$_keyadd."').html( loaded );
						    	} else if(e.target.result.substring(5,20) == 'application/pdf') {
						    		var loaded = '<object data=\"' + e.target.result + '\" type=\"application/pdf\" width=\"100%\" height=\"600px\"><p><b>Gagal Memuat PDF</b>: Browser Ini tidak mendukung penampil PDF.</p></object>';
										$('#IMG_".$_keyadd."').html( loaded );
						    	} else {
					    			$('#IMG_".$_keyadd."').html('<img class=\"thumbnail\" src=\"' + e.target.result + '\" style=\"width:100%;\" />');
						    	}
						    };

						    // read the image file as a data URL.
						    reader.readAsDataURL(this.files[0]);

								ajaxupload('".$_keyadd."');
								return false;
							});
						</script>
						";
				}
				elseif($_type == "map"){
					$default_position = ($data[$_keyadd] == "") ? $this->configurations['MAP_DEFAULT_LOCATION'] : $data[$_keyadd];
					$latlng = explode(",", $default_position);
					$default_lat = $latlng[0];
					$default_lng = $latlng[1];
					$addressChain = "";
						if( isset($this->params['fieldadd'][$_keyadd]['address_id'] )){
							$id = $this->params['fieldadd'][$_keyadd]['address_id'];
							$addressChain = "
							var input = document.getElementById('".$id."');
							var autocomplete = new google.maps.places.Autocomplete(input);
							autocomplete.bindTo('bounds', map);
							autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
							
							/*var infowindow = new google.maps.InfoWindow();
			        var infowindowContent = document.getElementById('infowindow-content');
			        infowindow.setContent(infowindowContent);
			        var marker = new google.maps.Marker({
			          map: map,
			          anchorPoint: new google.maps.Point(0, -29)
			        });*/

			        autocomplete.addListener('place_changed', function() {
		          
		          /*infowindow.close();*/

			          marker.setVisible(false);
			          var place = autocomplete.getPlace();
			          if (!place.geometry) {
			            // User entered the name of a Place that was not suggested and
			            // pressed the Enter key, or the Place Details request failed.
			            window.alert('No details available for input: ' + place.name);
			            return;
			          }

		          // If the place has a geometry, then present it on a map.

		          if (place.geometry.viewport) {
		            map.fitBounds(place.geometry.viewport);
		          } else {
		            map.setCenter(place.geometry.location);
		            map.setZoom(17);  // Why 17? Because it looks good.
		          }

		          marker.setPosition(place.geometry.location);
		          marker.setVisible(true);
		          var lat = place.geometry.location.lat();
		          var lng = place.geometry.location.lng();
							$('#".$_keyadd."').val(eval(lat) + ',' + eval(lng));

		          var address = '';
		          if (place.address_components) {
		            address = [
		              (place.address_components[0] && place.address_components[0].short_name || ''),
		              (place.address_components[1] && place.address_components[1].short_name || ''),
		              (place.address_components[2] && place.address_components[2].short_name || '')
		            ].join(' ');
		          }

		          /*
		          infowindowContent.children['place-icon'].src = place.icon;
		          infowindowContent.children['place-name'].textContent = place.name;
		          infowindowContent.children['place-address'].textContent = address;
		          infowindow.open(map, marker);
		          */
		        });
							";
						}
					$this->params['content'].="<div class='form-group row'>
						<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd). $rsign . "</label>
						<div class='".@$this->params['fieldeditslave'][$_keyadd]['class']."'>
							<div class='input-group'>
								<input type='text' class='form-control' id='".$_keyadd."' name='".$_keyadd."' value='".$default_position."' />
							</div>
							".$validation_message."
							<div class='map-container' id='MAP_".$_keyadd."' style='height:300px;'></div>
						</div>
						</div>
						<script type='text/javascript'>
							var map, infoWindow;
							".$_keyadd."_initMap();
							function ".$_keyadd."_initMap() {
								var location = { lat: ".$default_lat.", lng: ".$default_lng." };
						    var map = new google.maps.Map(document.getElementById('MAP_".$_keyadd."'), {
						      zoom: 18,
						      center: location
						    });

						    ".$addressChain."

						    // Add a marker at the center of the map.
						    var marker = new google.maps.Marker({
						      position: location,
						      title: 'Pilih Lokasi',
						      map: map,
						      draggable: true,
						   		animation: google.maps.Animation.DROP
						    });
						    google.maps.event.addListener(marker, 'dragend', function (event) {
						    	var lat = this.getPosition().lat();
						    	var lng = this.getPosition().lng();
							    $('#".$_keyadd."').val(eval(lat) + ',' + eval(lng));
								});
						  }
						</script>";
				}
				else {
					$step = "";
					$dateclass="";
					$append="";
					$js="";
					$min= isset($this->params['fieldeditslave'][$_keyadd]['min']) ? " min='".$this->params['fieldeditslave'][$_keyadd]['min']."' " : null;
					$max= isset($this->params['fieldeditslave'][$_keyadd]['max']) ? " max='".$this->params['fieldeditslave'][$_keyadd]['max']."' " : null;
					$currencyAdd = "";
					$currencyClass = "";
					$value=$data[$_keyadd];
					if($_type=="date"){
						$_type="text";
						$dateclass="date-picker";
						$append="<div class='input-group-append'><span class='input-group-text'><i class='la la-calendar'></i></span></div>";
						if(count($post) > 0){
							if($post[$_keyadd] == ""){
								unset($post[$_keyadd]);
							}
						}
					}
					elseif($_type=="time"){
						$_type="text";
						$dateclass="time-picker";
						$append="<div class='input-group-append'><span class='input-group-text'><i class='la la-clock-o'></i></span></div>
						<script type='text/javascript'>
							$('#".$_keyadd.", #".$_keyadd."_modal').timepicker({
			            minuteStep: 1,
			            defaultTime: '',
			            showSeconds: true,
			            showMeridian: false,
			            snapToStep: true
			        });
						</script>
						";
					}
					elseif($_type=="datetime"){
						$_type="text";
						$dateclass="datetime-picker";
						$append="<div class='input-group-append'><span class='input-group-text'><i class='la la-calendar'></i></span></div>";
					}
					elseif($_type=="password"){
						$value="";
					}
					elseif($_type=="decimal"){
						$_type="number";
						$step = "step='0.01'";
					}
					elseif($_type=="currency"){
						$_type="number";
						$step = "step='0.01'";
						$currencyAdd = " data-number-to-fixed='2' data-number-stepfactor='100'";
						$currencyClass = " currency";
					}
					elseif($_type=="tagsinput"){
						$js = "<script type='text/javascript'>
							$('#".$_keyadd."').tagsinput();
						</script>";
						$_type = "text";
					}
					$this->params['content'].="<div class='form-group row'>
						<label class='col-md-2' for='".$_keyadd."'>".$this->ci->lang->line($_keyadd) . $rsign ."</label>
						<div class='".$dateclass." ".@$this->params['fieldeditslave'][$_keyadd]['class']."'>
							<div class='input-group'>
								<input ".$currencyAdd." ".$min." ".$max." ".$isreadonly." ".$isdisabled." value='".$value."' type='".$_type."' ".$step." name='".$_keyadd."' id='".$_keyadd."' placeholder='".@$this->params['fieldeditslave'][$_keyadd]['placeholder']."' ".@$this->params['fieldeditslave'][$_keyadd]['is_required']." class='form-control ".$currencyClass."' maxlength='".@$this->params['fieldeditslave'][$_keyadd]['maxlength']."' />".$append."
								".$addon."
							</div>
							".$validation_message."
						</div>
					</div>" . $js;
				}
			}
		}
		$this->params['content'].="<div class='form-group row'>
			<div class='offset-md-2 col-md-6'>
				<button class='btn btn-pill btn-primary' type='submit'><i class='la la-save'></i>".$this->ci->lang->line("submit")."</button>
					<button class='btn btn-pill btn-danger' type='reset'><i class='la la-refresh'></i>".$this->ci->lang->line("reset")."</button>
			</div>
		</div>";
		$this->params['content'].="</form></div></div>";
		$this->params['content'].='</table>';
		$this->params['content'].='</div></div>
		<script type="text/javascript">
			$(document).ready(function(){
				$(this).updatePolyfill();
				webshim.activeLang("id");
				webshims.setOptions("forms-ext", {
				    replaceUI: "auto",
				    types: "number"
				});
				webshims.polyfill("forms forms-ext");

				$(".select2 > .input-group > select").select2({
					width: "100%",
					placeholder: $(this).attr("placeholder"),
					allowClear: Boolean($(this).data("allow-clear")),
				});
				$(".date-picker input").datepicker({
					format: "yyyy-mm-dd",
					autoclose: true,
				});
				$(".datetime-picker input").datetimepicker();
				$("#'.$this->formid.'").submit(function(){
					$("#'.$this->maincontent.'").html("<div class=\"text-center spin-loading\"><img src=\"" + base_url + "assets/img/spin.gif\" /></div>");
					var datapost=$(this).serialize();
					var target="'.$this->filename.'/edit_detail/pk/'.@$this->urisegments['pk'].'/valpk/'.@urlencode($this->urisegments['valpk']).'/fk/'.@$this->urisegments['fk'].'/valfk/'.@urlencode($this->urisegments['valfk']).'";
					loadpost("'.$this->maincontent.'", target, datapost);
					return false;
				});
			});
		</script>
		';
		// Updating Data POst
		if( count($post) > 0 && $valid == true){
			foreach($this->params['fieldeditslave'] as $_keyadd => $_params){
				if(@$_params['type'] == "separator"){
					unset($post[$_keyadd]);
				}
				if(@$_params['disabled'] == true){
					unset($post[$_keyadd]);
				}
			}
			if( isset($this->params['unset_post']) ){
				$this->params['unset_post'] = explode(",", $this->params['unset_post']);
				foreach( $this->params['unset_post'] as $unset){
					unset($post[$unset]);
				}
			}
			$upd=$this->ci->db->update($this->params['table-slave'],$post,array($this->urisegments['pk'] => urldecode($this->urisegments['valpk'])));
			if( $upd == 1 ){
				$this->params['content'] = '<script type="text/javascript">
					loadcontent("'.$this->maincontent.'","'.$this->filename.'/browse_detail/pk/'.@$this->urisegments['fk'].'ID/valpk/'.@urlencode($this->urisegments['valfk']).'/", true);
				</script>';
			}
		}
		$this->mainfunction($this->params);
		return $this->str;
	}
}
