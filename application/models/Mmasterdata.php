<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Mmasterdata extends CI_Model{
	private $logged = array();

	function __construct(){
		parent::__construct();
		$this->logged = $this->session->userdata("admin");
	}

	function getOrderSeq( $max = 100 ){
		$result =  array();
		for($i = 1; $i <= $max; $i++){
			$result[$i] = $i;
		}
		return $result;
	}

	function getMenu( $privilege ){
		return $this->db->select("menus.NAME, menus.URL, menus.ICON, menus.MENUID, menuprivileges.MENUPRIVILEGEID, menuprivileges.PARENTMENU, menuprivileges.MENU")
					->where("PRIVILEGE", $privilege)
					->join("menus", "MENU=MENUID", "LEFT")
					->join("privileges", "PRIVILEGE=PRIVILEGEID", "LEFT")
					->order_by("ORDERSEQ")
					->get("menuprivileges")->result();
	}

	function getChildMenu( $menuid, $privilegeid ){
		return $this->db->select("
						menus.NAME, menus.URL, menus.ICON, menus.MENUID, menuprivileges.MENUPRIVILEGEID, menuprivileges.PARENTMENU, menuprivileges.MENU
					")
					->where("PARENTMENU", $menuid)
					->where("PRIVILEGE", $privilegeid)
					->join("menus", "MENU=MENUID", "LEFT")
					->join("privileges", "PRIVILEGE=PRIVILEGEID", "LEFT")
					->order_by("ORDERSEQ")
					->get("menuprivileges")->result();
	}

	function getHomePage( $privilege ){
		$homepage = site_url();
		$privileges = $this->db->where("PRIVILEGEID", $privilege)->get("privileges")->result();
		if( count($privileges) > 0 ){
			$homepage .= $privileges[0]->HOMEDIR;
		}
		return $homepage;
	}

	function getAccessPrivilege( $privilege ){
		$directory = "";
		$privileges = $this->db->where("PRIVILEGEID", $privilege)->get("privileges")->result();
		if( count($privileges) > 0 ){
			$directory = $privileges[0]->HOMEDIR;
		}
		return $directory;
	}

	function getPrivilege(){
		$privileges = $this->db->get("privileges")->result();
		$result = array();
		foreach($privileges as $priv){
			$result[$priv->PRIVILEGEID] = $priv->NAME;
		}
		return $result;
	}

	function getMonth(){
		$month = array(
			'01' => "Januari",
			'02' => "Februari",
			'03' => "Maret",
			'04' => "April",
			'05' => "Mei",
			'06' => "Juni",
			'07' => "Juli",
			'08' => "Agustus",
			'09' => "September",
			'10' => "Oktober",
			'11' => "November",
			'12' => "Desember",
		);
		return $month;
	}

	function getDay(){
		$days = array(
			'0' => "Minggu",
			'1' => "Senin",
			'2' => "Selasa",
			'3' => "Rabu",
			'4' => "Kamis",
			'5' => "Juma't",
			'6' => "Sabtu",
		);
		return $days;
	}

	function getAgama(){
		return array(
			'1' => "Islam",
			'2' => "Kristen",
			'3' => "Katholik",
			'4' => "Hindu",
			'5' => "Budha",
			'6' => "Lainnya"
			);
	}

	function getGender(){
		return array(
			'L' => "Laki-laki",
			'P' => "Perempuan"
		);
	}

	function getKewarganegaraan(){
		return array(
			'WNI' => "WNI - Warga Negara Indonesia",
			'WNA' => "WNA - Warga Negara Asing"
		);
	}

	function getStatusHidup(){
		return array(
			'1' => "Hidup",
			'0' => "Meninggal"
		);
	}

	function pushToFirebase($title,$body,$url){
		$headers = array(
				 'Authorization:key=' .$this->config->item('firebase_server_key'),
				 'Content-Type:application/json');
		$fields = array(
			'to' => '/topics/punglor',
				'notification' => array(
					'title'=>$title,
					'body'=>$body,
				),
				'data' => array(
					'url' => $url
				),
			 );
		$payload=json_encode($fields);
		$curl_session = curl_init();
		curl_setopt($curl_session, CURLOPT_URL, $this->config->item('firebase_push_url'));
		curl_setopt($curl_session, CURLOPT_POST, true);
		curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl_session, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl_session, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
		curl_setopt($curl_session, CURLOPT_POSTFIELDS, $payload);
		$result = curl_exec($curl_session);
		return $result;
	}

	function getEducationLevel(){
		return array(
			'SD' => "Sekolah Dasar",
			'SMP' => "Sekolah Menengah Pertama",
			'SMA' => "Sekolah Menengah Atas",
			'D1' => "Diploma 1",
			'D2' => "Diploma 2",
			'D3' => "Diploma 3",
			'S1' => "Sarjana",
			'S2' => "Magister",
			'S3' => "Profesor / Doktor",
			'L' => "Pendidikan Lainnya",
		);
	}

	function getActiveStatus(){
		return array(
			'0' => "<span class='badge badge-danger'>Tidak Aktif</span>",
			'1' => "<span class='badge badge-success'>Aktif</span>"
		);
	}

	function getPaymentStatus(){
		return array(
			'0' => "<span class='kt-badge kt-badge--danger kt-badge--inline kt-badge--pill kt-badge--rounded'>Belum Lunas</span>",
			'1' => "<span class='kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--rounded'>Lunas</span>",
		);
	}

	function getUserStatus(){
		return array(
			'0' => "<span class='kt-badge kt-badge--danger kt-badge--inline kt-badge--pill kt-badge--rounded'>Tidak Aktif</span>",
			'1' => "<span class='kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--rounded'>Aktif</span>",
		);
	}

	function getParentMenu( $privilege ){
		return $this->db->select("MENUPRIVILEGEID as keydt, menus.NAME as valuedt")->where("MENULEVEL", '0')->get("menus")->result_array();
	}

	function getAllMenu(){
		return $this->db->select("MENUID as keydt, CONCAT(menus.NAME, ' -> ', URL) as valuedt")->get("menus")->result_array();
	}

	function sendEmail( $email, $subject, $msg){
		$subject = strip_tags($subject);
		$configs = $this->db->get("configurations")->result();
		$config = $configs[0];

		$mail = new PHPMailer(true);      
		try {
	    //Server settings
	    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
	    $mail->isSMTP();                                      // Set mailer to use SMTP
	    $mail->Host = $config->SMTP_HOST;  										// Specify main and backup SMTP servers
	    $mail->SMTPAuth = true;                               // Enable SMTP authentication
	    $mail->Username = $config->SMTP_USER;        					// SMTP username
	    $mail->Password = $config->SMTP_PASSWORD;             // SMTP password
	    $mail->SMTPSecure = $config->SMTP_SECURE;             // Enable TLS encryption, `ssl` also accepted
	    $mail->Port = $config->SMTP_PORT;                     // TCP port to connect to

	    //Recipients
	    $mail->setFrom($config->SMTP_USER, $config->SMTP_NAME);
	    $mail->addAddress($email);     // Add a recipient

	    // $mail->addReplyTo('info@example.com', 'Information');
	    // $mail->addCC('cc@example.com');
	    // $mail->addBCC('bcc@example.com');

	    //Attachments
	    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
	    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

	    //Content
	    $mail->isHTML(true);                                  // Set email format to HTML
	    $mail->Subject = $subject;
	    $data['msg'] = $msg;
	    $mail->Body  = $this->load->view("mail", $data, true);
	    // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

	    $mail->send();
	    return true;
		} catch (Exception $e) {
	    // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
	    return false;
		}

	}

	function getConfiguration(){
		return $this->db->get("configurations")->row();
	}

	function getConfigItem( $field ){
		$config = $this->db->select($field)->get("configurations")->row();
		return $config->$field;
	}
	
	function getAdmStatus( $filter = null ){
		if(isset($filter)){
			foreach($filter as $f){
				$this->db->or_where("ADMSTATUSID", $f);
			}
		}
		return $this->db->select("ADMSTATUSID as keydt, NAME as valuedt")->get("admstatuses")->result_array();
	}

	function getProvince(){
		return $this->db->select("PROVINCEID as keydt, NAME as valuedt")->get("provinces")->result_array();
	}

	function getCity( $cityid = null){
		$province = $this->getConfigItem("APP_PROVINCE");
		$this->db->where("PROVINCE", $province);
		if( isset($cityid) ){
			$this->db->where("CITYID", $cityid);
		}
		return $this->db->select("CITYID as keydt, cities.NAME as valuedt, provinces.NAME as labeldt")->join("provinces", "PROVINCEID=PROVINCE", "LEFT")->get("cities")->result_array();
	}

	function getDistrict(){
		return $this->db->select("DISTRICTID as keydt, districts.NAME as valuedt, cities.NAME as labeldt")->join("cities", "CITYID=CITY", "LEFT")->get("districts")->result_array();
	}

	function getVillage(){
		return $this->db->select("VILLAGEID as keydt, villages.NAME as valuedt, districts.NAME as labeldt")->join("districts", "DISTRICTID=DISTRICT", "LEFT")->get("villages")->result_array();
	}

	function getBlank( $msg = "" ){
		return array(0 => array("keydt" => "", "valuedt" => "- Pilih " . $msg . " -"));
	}

	/* APP CUSTOM */

	function getUnit(){
		return $this->db->select("UNITID as keydt, NAME as valuedt")->get("units")->result_array();
	}

	function getStoreStatus(){
		return array(
			'0' => "<span class='kt-badge kt-badge--danger kt-badge--inline kt-badge--pill kt-badge--rounded'>Tidak Aktif</span>",
			'1' => "<span class='kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--rounded'>Aktif</span>",
		);
	}

	function getFarmer( $logged = null ){
		if( isset($logged['PRIVILEGE']) && $logged['PRIVILEGE'] == "KAB" ){
			$this->db->where("CITY", $logged['IDREF']);
		}
		if( isset($logged['PRIVILEGE']) && $logged['PRIVILEGE'] == "PTN" ){
			$this->db->where("FARMERID", $logged['IDREF']);
		}
		return $this->db->select("FARMERID as keydt, CONCAT(IFNULL(NIK,''), ' | ',farmers.NAME) as valuedt, cities.NAME as labeldt")->join("cities", "CITY=CITYID")->order_by("CITYID")->get("farmers")->result_array();
	}

	function getFarmerDetail($farmerID){
		return $this->db->select("farmers.*, cities.NAME as CITY")
			->where("FARMERID", $farmerID)
			->join("cities", "CITY=CITYID","LEFT")
			->get("farmers")->row();
	}

	function getCommodityCategory(){
		return $this->db->select("COMMODITYCATEGORYID as keydt, NAME as valuedt")->order_by("CODE","ASC")->get("commoditycategories")->result_array();
	}

	function getCommodity(){
		return $this->db->select("COMMODITYID as keydt, CONCAT( LPAD(COMMODITYID,3,'0'),' - ', commodities.NAME, ' / ', LATINNAME) as valuedt, commoditycategories.NAME as labeldt")->join("commoditycategories", "COMMODITYCATEGORY=COMMODITYCATEGORYID", "LEFT")->get("commodities")->result_array();
	}

	function getCategory(){
		return $this->db->select("CATEGORYID as keydt, NAME as valuedt")->get("categories")->result_array();
	}

	function getProduct(){
		return $this->db->select("PRODUCTID as keydt, products.NAME as valuedt, stores.NAME as labeldt")->join("stores", "STORE=STOREID", "LEFT")->get("products")->result_array();
	}

	function getFieldCertificateStatus(){
		return array(
			'0' => "<span class='kt-badge kt-badge--warning kt-badge--inline kt-badge--pill kt-badge--rounded'>Pending</span>",
			'1' => "<span class='kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--rounded'>Aktif</span>",
			'2' => "<span class='kt-badge kt-badge--danger kt-badge--inline kt-badge--pill kt-badge--rounded'>Ditolak</span>",
		);
	}

	function getCityApproval(){
		return array(
			'0' => "<span class='kt-badge kt-badge--warning kt-badge--inline kt-badge--pill kt-badge--rounded'>Belum</span>",
			'1' => "<span class='kt-badge kt-badge--primary kt-badge--inline kt-badge--pill kt-badge--rounded'>Diterima</span>",
			'2' => "<span class='kt-badge kt-badge--info kt-badge--inline kt-badge--pill kt-badge--rounded'>Revisi</span>",
			'3' => "<span class='kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--rounded'>Diteruskan</span>",
		);
	}

	function getProvinceApproval(){
		return array(
			'0' => "<span class='kt-badge kt-badge--warning kt-badge--inline kt-badge--pill kt-badge--rounded'>Belum</span>",
			'1' => "<span class='kt-badge kt-badge--primary kt-badge--inline kt-badge--pill kt-badge--rounded'>Penilaian</span>",
			'2' => "<span class='kt-badge kt-badge--danger kt-badge--inline kt-badge--pill kt-badge--rounded'>Ditolak</span>",
			'3' => "<span class='kt-badge kt-badge--info kt-badge--inline kt-badge--pill kt-badge--rounded'>Revisi</span>",
			'4' => "<span class='kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--rounded'>Disetujui</span>",
		);
	}

	function getField( $farmer = null,$commodity = null){
		if( isset($farmer) ){
			$this->db->where("FARMER", $farmer);
		}
		if( isset($commodity) ){
			$this->db->where("COMMODITY", $commodity);
		}
		return $this->db->select("FIELDID as keydt, CONCAT(NAME,' - ',FIELDSIZE,' ha') as valuedt")->get("fields")->result_array();
	}

	function getCertificateTerm( $type = "B"){
		$this->db->where("CERTIFICATETERMTYPE", $type);
		return $this->db->select("CERTIFICATETERMID as keydt, NAME as valuedt")->get("certificateterms")->result_array();
	}

	function getGAPSequence( $city, $code ){
		$seq = $this->db->select("SEQUENCE")->where("CITY", $city)->where("COMMODITY", $code)->get("gapsequences")->result();
		$result = 1;
		if( count($seq) > 0  ){
			$result = $seq[0]->SEQUENCE;
			$this->db->query("UPDATE gapsequences SET SEQUENCE=SEQUENCE+1 WHERE COMMODITY=".$this->db->escape($code)." AND CITY=".$this->db->escape($city));
		} else {
			$this->db->insert("gapsequences", array("CITY" => $city, "COMMODITY" => $code, "SEQUENCE" => 1));
		}
		return $result;
	}

	function getCertificateTermValue( $fieldCertificate ){
		return $this->db->select("FIELDCERTIFICATETERMID, certificateterms.NAME, CERTIFICATETERMSTATUS")
				->join("certificateterms", "CERTIFICATETERM=CERTIFICATETERMID", "LEFT")
				->where("FIELDCERTIFICATE", $fieldCertificate)
				->get("fieldcertificateterms")->result_array();
	}

	function getFieldCertificateDetail( $fieldCertificate ){
		return $this->db->select("FIELDCERTIFICATEDETAILID, fields.NAME, fields.FIELDID, fields.FIELDADDRESS,fields.FIELDLOCATION, fields.FIELDSIZE")
				->join("fields", "FIELD=FIELDID", "LEFT")
				->where("fieldcertificatedetails.FIELDCERTIFICATE", $fieldCertificate)
				->get("fieldcertificatedetails")->result_array();
	}

	function getFieldCertificateEdit($id){
		return $this->db->where("FIELDCERTIFICATEID", $id)->get("fieldcertificates")->row();
	}

	function getFieldCertificatePrint($id){
		return $this->db->select("
				fieldcertificates.*,
				farmers.NAME as FARMER,
				commodities.NAME as COMMODITY,
				cities.NAME as CITY,
				farmergroups.NAME as FARMERGROUP,
				farmers.ADDRESS,
				farmers.MOBILE,
				farmers.EMAIL,
			")
			->where("FIELDCERTIFICATEID", $id)
			->join("farmers", "FARMER=FARMERID", "LEFT")
			->join("commodities", "COMMODITY=COMMODITYID", "LEFT")
			->join("cities", "farmers.CITY=CITYID", "LEFT")
			->join("farmergroups", "farmers.FARMERGROUP=FARMERGROUPID", "LEFT")
			->get("fieldcertificates")->row();
	}

	function getFarmerGroup(){
		return $this->db->select("FARMERGROUPID as keydt, farmergroups.NAME as valuedt, cities.NAME as labeldt")->join("cities", "CITY=CITYID", "LEFT")->order_by("cities.CITYID")->get("farmergroups")->result_array();
	}

	// Dashboard

	function getTotFarmer( $logged, $gender = null ){
		if( $logged['PRIVILEGE'] == "KAB" ){
			$this->db->where("CITY", $logged['IDREF']);
		}
		if( isset($gender) ){
			$this->db->where("GENDER", $gender);
		}
		if( $logged['PRIVILEGE'] == "BAG" ){
			$cc = explode(",",$logged['COMMODITYCATEGORY']);
	  	$cCategory = "'" . implode("','", $cc) . "'";
			return $this->db
				->join("fields", "FIELD=FIELDID", "LEFT")
				->join("commodities", "COMMODITY=COMMODITYID", "LEFT")
				->where("commodities.COMMODITYCATEGORY IN (".$cCategory.")")
				->group_by("FARMERID")->get("farmers")->num_rows();
		}
		if( $logged['PRIVILEGE'] == "SBAG" ){
			$cc = explode(",",$this->logged['COMMODITY']);
	  	$commodity = "'" . implode("','", $cc) . "'";
			return $this->db
				->join("fields", "FARMER=FARMERID", "LEFT")
				->where("COMMODITY IN (".$commodity.")")
				->group_by("FARMERID")->get("farmers")->num_rows();
		}
		return $this->db->get("farmers")->num_rows();
	}

	function getTotFarmerWithoutField( $logged ){
		$and = "";
		if( $logged['PRIVILEGE'] == "KAB" ){
			$and = " AND farmers.CITY = ".$this->db->escape($logged['IDREF']);
		}
		return $this->db->query("
				SELECT 
				FARMERID, FIELDID
				FROM farmers
				LEFT JOIN fields ON FARMER=FARMERID
				WHERE ISNULL(FIELDID)
				".$and."
			")->num_rows();
	}

	function getTotField( $logged ){
		if( $logged['PRIVILEGE'] == "KAB" ){
			$this->db->where("fields.CITY", $logged['IDREF']);
		}
		if( $logged['PRIVILEGE'] == "BAG" ){
			$cc = explode(",",$logged['COMMODITYCATEGORY']);
	  	$cCategory = "'" . implode("','", $cc) . "'";
	  	$this->db->join("commodities", "COMMODITYID=COMMODITY", "LEFT");
	  	$this->db->where("commodities.COMMODITYCATEGORY IN (".$cCategory.")");
	  }
	  if( $logged['PRIVILEGE'] == "SBAG" ){
			$cc = explode(",",$this->logged['COMMODITY']);
	  	$commodity = "'" . implode("','", $cc) . "'";
	  	$this->db->where("COMMODITY IN (".$commodity.")");
	  }
		return $this->db->get("fields")->num_rows();
	}

	function getTotFieldArea( $logged ){
		if( $logged['PRIVILEGE'] == "KAB" ){
			$this->db->where("fields.CITY", $logged['IDREF']);
		}
		if( $logged['PRIVILEGE'] == "BAG" ){
			$cc = explode(",",$logged['COMMODITYCATEGORY']);
	  	$cCategory = "'" . implode("','", $cc) . "'";
	  	$this->db->join("commodities", "COMMODITYID=COMMODITY", "LEFT");
	  	$this->db->where("commodities.COMMODITYCATEGORY IN (".$cCategory.")");
	  }
	  if( $logged['PRIVILEGE'] == "SBAG" ){
			$cc = explode(",",$this->logged['COMMODITY']);
	  	$commodity = "'" . implode("','", $cc) . "'";
	  	$this->db->where("COMMODITY IN (".$commodity.")");
	  }
		$field = $this->db->select("SUM(FIELDSIZE) as FIELDSIZE")->get("fields")->row();
		return $field->FIELDSIZE; 
	}

	function getTotCertificate( $logged ){
		if( $logged['PRIVILEGE'] == "KAB" ){
			$this->db->where("farmers.CITY", $logged['IDREF']);
		}
		if( $logged['PRIVILEGE'] == "BAG" ){
			$cc = explode(",",$logged['COMMODITYCATEGORY']);
	  	$cCategory = "'" . implode("','", $cc) . "'";
	  	$this->db->join("commodities", "COMMODITYID=COMMODITY", "LEFT");
	  	$this->db->where("commodities.COMMODITYCATEGORY IN (".$cCategory.")");
	  }
	  if( $logged['PRIVILEGE'] == "SBAG" ){
			$cc = explode(",",$this->logged['COMMODITY']);
	  	$commodity = "'" . implode("','", $cc) . "'";
	  	$this->db->where("COMMODITY IN (".$commodity.")");
	  }
		return $this->db->join("farmers", "FARMER=FARMERID", "LEFT")->get("fieldcertificates")->num_rows();
	}

	function getTotActiveCertificate( $logged ){
		if( $logged['PRIVILEGE'] == "KAB" ){
			$this->db->where("farmers.CITY", $logged['IDREF']);
		}
		if( $logged['PRIVILEGE'] == "BAG" ){
			$cc = explode(",",$logged['COMMODITYCATEGORY']);
	  	$cCategory = "'" . implode("','", $cc) . "'";
	  	$this->db->join("commodities", "COMMODITYID=COMMODITY", "LEFT");
	  	$this->db->where("commodities.COMMODITYCATEGORY IN (".$cCategory.")");
	  }
	  if( $logged['PRIVILEGE'] == "SBAG" ){
			$cc = explode(",",$this->logged['COMMODITY']);
	  	$commodity = "'" . implode("','", $cc) . "'";
	  	$this->db->where("COMMODITY IN (".$commodity.")");
	  }
		return $this->db->where("ACTIVESTARTDATE <= CURDATE()")->where("ACTIVEENDDATE >= CURDATE()")->join("farmers", "FARMER=FARMERID", "LEFT")->get("fieldcertificates")->num_rows();
	}

	function getTotInActiveCertificate( $logged ){
		if( $logged['PRIVILEGE'] == "KAB" ){
			$this->db->where("farmers.CITY", $logged['IDREF']);
		}
		if( $logged['PRIVILEGE'] == "BAG" ){
			$cc = explode(",",$logged['COMMODITYCATEGORY']);
	  	$cCategory = "'" . implode("','", $cc) . "'";
	  	$this->db->join("commodities", "COMMODITYID=COMMODITY", "LEFT");
	  	$this->db->where("commodities.COMMODITYCATEGORY IN (".$cCategory.")");
	  }
	  if( $logged['PRIVILEGE'] == "SBAG" ){
			$cc = explode(",",$this->logged['COMMODITY']);
	  	$commodity = "'" . implode("','", $cc) . "'";
	  	$this->db->where("COMMODITY IN (".$commodity.")");
	  }
		return $this->db->where("ACTIVEENDDATE <= CURDATE()")->join("farmers", "FARMER=FARMERID", "LEFT")->get("fieldcertificates")->num_rows();
	}

	function getTotCommodity( $logged ){
		if( $logged['PRIVILEGE'] == "KAB" ){
			$this->db->where("fields.CITY", $logged['IDREF']);
		}
		if( $logged['PRIVILEGE'] == "BAG" ){
			$cc = explode(",",$logged['COMMODITYCATEGORY']);
	  	$cCategory = "'" . implode("','", $cc) . "'";
	  	$this->db->join("commodities", "COMMODITYID=COMMODITY", "LEFT");
	  	$this->db->where("commodities.COMMODITYCATEGORY IN (".$cCategory.")");
	  }
	  if( $logged['PRIVILEGE'] == "SBAG" ){
			$cc = explode(",",$this->logged['COMMODITY']);
	  	$commodity = "'" . implode("','", $cc) . "'";
	  	$this->db->where("COMMODITY IN (".$commodity.")");
	  }
		return $this->db->group_by("COMMODITY")->get("fields")->num_rows();
	}

	function getTotProduct ( $logged ){
		if( $logged['PRIVILEGE'] == "KAB" ){
			$this->db->where("stores.CITY", $logged['IDREF']);
		}
		if( $logged['PRIVILEGE'] == "BAG" ){
			$cc = explode(",",$logged['COMMODITYCATEGORY']);
	  	$cCategory = "'" . implode("','", $cc) . "'";
	  	$this->db->join("commodities", "COMMODITYID=COMMODITY", "LEFT");
	  	$this->db->where("commodities.COMMODITYCATEGORY IN (".$cCategory.")");
	  }
	  if( $logged['PRIVILEGE'] == "SBAG" ){
			$cc = explode(",",$this->logged['COMMODITY']);
	  	$commodity = "'" . implode("','", $cc) . "'";
	  	$this->db->where("COMMODITY IN (".$commodity.")");
	  }
		return $this->db->join("stores", "STORE=STOREID", "LEFT")->get("products")->num_rows();
	}

	function getTotStoreWithoutProduct ( $logged ){
		if( $logged['PRIVILEGE'] == "KAB" ){
			$this->db->where("stores.CITY", $logged['IDREF']);
		}
		return $this->db->where("TOTPRODUCT", 0)->get("stores")->num_rows();
	}

	function getTotFarmerProduct ( $farmer ){
		$this->db->where("stores.FARMER", $farmer);
		return $this->db->join("stores", "STORE=STOREID", "LEFT")->get("products")->num_rows();
	}
	
/* END DASHBOARD */

	function getFieldTotal( $id ){
		return $fieldcertificate = $this->db->select("SUM(fields.FIELDSIZE) as FIELDSIZE, fields.FIELDADDRESS, fields.FIELDMAP")
			->where("FIELDCERTIFICATEID", $id)
			->join("fieldcertificatedetails", "FIELDCERTIFICATE=FIELDCERTIFICATEID", "LEFT")
			->join("fields", "fieldcertificatedetails.FIELD=FIELDID", "LEFT")
			->group_by("FIELDCERTIFICATEID")
			->get("fieldcertificates")->row();
	}

	function getFarmerStatus(){
		return array(
			'0' => "<span class='kt-badge kt-badge--danger kt-badge--inline kt-badge--pill kt-badge--rounded'>Tidak Aktif</span>",
			'1' => "<span class='kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--rounded'>Aktif</span>",
		);
	}

	function getFarmerToolType(){
		return $this->db->select("FARMERTOOLTYPEID as keydt, NAME as valuedt")->get("farmertooltypes")->result_array();
	}

	function getCertificateTermType(){
		return array(
			'B' => "<span class='kt-badge kt-badge--primary kt-badge--inline kt-badge--pill kt-badge--rounded'>Baru</span>",
			'P' => "<span class='kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--rounded'>Perpanjangan</span>",
		);
	}

	function getApplicantType(){
		return $this->db->select("APPLICANTTYPEID as keydt, NAME as valuedt")->get("aplicanttypes")->result_array();
	}

	function getOldFieldCertificate( $id ){
		return $this->db->where("FIELDCERTIFICATEID", $id)->get("fieldcertificates")->row();
	}

	function getFieldFarmer( $farmer ){
		return $this->db->select("COUNT(FIELDID) as TOTFIELD, SUM(FIELDSIZE) as TOTFIELDSIZE")->where("FARMER", $farmer)->get("fields")->row();
	}

	function getTotCommodityFarmer($farmer){
		return $this->db->where("FARMER", $farmer)->group_by("COMMODITY")->get("fields")->num_rows();
	}

	function getTotGAPFarmer( $farmer ){
		return $this->db->where("FARMER", $farmer)->get("fieldcertificates")->num_rows();
	}

	function getUnitType(){
		return $this->db->select("UNITTYPEID as keydt, NAME as valuedt")->get("unittypes")->result_array();
	}

	

}
