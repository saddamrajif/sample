<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Mmasterpublic extends CI_Model{

	function __construct(){
		parent::__construct();
	}

	function getLoggedUser( $userid ){
		return $this->db->get_where("users", array("USERID" => $userid) )->result();
	}

	function getSocialMedia(){
		return $this->db->get("socialmedias")->result();
	}

	function getCommodityCategory(){
		return $this->db->order_by("CODE", "ASC")->get("commoditycategories")->result();
	}

	function getPartner(){
		return $this->db->get("partners")->result();
	}

	function getSlider(){
		return $this->db->get("sliders")->result();
	}

	function getNewsCategory(){
		return $this->db->get("categories")->result();
	}

	function getFeaturedProduct( $category = null ){
		if( isset($category) ){
			$this->db->where("featuredproducts.COMMODITYCATEGORY", $category);
		}
		return $this->db->select("
				PRODUCTID, products.PRICE, products.PRICE_ST, products.NAME, 
				products.IMAGE1, commoditycategories.NAME as COMMODITYCATEGORY
			")
			->join("products", "PRODUCT=PRODUCTID", "LEFT")
			->join("commoditycategories", "featuredproducts.COMMODITYCATEGORY=COMMODITYCATEGORYID", "LEFT")
			->get("featuredproducts")->result();
	}

	function getPage(){
		return $this->db->where("POSTTYPE", "page")->get("posts")->result();
	}

	function getProductList($page = 0, $category = null, $tag = null, $query = null, $order_by = array(), $all = false){
		$perpage = $this->session->userdata("limit");
		// $page = $page * $perpage;
		if(isset($category)){
			$this->db->where("COMMODITYCATEGORY", $category);
		}
		if(isset($tag)){
			$this->db->where("
				products.TAGS LIKE '%".$this->db->escape_like_str($tag)."%'
			");
		}
		if(isset($query)){
			$this->db->where("
				products.NAME LIKE '%".$this->db->escape_like_str($query)."%'
			");
		}
		if( $order_by!= array() ){
			$this->db->order_by($order_by['field'], $order_by['seq']);
		}
		if( !$all ){
			$products = $this->db->select(
					"products.PRODUCTID, products.NAME, commoditycategories.NAME as COMMODITYCATEGORY,
					PRICE, PRICE_ST, stores.NAME as STORE, DESCRIPTION, IMAGE1"
				)->join("commoditycategories", "products.COMMODITYCATEGORY=COMMODITYCATEGORYID")
				->join("stores", "STORE=STOREID")
				->limit($perpage, $page)
				->get("products")->result();
			return $products;
		} else {
			$products = $this->db->select(
					"products.PRODUCTID, products.NAME, commoditycategories.NAME as COMMODITYCATEGORY,
					PRICE, PRICE_ST, stores.NAME as STORE"
				)->join("commoditycategories", "products.COMMODITYCATEGORY=COMMODITYCATEGORYID")
				->join("stores", "STORE=STOREID")
				->get("products")->result();
			return $products;
		}
	}

	function getCategoryBySlug( $slug ){
		return $this->db->where("SLUG", $slug)->get("commoditycategories")->row();
	}
	
	function getNewsCategoryBySlug( $slug ){
		return $this->db->where("SLUG", $slug)->get("categories")->row();
	}

	function getProductTag(){
		return $this->db->limit(25)->order_by("RAND()")->get("producttags")->result();
	}

	function getProductDetail( $id ){
		$products = $this->db->select(
					"products.PRODUCTID, products.NAME, commoditycategories.NAME as COMMODITYCATEGORY,
					PRICE, PRICE_ST, stores.NAME as STORE, DESCRIPTION, IMAGE1, IMAGE2, IMAGE3,
					stores.MOBILE, VIEWED, CONTACTED, STORESTATUS, STOREID,
					stores.EMAIL, stores.NAME as STORE, stores.ADDRESS, cities.NAME as CITY"
				)
				->where("PRODUCTID", $id)
				->join("commoditycategories", "products.COMMODITYCATEGORY=COMMODITYCATEGORYID")
				->join("stores", "STORE=STOREID")
				->join("cities", "stores.CITY=CITYID")
				->get("products")->row();
			return $products;
	}

	function getBlogList( $page = 0, $all = false, $category = null, $query = null ){
		$perpage = 10;
		if( !$all ){
			$this->db->limit($perpage, $page);
		}
		if( isset($category) ){
			$this->db->where("CATEGORY", $category);
		}
		if( isset($query) ){
			$this->db->where("
				TITLE LIKE '%".$this->db->escape_like_str($query)."%' 
				OR CONTENT LIKE '%".$this->db->escape_like_str($query)."%'
			");
		}
		return $this->db->select("POSTID, TITLE, IMAGE, CONTENT, categories.NAME as CATEGORY, CREATEAT, users.NAME as AUTHOR")
						->join("users", "AUTHOR=USERID", "LEFT")
						->join("categories", "CATEGORYID=CATEGORY", "LEFT")
						->where("POSTTYPE", "post")
						->order_by("CREATEAT", "DESC")
						->get("posts")->result();
	}

	function getRecentNews(){
		return $this->db->select("POSTID, TITLE, IMAGE, CONTENT, categories.NAME as CATEGORY, CREATEAT, users.NAME as AUTHOR")
						->join("categories", "CATEGORYID=CATEGORY", "LEFT")
						->join("users", "AUTHOR=USERID", "LEFT")
						->where("POSTTYPE", "post")
						->order_by("CREATEAT", "DESC")
						->limit(5)
						->get("posts")->result();
	}

	function getSingleBlog( $id ){
		return $this->db->select("POSTID, TITLE, IMAGE, CONTENT, categories.NAME as CATEGORY, CREATEAT, users.NAME as AUTHOR")
						->join("categories", "CATEGORYID=CATEGORY", "LEFT")
						->join("users", "AUTHOR=USERID", "LEFT")
						->where("POSTID", $id)
						->order_by("CREATEAT", "DESC")
						->get("posts")->row();
	}

	function getSinglePage( $id ){
		return $this->db->select("POSTID, TITLE, IMAGE, CONTENT, categories.NAME as CATEGORY, CREATEAT")
						->join("categories", "CATEGORYID=CATEGORY", "LEFT")
						->where("POSTID", $id)
						->order_by("CREATEAT", "DESC")
						->get("posts")->row();
	}

	function getRelatedLink(){
		return $this->db->get("relatedlinks")->result();
	}

	function getStoreStatus(){
		return array(
			'0' => "<span class='badge badge-danger badge-pill'> Tidak Aktif </span>",
			'1' => "<span class='badge badge-success badge-pill'> Aktif </span>",
		);
	}

	function getAlsintan( $farmer ){
		return $this->db->select("FARMERTOOLID, farmertooltypes.NAME as FARMERTOOLTYPE, farmertools.NAME, QUANTITY")->join("farmertooltypes", "FARMERTOOLTYPE=FARMERTOOLTYPEID", "LEFT")->where("FARMER", $farmer)->get("farmertools")->result();
	}

	function getPlanHarvestPeriod( $farmer ){
		return $this->db->select("PLANHARVESTPERIODID, fields.NAME as FIELD, planharvestperiods.NAME, commodities.NAME as COMMODITY, PLANDATE, HARVESTDATE, SEEDPLANWEIGHT, HARVESTWEIGHT")
					->join("fields", "FIELD=FIELDID", "LEFT")
					->join("commodities", "fields.COMMODITY=COMMODITYID", "LEFT")
					->where("planharvestperiods.FARMER", $farmer)->get("planharvestperiods")->result();
	}

}
		