<?php

$lang['form_validation_required'] = "Kolom {field} tidak boleh kosong";
$lang['form_validation_numeric'] = "Kolom {field} harus berisi angka";
$lang['form_validation_is_unique'] = "{field} ini sudah pernah digunakan";
$lang['form_validation_min_length'] = "Kolom {field} minimal {param} karakter";
$lang['form_validation_max_length'] = "Kolom {field} maksimal {param} karakter";
$lang['form_validation_exact_length'] = "Kolom {field} harus {param} karakter";
$lang['form_validation_alpha_dash'] = "Kolom {field} berisi huruf atau dash";