<?php

$config['custom_config'] = "Think as a sample !";
$config['encrypt_url'] = TRUE;

// Google MAP API
$config['google_map_api_key'] = "AIzaSyAipPhqE7-LB3F-kJCdq3Lz6vHfiPIIspA";
$config['default_position'] = "-7.505301,110.220613";

// Firebase Cloud Messages

$config['firebase_push_url'] = "https://fcm.googleapis.com/fcm/send";
$config['firebase_server_key'] = "AAAA4nrOYxo:APA91bFDy-Cx_Eu2v6F4hh1LIiYM1FMvLnUKbLG3uYtdKn117Lr7-HjvBCmphB-id-RM3rimOvYTR4FyxAKPKPjoD0uSVE2exWbBrDcULGPziNsLI-ep-J1ZLzSiYbGakcNyBQokbRoZtYMcM9P1H4E9SBbhB9PD6Q";


// POSTMARKAPP CONFIGURATION
$config['postmarkapp_token'] = "f50e2125-0dcb-40ec-ae10-d61a94b120db";
$config['postmarkapp_alias'] = "developer@siedoo.com";


// APPLICATION VARIABLE
$config['app_name'] = "Sistem Informasi Pencatatan Lahan Pertanian";
$config['app_short_name'] = "SIPLAP";

// Datagrid Container
$config['header_container'] = '
      <div class="kt-subheader kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
          <h3 class="kt-subheader__title">[name]</h3>
          <span class="kt-subheader__separator kt-subheader__separator--v"></span>
          <div class="kt-subheader__breadcrumbs">
            <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="#" class="kt-subheader__breadcrumbs-link">
              Admin </a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="#" class="kt-subheader__breadcrumbs-link">
              [name] </a>
          </div>
        </div>
      </div>
      <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
        ';

$config['footer_container'] = '
</div>';

$config['months'] = array (
    1 => 'Januari',
    2 => 'Februari',
    3 => 'Maret',
    4 => 'April',
    5 => 'Mei',
    6 => 'Juni',
    7 => 'Juli',
    8 =>'Agustus',
    9 => 'September',
    10 => 'Oktober',
    11 =>'November',
    12 => 'Desember'
);

$config['days'] = array ( 
  1 => 'Senin',
  2 => 'Selasa',
  3 => 'Rabu',
  4 => 'Kamis',
  5 => 'Jumat',
  6 => 'Sabtu',
  7 => 'Minggu'
);