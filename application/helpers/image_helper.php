<?php
if( !function_exists("image_check")){
	function image_check( $image ){
    $foto = (file_exists("./uploads/".$image)) ? "uploads/".$image : "uploads/noimage.png";
    if( $foto != "uploads/noimage.png" ){
      $foto = ($image != "") ? "uploads/".$image : "uploads/noimage.png";
    }
    return $foto;
	}
}

if( !function_exists("imageload")){
	function imageload( $image, $name, $uploaddir ){
		if($image != "" ){
			return "<img src='".$uploaddir."/".$image."' class='m--img-rounded m--marginless' alt='".$name."'>";
		} else {
			return "<span class='kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold'>".strtoupper(substr($name,0,1))."</span>";
		}
	}
}
