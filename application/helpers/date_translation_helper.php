<?php
if (!function_exists("date_to_ID")) {
  function date_to_ID($date = "", $day = false){
    $date = date("Y-m-d", strtotime($date));
    if( $date == "" ){
      return "";
    }
    $bulan = array (1 => 'Januari',
            				2 => 'Februari',
            				3 => 'Maret',
            				4 => 'April',
            				5 => 'Mei',
            				6 => 'Juni',
            				7 => 'Juli',
            				8 =>'Agustus',
            				9 => 'September',
            				10 => 'Oktober',
            				11 =>'November',
            				12 => 'Desember'
			);
	$split = explode('-', $date);
	$ID_date =  $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
  $hari = array ( 1 => 'Senin',
          				2 => 'Selasa',
          				3 => 'Rabu',
          				4 => 'Kamis',
          				5 => 'Jumat',
          				6 => 'Sabtu',
          				7 => 'Minggu'
			);

	if ($day) {
		$num = date('N', strtotime($date));
		return $hari[$num] . ', ' . $ID_date;
	}
	return $ID_date;
  }
}
