<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {

	private $error = array();

	function __construct(){
		parent::__construct();
		$this->load->model("Mmasterdata");
	}

	function index(){
		$this->session->unset_userdata('login');
		$this->session->unset_userdata('admin');
		// unset($_SESSION['reload']);
		// $_SESSION['reload'] = "";
		// redirect( $_SESSION['current_url'] );
		redirect( base_url() );
	}

	function admin(){
		$this->session->unset_userdata('admin');
		redirect( base_url() );	
	}
}
