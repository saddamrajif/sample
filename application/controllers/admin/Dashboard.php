<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	private $data = array();
	private $logged = array();

	public function __construct(){
		parent::__construct();
		$this->load->model('Mmasterdata');
		$this->logged = $this->session->userdata("admin");
	}

	public function index(){
		$this->data["logged"] = $this->logged;
		$this->data['TotFarmer'] = $this->Mmasterdata->getTotFarmer($this->logged);
		$this->data['TotCommodity'] = $this->Mmasterdata->getTotCommodity($this->logged);
		$this->data['TotProduct'] = $this->Mmasterdata->getTotProduct($this->logged);
		$this->data['TotStoreWithoutProduct'] = $this->Mmasterdata->getTotStoreWithoutProduct($this->logged);
		$this->data['TotFarmerMale'] = $this->Mmasterdata->getTotFarmer($this->logged, "L");
		$this->data['TotFarmerFemale'] = $this->Mmasterdata->getTotFarmer($this->logged, "P");
		$this->data['TotFarmerWithoutField'] = $this->Mmasterdata->getTotFarmerWithoutField($this->logged);
		$this->data['TotField'] = $this->Mmasterdata->getTotField( $this->logged );
		$this->data['TotFieldArea'] = $this->Mmasterdata->getTotFieldArea( $this->logged );
		$this->data['TotCertificate'] = $this->Mmasterdata->getTotCertificate( $this->logged );
		$this->data['TotActiveCertificate'] = $this->Mmasterdata->getTotActiveCertificate( $this->logged );
		$this->data['TotInActiveCertificate'] = $this->Mmasterdata->getTotInActiveCertificate( $this->logged );
		if($this->logged['PRIVILEGE'] == "ADM" || $this->logged['PRIVILEGE'] == "DNS"){
			$this->load->view('admin/dashboard',$this->data);
		} elseif($this->logged['PRIVILEGE'] == "KAB") {
			$this->load->view('admin/dashboard',$this->data);
		} elseif($this->logged['PRIVILEGE'] == "PTN") {
			$fieldFarmer = $this->Mmasterdata->getFieldFarmer($this->logged['IDREF']);
			$this->data['qtyField'] = $fieldFarmer->TOTFIELD;
			$this->data['sizeField'] = $fieldFarmer->TOTFIELDSIZE;
			$this->data['commodity'] = $this->Mmasterdata->getTotCommodityFarmer($this->logged['IDREF']);
			$this->data['gap'] = $this->Mmasterdata->getTotGAPFarmer($this->logged['IDREF']);
			$this->data['product'] = $this->Mmasterdata->getTotFarmerProduct($this->logged['IDREF']);
			$this->load->view('farmer/dashboard',$this->data);
		} elseif($this->logged['PRIVILEGE'] == "BAG") {
			$this->load->view('admin/dashboard',$this->data);
		} elseif($this->logged['PRIVILEGE'] == "SBAG") {
			$this->load->view('admin/dashboard',$this->data);
		}
	}
}
