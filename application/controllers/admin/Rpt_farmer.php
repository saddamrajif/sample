<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rpt_farmer extends CI_Controller{
	
	private $params = array();
	private $logged = array();
	
	function __construct(){
		parent::__construct();
		$this->logged = $this->session->userdata('admin');
		$this->getparams();
		
	}
	
	function index(){
		$this->report();
	}
	
	function getparams(){
		$kop = $this->db->get('configurations')->result_array(); $kop = $kop[0];
		// $this->params['ttd1jabatan']=$kop["TTD1JABATAN"];
		// $this->params['ttd1name']=$kop["TTD1NAME"];
		// $this->params['ttd1nip']= "NIP. " . $kop["TTD1NIP"];
		$this->params['ttd2jabatan']=$kop["TTD1JABATAN"];
		$this->params['ttd2name']=$kop["TTD1NAME"];
		$this->params['ttd2nip']=$kop["TTD1NIP"];
		$this->params['logo'] = base_url("assets/img/logo-jatim-128.png");
		$this->params['kop1']="Laporan Data Petani";
		$this->params['kop2']=$kop["OFFICE_NAME"];
		$this->params['kopaddress']=$kop["OFFICE_ADDRESS"];
		$this->params['command']="browse,print,search,excel";
		$this->params['name']="Lap. " . $this->lang->line("farmer");
		$this->params['table']="fieldcertificates";
		$this->params['title']="";
		$this->params['sql']="
			SELECT FARMERID,
				farmers.NIK,
				CONCAT(farmers.NAME,' (',GENDER,')') as NAME,
      	farmergroups.NAME as FARMERGROUP, FARMERSTATUS,
      	GENDER, farmers.ADDRESS, BIRTHDATE, BIRTHPLACE, cities.NAME as CITY
      	FROM farmers
      	LEFT JOIN cities ON CITY=CITYID
      	LEFT JOIN farmergroups ON FARMERGROUP=FARMERGROUPID
      	WHERE 1";
		//$startdate = (isset($_POST['STARTDATE'])) ? $_POST['STARTDATE'] : date("Y-m-1");
		//$enddate = (isset($_POST['ENDDATE'])) ? $_POST['ENDDATE'] : date("Y-m-d");
		//$this->params['sql'] .= " WHERE CREATEAT >= ".$this->db->escape($startdate)." AND CREATEAT <= ".$this->db->escape($enddate);
		if( $this->logged['PRIVILEGE'] == "KAB"){
			$this->params['sql'] .= " AND farmers.CITY='".$this->logged['IDREF']."'";
		}
		if( $this->logged['PRIVILEGE'] == "BAG" ){
	  	$cc = explode(",",$this->logged['COMMODITYCATEGORY']);
	  	$cCategory = "'" . implode("','", $cc) . "'";
	  	$this->params['sql'] = "
	  		SELECT * FROM (SELECT FARMERID,
				farmers.NIK,
				CONCAT(farmers.NAME,' (',GENDER,')') as NAME,
      	farmergroups.NAME as FARMERGROUP, FARMERSTATUS,
      	GENDER, farmers.ADDRESS, BIRTHDATE, BIRTHPLACE, cities.NAME as CITY
      	FROM farmers
      	LEFT JOIN cities ON CITY=CITYID
      	LEFT JOIN farmergroups ON FARMERGROUP=FARMERGROUPID
      	LEFT JOIN fields ON FARMER=FARMERID
	    	LEFT JOIN commodities ON COMMODITY=COMMODITYID
	    	WHERE commodities.COMMODITYCATEGORY IN (".$cCategory.")
	    	GROUP BY farmers.FARMERID) as farmers
	    	WHERE 1";
	  }
	   if( $this->logged['PRIVILEGE'] == "SBAG" ){
	  	$cc = explode(",",$this->logged['COMMODITY']);
	  	$commodity = "'" . implode("','", $cc) . "'";
	  	$this->params['sql'] = "SELECT * FROM (SELECT FARMERID,
				farmers.NIK,
				CONCAT(farmers.NAME,' (',GENDER,')') as NAME,
      	farmergroups.NAME as FARMERGROUP, FARMERSTATUS,
      	GENDER, farmers.ADDRESS, BIRTHDATE, BIRTHPLACE, cities.NAME as CITY
      	FROM farmers
      	LEFT JOIN cities ON CITY=CITYID
      	LEFT JOIN farmergroups ON FARMERGROUP=FARMERGROUPID
	    	LEFT JOIN fields ON FARMER=FARMERID
    		WHERE fields.COMMODITY IN (".$commodity.")
	    	GROUP BY farmers.FARMERID) as farmers
	    	WHERE 1";
	  }
		if(isset($_POST['CITY']) && $_POST['CITY'] != ""){
			$this->params['sql'] .= " AND farmers.CITY=".$this->db->escape($_POST['CITY'])."";
		}
		$this->params['ttddate'] = date_to_ID(date("Y-m-d"));
		$this->params['ttdcity'] = "Surabaya";
		if(isset($_POST['PRINTDATE']) && $_POST['PRINTDATE'] != ""){
			$this->params['ttddate'] = date_to_ID($_POST['PRINTDATE']);
		}
		if(isset($_POST['PRINTCITY']) && $_POST['PRINTCITY'] != ""){
			$city = $this->db->where("CITYID", $_POST['PRINTCITY'])->get("cities")->row();
			$this->params['ttdcity'] = $city->NAME;
		}
		
		if( $this->logged['PRIVILEGE'] == "UNT"){
			$this->params['sql'] .= " AND UNIT='".$this->logged['IDREF']."'";
		}

		$this->params['search']=array(
			'PRINTDATE' => array(
				'type' => "date",
				'value' => date("Y-m-d")
			),
			'PRINTCITY' => array(
				'type' => "dropdownquery",
				'sourcequery' => $this->Mmasterdata->getCity(),
				'class' => "select2"
			),
			'CITY' => array(
				'type' => "dropdownquery",
				'sourcequery' => array_merge(array(0 => array("keydt" => "", "valuedt" => "-")),$this->Mmasterdata->getCity()),
				'class' => "select2"
			),
		);
		if( $this->logged['PRIVILEGE'] == "KAB"){
			unset($this->params['search']['CITY']);
		}
		$this->getfieldreport();
	}
	
	function report(){
		$this->load->library("engine",$this->params);
		echo $this->engine->report();
	}
	
	function getfieldreport(){
		$this->params['fieldselect']=array(
			'SEQ' => array(
				
			),
			'FARMERID' => array(
				'type' => "primarykey",
				'hidden' => true
			),
			'NAME' => array(
				'class' => "sorting",
			),	
			'ADDRESS' => array(
				'class' => "sorting",
			),
			'CITY' => array(
				'class' => "sorting",
			),
			'FARMERGROUP' => array(
				'class' => "sorting",
			),
			'FARMERSTATUS' => array(
				'class' => "sorting",
				'type' => "dropdownarray",
				'sourcearray' => $this->Mmasterdata->getFarmerStatus()
			),	
		);
	}
}