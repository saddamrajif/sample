<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller{
	
	private $params = array();
	
	function __construct(){
		parent::__construct();
		$this->load->model('Mmasterdata');
		$this->getparams();
	}
	
	function index(){
		$this->edit();
	}
	
	function getparams(){
		$this->params['command'] = "browse";
		$this->params['name']=$this->lang->line("profile");
		$this->params['table']="users";
		$this->urisegments=$this->uri->uri_to_assoc(4);
		$this->getfieldedit();
	}
	
	function getfieldedit(){
		$this->params['fieldadd']=array(
			'USERID' => array(
				'class' => "col-md-3",
				'maxlength' => "25",
				'type' => "primarykey"
			),
			'NAME' => array(
				'validation' => "required",
				'class' => "col-md-6",
				'maxlength' => "100",
			),
			'MOBILE' => array(
				'validation' => "required",
				'class' => "col-md-6",
				'maxlength' => "100",
			),
			'EMAIL' => array(
				'validation' => "valid_email",
				'class' => "col-md-6",
				'type' => "email"
			),
			'ADDRESS' => array(
				'validation' => "required",
				'class' => "col-md-6",
				'type' => "textarea"
			),
			'PHOTO' => array(
				'class' => "col-md-3",
				'type' => "file"
			),
		);
	}
	
	
	function browse(){
		$this->edit();
	}
	
	function edit(){
		$this->params['urisegments']['pk'] = 'USERID';
		$this->params['urisegments']['valpk'] = $_SESSION['admin']['USERID'];
		$this->load->library("engine",$this->params);
		echo $this->engine->edit();
	}
	
}
?>