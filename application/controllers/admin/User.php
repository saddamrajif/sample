<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller{
	
	private $params = array();
	
	function __construct(){
		parent::__construct();
		$this->load->model('Mmasterdata');
		$this->getparams();
	}
	
	function index(){
		$this->browse();
	}
	
	function getparams(){
		$this->params['command'] = "browse,add,edit,delete,deleteall";
		$this->params['name'] = $this->lang->line("user");
		$this->params['table'] = "users";
		$this->params['sql'] = "SELECT USERID, users.NAME,
		CONCAT('<div class=\"kt-user-card-v2\">
	              <div class=\"kt-user-card-v2__pic\">
	                  ',imageload(PHOTO, users.NAME,\"".base_url("uploads")."\"),'
	              </div>
	              <div class=\"kt-user-card-v2__details\">
	                  <span class=\"kt-user-card-v2__name\">',users.NAME,'</span>
	                  <a href=\"#\" class=\"kt-user-card-v2__email kt-link\">@',USERID,'</a>
	              </div>
	          	</div>') as USERNAME,
		CONCAT('M :',IFNULL(MOBILE,''),'<br />E : ', IFNULL(EMAIL,'')) as CONTACT, 
		PRIVILEGE, USERSTATUS,
		PHOTO, cities.NAME as CITY
		FROM users 
		LEFT JOIN cities ON IDREF=CITYID
		";
		$this->urisegments = $this->uri->uri_to_assoc(4);
		$this->getfieldselect();
		$this->getfieldedit();
	}
	
	function getfieldselect(){
		$this->params['fieldselect']=array(
			'SEQ' => array(
				
			),
			'#' => array(

			),
			'USERID' => array(
				'type' => "primarykey",
				'hidden' => true
			),
			'USERNAME' => array(
				'class' => "sorting",
				'width' => "200px"
			),
			'CONTACT' => array(
				'class' => "sorting",
				'width' => "150px"
			),
			'PRIVILEGE' => array(
				'class' => "sorting",
				'type' => "dropdownarray",
				'sourcearray' => $this->Mmasterdata->getPrivilege()
			),
			'CITY' => array(
				'class' => "sorting",
			),
			'USERSTATUS' => array(
				'type' => "dropdownarray",
				'sourcearray' => $this->Mmasterdata->getUserStatus(),
				'width' => "100px"
			),
		);
	}
	
	function getfieldedit(){
		$this->params['fieldadd']=array(
			'USERID' => array(
				'validation' => "required",
				'class' => "col-md-3",
				'maxlength' => "25",
				'type' => "primarykey"
			),
			'PASSWD' => array(
				'validation' => "required",
				'class' => "col-md-6",
				'maxlength' => "50",
				'type' => "password",
				'help' => "Kosongi = tanpa mengganti password"
			),
			'NAME' => array(
				'validation' => "required",
				'class' => "col-md-6",
				'maxlength' => "50",
			),
			'MOBILE' => array(
				'validation' => "required",
				'class' => "col-md-6",
				'maxlength' => "50",
			),
			'EMAIL' => array(
				'validation' => "required",
				'class' => "col-md-6",
				'maxlength' => "50",
				'type' => "email"
			),
			'PRIVILEGE' => array(
				'validation' => "required",
				'class' => "col-md-3",
				'type' => "dropdownarray",
				'sourcearray' => $this->Mmasterdata->getPrivilege()
			),
			'IDREF' => array(
				'class' => "col-md-6 select2",
				'type' => "dropdownquery",
				'sourcequery' => array_merge(array(0 => array("keydt" => "", "valuedt" => "-")),$this->Mmasterdata->getCity())
			),
			'PHOTO' => array(
				'class' => "col-md-3",
				'type' => "file"
			),
			'USERSTATUS' => array(
				'validation' => "required",
				'class' => "col-md-3",
				'type' => "dropdownarray",
				'sourcearray' => $this->Mmasterdata->getUserStatus()
			),
			'COMMODITYCATEGORY' => array(
				'class' => "col-md-8 select2",
				'type' => "dropdownquery",
				'sourcequery' => $this->Mmasterdata->getCommodityCategory(),
				'multiple' => true,
			),
			'COMMODITY' => array(
				'class' => "col-md-8 select2",
				'type' => "dropdownquery",
				'sourcequery' => $this->Mmasterdata->getCommodity(),
				'multiple' => true,
			),
		);
	}
	
	function getData(){
		$this->load->library("Engine",$this->params);
		echo $this->engine->getData();
	}
	
	function browse(){
		$this->load->library("Engine",$this->params);
		echo $this->engine->browse();
	}

	function jsinclude(){
		echo "<script type='text/javascript'>
			
		</script>";
	}

	function add(){
		if(count($_POST)>0){
			$_POST['PASSWD'] = md5($_POST['PASSWD']);
			if(isset($_POST['COMMODITYCATEGORY'])){
				$_POST['COMMODITYCATEGORY'] = implode(",", $_POST['COMMODITYCATEGORY']);
			}
			if(isset($_POST['COMMODITY'])){
				$_POST['COMMODITY'] = implode(",", $_POST['COMMODITY']);
			}
		}
		$this->params['fieldadd']['EMAIL']['validation'] .= "|is_unique[users.EMAIL]";
		$this->load->library("Engine",$this->params);
		echo $this->engine->add();
	}
	
	function edit(){
		unset($this->params['fieldadd']['PASSWD']['validation']);
		if(count($_POST)>0){
			if(isset($_POST['COMMODITYCATEGORY'])){
				$_POST['COMMODITYCATEGORY'] = implode(",", $_POST['COMMODITYCATEGORY']);
			}
			if(isset($_POST['COMMODITY'])){
				$_POST['COMMODITY'] = implode(",", $_POST['COMMODITY']);
			}
			if( $_POST['PASSWD'] == ""){
				unset($_POST['PASSWD']);
			}
			else {
				$_POST['PASSWD'] = md5($_POST['PASSWD']);
			}
			// Check Email
			$check = $this->db->where("EMAIL", $_POST['EMAIL'])->where("USERID != ",$this->urisegments['valpk'])->get("users")->result();
			if( count($check) > 0 ){
				$this->params['alert'] = array(
					'type' => "danger",
					'title' => "error",
					'message' => "Email ini sudah digunakan"
				);
				unset($_POST['EMAIL']);
			}
		}
		$this->load->library("engine",$this->params);
		echo $this->engine->edit();
	}
	
	function delete(){
		// Check
		$check = $this->db->get("users")->num_rows();
		if($check == 1){
			echo "Anda Tidak dapat menghapus user terakhir";
		} else {
			$delete=$this->db->delete($this->params['table'],array($this->urisegments['pk'] => urldecode($this->urisegments['valpk'])));
			if($delete){
				echo "<script>
					loadcontent('main-content','".site_url($this->router->fetch_directory().$this->router->fetch_class())."');
				</script>";
			}
		}
	}
	
}
?>