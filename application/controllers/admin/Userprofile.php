<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Userprofile extends CI_Controller{
	
	private $params = array();
	
	function __construct(){
		parent::__construct();
		$this->load->model('Mmasterdata');
		$this->logged = $this->session->userdata("admin");
		$this->getparams();
	}
	
	function index(){
		$this->browse();
	}
	
	function getparams(){
		$this->params['command'] = "browse";
		$this->params['name'] = $this->lang->line("userprofile");
		$this->params['table'] = "users";
		$this->params['sql'] = "SELECT USERID, USERID as BANKACCOUNTNO, users.NAME, 
		CONCAT('M :',IFNULL(users.MOBILE,''),'<br />E : ', IFNULL(users.EMAIL,'')) as CONTACT, 
		PRIVILEGE,
		BENDAHARA, users.ADDRESS, users.MOBILE, users.EMAIL,
		areas.NAME as AREA,
		schools.NPSN, schools.NAME as SCHOOL, 
		PHOTO, cities.NAME as CITY
		FROM users 
		LEFT JOIN areas ON users.AREA=AREAID
		LEFT JOIN schools ON users.IDREF=SCHOOLID
		LEFT JOIN cities ON schools.CITY=CITYID
		WHERE PRIVILEGE='MBR'";
		if( $this->logged['PRIVILEGE'] == "CBG" && $this->logged['AREA'] != "" ){
			$this->params['sql'] .= " AND schools.CITY IN (SELECT CITYID FROM cities WHERE AREA='".$this->logged['AREA']."')";
		}
		$this->urisegments = $this->uri->uri_to_assoc(4);
		$this->getfieldselect();
		$this->getfieldedit();
	}
	
	function getfieldselect(){
		$this->params['fieldselect']=array(
			'SEQ' => array(
				
			),
			'PHOTO' => array(
				'type' => "image",
				'class' => "sorting",
				'width' => "100px"
			),
			'USERID' => array(
				'type' => "primarykey",
				'hidden' => true
			),
			'BANKACCOUNTNO' => array(
				'class' => "sorting",
				'width' => "100px"
			),
			'NAME' => array(
				'class' => "sorting",
			),
			'CITY' => array(
				'class' => "sorting",
			),
			'BENDAHARA' => array(
				'class' => "sorting",
				'width' => "200px"
			),
			'CONTACT' => array(
				'class' => "sorting",
				'width' => "150px"
			),
		);
	}
	
	function getfieldedit(){
		$this->params['fieldadd']=array(
			'USERID' => array(
				'validation' => "required",
				'class' => "col-md-3",
				'maxlength' => "25",
				'type' => "primarykey"
			),
			'PASSWD' => array(
				'validation' => "required",
				'class' => "col-md-6",
				'maxlength' => "50",
				'type' => "password",
				'help' => "Kosongi = tanpa mengganti password"
			),
			'NAME' => array(
				'validation' => "required",
				'class' => "col-md-6",
				'maxlength' => "50",
			),
			'MOBILE' => array(
				'validation' => "required",
				'class' => "col-md-6",
				'maxlength' => "50",
			),
			'EMAIL' => array(
				'validation' => "required",
				'class' => "col-md-6",
				'maxlength' => "50",
				'type' => "email"
			),
			'PRIVILEGE' => array(
				'validation' => "required",
				'class' => "col-md-3",
				'type' => "dropdownarray",
				'sourcearray' => $this->Mmasterdata->getPrivilege()
			),
			'AREA' => array(
				'class' => "col-md-8 select2",
				'type' => "dropdownquery",
				'sourcequery' => array_merge(array( 0 => array('keydt' => "", 'valuedt' => "-")), $this->Mmasterdata->getArea()),
				'help' => "diisi jika Hak Akses Cabang"
			),
			'PHOTO' => array(
				'class' => "col-md-3",
				'type' => "file"
			),
		);
	}
	
	function getData(){
		$this->load->library("Engine",$this->params);
		echo $this->engine->getData();
	}
	
	function browse(){
		$this->load->library("Engine",$this->params);
		echo $this->engine->browse();
	}

	function add(){
		if(count($_POST)>0){
			$_POST['PASSWD'] = md5($_POST['PASSWD']);
		}
		$this->load->library("Engine",$this->params);
		echo $this->engine->add();
	}
	
	function edit(){
		unset($this->params['fieldadd']['PASSWD']['validation']);
		if(count($_POST)>0){
			if( $_POST['PASSWD'] == ""){
				unset($_POST['PASSWD']);
			}
			else {
				$_POST['PASSWD'] = md5($_POST['PASSWD']);
			}
		}
		$this->load->library("engine",$this->params);
		echo $this->engine->edit();
	}
	
	function delete(){
		$delete=$this->db->delete($this->params['table'],array($this->urisegments['pk'] => urldecode($this->urisegments['valpk'])));
		if($delete){
			echo "<script>
				loadcontent('main-content','".site_url($this->router->fetch_directory().$this->router->fetch_class())."');
			</script>";
		}
	}
	
}
?>