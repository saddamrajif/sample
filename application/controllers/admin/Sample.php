<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sample extends CI_Controller{
	
	private $params = array();
	
	function __construct(){
		parent::__construct();
		$this->load->model('Mmasterdata');
		$this->getparams();
	}
	
	function index(){
		$this->browse();
	}
	
	function getparams(){
		$this->params['command'] = "browse,add,edit,delete,deleteall";
		$this->params['name'] = $this->lang->line("sample");
		$this->params['table'] = "samples";
		$this->params['sql'] = "SELECT SAMPLEID, NAME, DESCRIPTION, MAP, ADDRESS, ADDRESS2, MAP2
		DROPDOWNQUERY, DROPDOWNARRAY, MULTIPLE, FILE
		FROM samples	
		";
		$this->urisegments = $this->uri->uri_to_assoc(4);
		$this->getfieldselect();
		$this->getfieldedit();
	}
	
	function getfieldselect(){
		$this->params['fieldselect']=array(
			'SEQ' => array(
				
			),
			'#' => array(

			),
			'SAMPLEID' => array(
				'type' => "primarykey",
				'hidden' => true,
			),
			'NAME' => array(
				'class' => "sorting",
			),
			'DESCRIPTION' => array(
				'class' => "sorting",
				'width' => "100px"
			),
			'FILE' => array(
				'class' => "sorting",
				'width' => "100px",
				'type' => "download"
			),
		);
	}
	
	function getfieldedit(){
		$this->params['fieldadd']=array(
			'SAMPLEID' => array(
				'hidden' => true,
				'type' => "primarykey"
			),
			'NAME' => array(
				'validation' => "required",
				'class' => "col-md-6",
				'maxlength' => "50",
			),
			'DESCRIPTION' => array(
				'class' => "col-md-6",
				'type' => "wysiwyg"
			),
			'DROPDOWNQUERY' => array(
				'validation' => "required",
				'class' => "col-md-6 select2",
				'type' => "dropdownquery",
				'sourcequery' => $this->Mmasterdata->getProvince()
			),
			'DROPDOWNARRAY' => array(
				'validation' => "required",
				'class' => "col-md-6 select2",
				'type' => "dropdownarray",
				'sourcearray' => $this->Mmasterdata->getGender()
			),
			'MAP' => array(
				'class' => "col-md-6",
				'type' => "map"
			),
			'ADDRESS' => array(
				'validation' => "required",
				'class' => "col-md-6",
				'type' => "textarea"
			),
			'ADDRESS2' => array(
				'validation' => "required",
				'class' => "col-md-6",
				'type' => "text"
			),
			'MAP2' => array(
				'validation' => "required",
				'class' => "col-md-6",
				'type' => "map",
				'address_id' => "ADDRESS"
			),
			'MULTIPLE' => array(
				'validation' => "required",
				'class' => "col-md-6 select2",
				'type' => "dropdownquery",
				'multiple' => true,
				'sourcequery' => $this->Mmasterdata->getProvince()
			),
			'FILE' => array(
				'class' => "col-md-6",
				'type' => "file"
			),
		);
	}
	
	function getData(){
		$this->load->library("Engine",$this->params);
		echo $this->engine->getData();
	}
	
	function browse(){
		$this->load->library("Engine",$this->params);
		echo $this->engine->browse();
	}

	function jsinclude(){
		echo "<script type='text/javascript'>
			
		</script>";
	}

	function add(){
		if( count($_POST) > 0){
			if(isset($_POST['MULTIPLE'])){
				$_POST['MULTIPLE'] = implode(",", $_POST['MULTIPLE']);
			}
		}
		$this->load->library("Engine",$this->params);
		echo $this->engine->add();
	}
	
	function edit(){
		if( count($_POST) > 0){
			if(isset($_POST['MULTIPLE'])){
				$_POST['MULTIPLE'] = implode(",", $_POST['MULTIPLE']);
			}
		}
		$this->load->library("engine",$this->params);
		echo $this->engine->edit();
	}
	
	function delete(){
		$delete=$this->db->delete($this->params['table'],array($this->urisegments['pk'] => urldecode($this->urisegments['valpk'])));
		if($delete){
			echo "<script>
				loadcontent('main-content','".site_url($this->router->fetch_directory().$this->router->fetch_class())."');
			</script>";
		}
	}

	function deleteall(){
		$post = $this->input->post();
		foreach($post as $key => $value){
			if($value == true){
				$id = explode("-", $key);
				$pk = $id[1];
				$val = $id[2];
				$this->db->delete($this->params['table'], array($pk => $val));
			}
		}
		echo "<script>
			loadcontent('engine-content','".site_url($this->router->fetch_directory().$this->router->fetch_class())."/browse/');
		</script>";
	}
	
}
?>