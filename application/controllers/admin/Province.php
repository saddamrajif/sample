<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Province extends CI_Controller{
	
	private $params = array();
	
	function __construct(){
		parent::__construct();
		$this->load->model('Mmasterdata');
		$this->getparams();
	}
	
	function index(){
		$this->browse();
	}
	
	function getparams(){
		$this->params['command'] = "browse,add,edit,delete,deleteall";
		$this->params['name'] = $this->lang->line("province");
		$this->params['table'] = "provinces";
		$this->params['sql'] = "SELECT PROVINCEID, provinces.NAME, 
		admstatuses.NAME as ADMSTATUS
		FROM provinces
		LEFT JOIN admstatuses ON ADMSTATUS=ADMSTATUSID";
		$this->urisegments = $this->uri->uri_to_assoc(4);
		$this->getfieldselect();
		$this->getfieldedit();
	}
	
	function getfieldselect(){
		$this->params['fieldselect']=array(
			'SEQ' => array(
				
			),
			'#' => array(

			),
			'PROVINCEID' => array(
				'type' => "primarykey",
				'width' => "100px",
				'class' => "sorting",
			),
			'NAME' => array(
				'class' => "sorting",
			),
			'ADMSTATUS' => array(
				'class' => "sorting",
				'width' => "100px"
			),
		);
	}
	
	function getfieldedit(){
		$this->params['fieldadd']=array(
			'PROVINCEID' => array(
				'hidden' => true,
				'type' => "primarykey"
			),
			'NAME' => array(
				'validation' => "required",
				'class' => "col-md-6",
				'maxlength' => "100",
			),
			'ADMSTATUS' => array(
				'validation' => "required",
				'type' => "dropdownquery",
				'class' => "col-md-6 select2",
				'sourcequery' => $this->Mmasterdata->getAdmStatus()
			),
		);
	}
	
	function getData(){
		$this->load->library("Engine",$this->params);
		echo $this->engine->getData();
	}
	
	function browse(){
		$this->load->library("Engine",$this->params);
		echo $this->engine->browse();
	}

	function jsinclude(){
		echo "<script type='text/javascript'>
			
		</script>";
	}

	function add(){
		$this->load->library("Engine",$this->params);
		echo $this->engine->add();
	}
	
	function edit(){
		$this->load->library("engine",$this->params);
		echo $this->engine->edit();
	}
	
	function delete(){
		$delete=$this->db->delete($this->params['table'],array($this->urisegments['pk'] => urldecode($this->urisegments['valpk'])));
		if($delete){
			echo "<script>
				loadcontent('main-content','".site_url($this->router->fetch_directory().$this->router->fetch_class())."');
			</script>";
		}
	}

	function deleteall(){
		$post = $this->input->post();
		foreach($post as $key => $value){
			if($value == true){
				$id = explode("-", $key);
				$pk = $id[1];
				$val = $id[2];
				$this->db->delete($this->params['table'], array($pk => $val));
			}
		}
		echo "<script>
			loadcontent('engine-content','".site_url($this->router->fetch_directory().$this->router->fetch_class())."/browse/');
		</script>";
	}
	
}
?>