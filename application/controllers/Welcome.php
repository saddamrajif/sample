<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	private $error = array();
	private $data = array();
	private $logged = array();
	private $admin = array();

	function __construct(){
		parent::__construct();
		$this->logged = $this->session->userdata("login");
	}

	function index(){
		redirect(site_url("admin"));
	}

	function error404(){
		$this->error['heading'] = "Halaman tidak ditemukan";
		$this->error['message'] = "<p>Pastikan Anda melakukan navigasi melalui menu</p>";
		echo "
		<div class='kt-subheader kt-grid__item' id='kt_subheader'>
      <div class='kt-subheader__main'>
        <h3 class='kt-subheader__title'>ERROR</h3>
        <span class='kt-subheader__separator kt-subheader__separator--v'></span>
        <div class='kt-subheader__breadcrumbs'>
          <a href='#' class='kt-subheader__breadcrumbs-home'><i class='flaticon2-shelter'></i></a>
          <span class='kt-subheader__breadcrumbs-separator'></span>
          <a href='#' class='kt-subheader__breadcrumbs-link'>
            Error Page </a>
          <span class='kt-subheader__breadcrumbs-separator'></span>
          <a href='#' class='kt-subheader__breadcrumbs-link'>
            Error 404 </a>
        </div>
      </div>
    </div>
		<div class='text-center alert alert-danger' role='alert'>
			<div class='alert-text'>
				<h4 class='alert-heading'>Error 404</h4>
				<p>Halaman tidak ditemukan</p>
			</div>
		</div>";
		// $this->load->view("errors/html/error_404");
	}

	function sendMessage(){
		$post = $this->input->post();
		if(count($post) > 0){
			$insert = $this->db->insert( "messages", $post );
			if( $insert ){
				echo "<div class='alert alert-success'>Pesan berhasil dikirimkan</div>";
			} else {
				echo "<div class='alert alert-danger'>Pesan Gagal dikirimkan</div>";
			}
		}
	}
}
