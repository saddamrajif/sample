<form class="form-horizontal" method="POST" role="form" id="formFieldCertficate" enctype="multipart/formdata">
	<?php if($logged['PRIVILEGE'] == "ADM" || $logged['PRIVILEGE'] == "DNS"){ ?>
	<div class="form-group row">
		<label class="col-md-2" for="CERTIFICATENUMBER">No. Sertifikat</label>
		<div class="col-md-6">
			<div class="input-group">
				<input type="text" name="CERTIFICATENUMBER" id="CERTIFICATENUMBER" class="form-control" />
			</div>
		</div>
	</div>
	<?php } ?>
	<div class="form-group row">
		<label class="col-md-2" for="SUBMISSIONDATE">Tgl. Pengajuan</label>
		<div class="col-md-3">
			<div class="input-group">
				<input type="text" required value="<?php echo date("Y-m-d"); ?>" name="SUBMISSIONDATE" id="SUBMISSIONDATE" class="form-control date-picker" />
			</div>
		</div>
	</div>
	
	<div class="form-group row">
		<label class="col-md-2" for="FARMER">Petani</label>
		<div class="col-md-6">
			<div class="input-group">
				<select name="FARMER" id="FARMER" class="form-control select2" required>
					<option value="">-- PIlih Petani --</option>
					<?php 
						$label = "";
						foreach($farmers as $farmer){
							if( $label != $farmer['labeldt']){
								echo "<optgroup label='".$farmer['labeldt']."'>";
								$label = $farmer['labeldt'];
							}
							echo "<option value='".$farmer['keydt']."'>".$farmer['valuedt']."</option>";
						}
					?>
				</select>
			</div>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-2" for="NAME">Nama</label>
		<div class="col-md-6">
			<div class="input-group">
				<input type="text" name="NAME" id="NAME" disabled class="form-control" />
			</div>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-2" for="ADDRESS">Alamat</label>
		<div class="col-md-6">
			<div class="input-group">
				<textarea name="ADDRESS" id="ADDRESS" disabled class="form-control"></textarea>
			</div>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-2" for="MOBILE">Telepon</label>
		<div class="col-md-6">
			<div class="input-group">
				<input type="text" name="MOBILE" id="MOBILE" disabled class="form-control" />
			</div>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-2" for="EMAIL">Email</label>
		<div class="col-md-6">
			<div class="input-group">
				<input type="text" name="EMAIL" id="EMAIL" disabled class="form-control" />
			</div>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-2" for="COMMODITY">Komoditas</label>
		<div class="col-md-6">
			<div class="input-group">
				<!-- <input value="" type="text" name="NAME" id="NAME" placeholder="" required="" class="form-control " maxlength="50"> -->
				<select name="COMMODITY" id="COMMODITY" class="form-control select2">
					<?php 
						foreach($commodities as $commodity){
							echo "<option value='".$commodity['keydt']."'>".$commodity['valuedt']."</option>";
						}
					?>
				</select>
			</div>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-2" for="PREINFORMATION">Informasi Awal</label>
		<div class="col-md-8 table-responsive">
			<table class="table table-bordered table-sm">
				<thead>
					<tr>
						<th style="width:40px;">No</th>
						<th>Pertanyaan</th>
						<th style="width:120px;">Jawaban</th>
					</tr>
				</thead>
				<tbody>
					<?php $seq = 1; foreach($certificateterms as $ct){ ?>
					<tr>
						<td><?php echo $seq; ?>.</td>
						<td><?php echo $ct['valuedt']; ?></td>
						<td>
							<select name="ANSWER[<?php echo $ct['keydt']; ?>]" class="form-control">
								<option value="0">Tidak</option>
								<option value="1">Ya</option>
							</select>
						</td>
					</tr>
					<?php $seq++; } ?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-2" for="FIELD">Lahan</label>
		<div class="col-md-10 table-responsive">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th style='width:30px;'>No</th>
						<th style='width:200px;'>Nama Lahan</th>
						<th>Alamat</th>
						<th>Luas</th>
						<th style='width:40px;'>Delete</th>
					</tr>
				</thead>
				<tbody id="fieldList">
					
				</tbody>
			</table>
			<div class="text-right">
				<button class="btn btn-primary btn-pill" type="button" id="addField"><i class="la la-plus"></i> Add Lahan</button>
			</div>
		</div>
	</div>
	<?php if($logged['PRIVILEGE'] == "ADM" || $logged['PRIVILEGE'] == "DNS"){ ?>
	<div class="form-group row">
		<label class="col-md-2" for="ASSESMENTDATE">Tgl. Penilaian</label>
		<div class="col-md-3">
			<div class="input-group">
				<input type="text" name="ASSESMENTDATE" id="ASSESMENTDATE" class="form-control date-picker" />
			</div>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-2" for="ACTIVESTARTDATE">Tgl. Mulai Berlaku</label>
		<div class="col-md-3">
			<div class="input-group">
				<input type="text"  name="ACTIVESTARTDATE" id="ACTIVESTARTDATE" class="form-control date-picker" />
			</div>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-2" for="ACTIVEENDDATE">Tgl. Berakhir</label>
		<div class="col-md-3">
			<div class="input-group">
				<input type="text" name="ACTIVEENDDATE" id="ACTIVEENDDATE" class="form-control date-picker" />
			</div>
		</div>
	</div>
	<?php } ?>
	<div class="form-group row">
		<label class="col-md-2" for="NOTES">Catatan</label>
		<div class="col-md-6">
			<div class="input-group">
				<textarea name="NOTES" id="NOTES" class="form-control"></textarea>
			</div>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-2" for="CITYAPPROVAL">Persetujuan Kab/Kota</label>
		<div class="col-md-6">
			<div class="input-group">
				<select name="CITYAPPROVAL" id="CITYAPPROVAL" class="form-control">
					<?php 
						$cityApproval = $this->Mmasterdata->getCityApproval();
						foreach( $cityApproval as $key => $value){
							echo "<option value='".$key."'>".$value."</option>";
						}
					?>
				</select>
			</div>
		</div>
	</div>
	<?php if($logged['PRIVILEGE'] == "ADM" || $logged['PRIVILEGE'] == "DNS"){ ?>
	<div class="form-group row">
		<label class="col-md-2" for="PROVINCEAPPROVAL">Persetujuan Provinsi</label>
		<div class="col-md-6">
			<div class="input-group">
				<select name="PROVINCEAPPROVAL" id="PROVINCEAPPROVAL" class="form-control">
					<?php 
						$provinceApproval = $this->Mmasterdata->getProvinceApproval();
						foreach( $provinceApproval as $key => $value){
							echo "<option value='".$key."'>".$value."</option>";
						}
					?>
				</select>
			</div>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-md-2" for="CERTIFICATECODE">No. GAP</label>
		<div class="col-md-6">
			<div class="input-group">
				<div class="input-group-prepend"><button id="generate" type="button" class='btn btn-primary'>Beri Nomor</button></div>
				<input type="text" readonly name="CERTIFICATECODE" id="CERTIFICATECODE" class="form-control" value="GAP.01-35." />
			</div>
		</div>
	</div>
	<?php } ?>
	<div class="form-group row">
		<div class="col-md-10 offset-md-2">
			<button type="submit" class="btn btn-primary btn-pill"><i class="fa fa-save"></i> Simpan</button>
			<button type="reset" onclick="loadcontent('engine-content', site_url + 'admin/fieldcertificate/index');" class="btn btn-danger btn-pill"><i class="la la-refresh"></i> Reset</button>
		</div>
	</div>
</form>
<script type="text/javascript">
	$(function(){
		var fieldSeq = 1;
		$(".select2").select2();
		$(".date-picker").datepicker({
			autoclose: true,
			format: "yyyy-mm-dd"
		});

		$("#ACTIVESTARTDATE").on("change", function(){
			<?php $config = $this->Mmasterdata->getConfiguration(); ?>
			var oldDate = new Date($(this).val());
			var year = oldDate.getFullYear();
			var month = (oldDate.getMonth() > 10) ? (eval(oldDate.getMonth()) + 1) : "0" + (eval(oldDate.getMonth()) + 1);
			var date = (oldDate.getDate() > 10) ? oldDate.getDate() : "0" + oldDate.getDate();
			var offset = <?php echo $config->CONTRACTPERIOD; ?>;
			// var newDate = new Date(year + offset, month, date);
			var newDate = (eval(year) + offset) + "-" + month + "-" + date;
			$("#ACTIVEENDDATE").val(newDate);
		});
		
		$("#FARMER").on("change", function(){
			var target = site_url + "admin/fieldcertificate/getFarmer";
			var datapost = {
				'FARMERID' : $(this).val()
			}
			if( $(this).val() != "" ){
				$.post(target, datapost, function( e ){
					$("#NAME").val( e.NAME );
					$("#ADDRESS").val( e.ADDRESS );
					$("#EMAIL").val( e.EMAIL );
					$("#MOBILE").val( e.MOBILE );
				},"json");
			} else {
				$("#NAME").val( "" );
				$("#ADDRESS").val( "" );
				$("#EMAIL").val( "" );
				$("#MOBILE").val( "" );
			}
		});

		$("#addField").on("click", function(){
			// Get Field List
			if( $("#FARMER").val() == "" || $("#COMMODITY").val() == ""){
				swal.fire("Lengkapi Data", "Isikan Petani dan Komoditas terlebih dahulu", "warning");
				return false;
			}
			var target = site_url + "admin/fieldcertificate/getField";
			var datapost = {
				'farmer': $("#FARMER").val(),
				'commodity': $("#COMMODITY").val(),
			}
			$.post( target, datapost, function(e){
				var blank = "<tr id='field_"+fieldSeq+"'><td style='width:30px;'>"+ fieldSeq +".</td><td style='width:200px;'><select onchange='loadDetail( $(this) )' name='FIELD[]' id='FIELD_" + fieldSeq + "' class='form-control'>" + e + "</select></td><td></td><td></td><td><button onclick='deleteList(\""+fieldSeq+"\");' class='btn btn-danger btn-elevate btn-circle btn-icon btn-delete btn-sm' type='button'><i class='fa fa-trash'></i></button></td></tr>";
				$("#fieldList").append(blank);
				fieldSeq++;
			});
		});

		$("#formFieldCertficate").on("submit", function(){
			$("#engine-content").html("<div class='text-center spin-loading'><img src='" + base_url + "assets/img/spin.gif' /></div>");
			var datapost = $(this).serialize();
			var target = site_url + "admin/fieldcertificate/addSubmit";
			$.post(target, datapost, function( e ){
				loadcontent("engine-content", site_url + "admin/fieldcertificate/index");
			});
			return false;
		});

		$("#generate").on("click", function(){
			getGAPNumber();
			$(this).attr("disabled", true);
		});

	});
	function deleteList( id ){
		$("tr#field_" + id).remove();
	}

	function loadDetail( elem ){
		var target = site_url + "admin/fieldcertificate/loadDetail";
		var datapost = {
			fieldId: elem.val()
		}
		$.post(target, datapost, function( e ){
			elem.parents("tr").find("td:nth-child(3)").html( e.FIELDADDRESS );
			elem.parents("tr").find("td:nth-child(4)").html( e.FIELDSIZE + ' ha' );
		},"json");
	}

	function getGAPNumber(){
		if( $("#PROVINCEAPPROVAL").val() == "4" ){
			var target = site_url + "admin/fieldcertificate/getGAPNumber";
			var datapost = {
				"FARMER": $("#FARMER").val(),
				"COMMODITY": $("#COMMODITY").val(),
			}
			$.post(target, datapost, function(e){
				$("#CERTIFICATECODE").val( e );
			});
		} else  {
			swal.fire("Belum Disetujui", "Sertifikat GAP ini belum disetujui", "warning");
		}
	}
</script>