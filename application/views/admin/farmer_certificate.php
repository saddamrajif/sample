<style>
	.print{
		font-family:  Lato;
	}
	.print p{
		color: #333;
		font-size: 16pt;
	}
	.print tr td{
		color: #333;
		font-size: 16pt;	
	}
</style>
<?php 
	$config = $this->Mmasterdata->getConfiguration();
?>
<div class="print">
<table class="table table-borderless" style="width:80%; border:3px solid #555;">
	<tr>
		<td style="border-bottom: 2px solid #444;">
			<table id="table-header" class="table no-border" cellspacing="0" cellpadding="5" border="0">
				<tbody>
					<tr>
						<td style="width:100px;">
							<img src="<?php echo base_url("assets") ?>/img/logo-jatim-128.png" style="width:100px">
						</td>
						<td>
							<h4 class="text-center">PEMERINTAH PROVINSI JAWA TIMUR</h4>
							<h3 class="text-center"><?php echo $config->OFFICE_NAME; ?></h3>
							<p class="text-center"><?php echo $config->OFFICE_ADDRESS ?></p>
						</td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<h2 class="text-center">SURAT KETERANGAN REGISTRASI</h2>
			<p class="text-center font-weight-bold">NOMOR : <?php echo $fieldcertificate->CERTIFICATENUMBER; ?></p>
			<br /><br />
			<p class="text-center">Diberikan kepada :</p>
			<table class="table table-borderless table-sm" align="center" style="width: 60%">
				<tr>
					<td colspan="3" style="text-align:center;"><h3><?php echo $fieldcertificate->FARMER . " / KELOMPOK TANI "; ?>"<?php echo $fieldcertificate->FARMERGROUP; ?>"</h3></td>
				</tr>
				<tr>
					<td style="width:200px;">Komoditas</td>
					<td>:</td>
					<td><?php echo $fieldcertificate->COMMODITY; ?></td>
				</tr>
				<tr>
					<td>Luas Lahan</td>
					<td>:</td>
					<td><?php echo $fieldsize->FIELDSIZE; ?> ha</td>
				</tr>
				<tr>
					<td>Alamat Kebun/Lahan Usaha</td>
					<td>:</td>
					<td><?php echo $fieldsize->FIELDADDRESS; ?></td>
				</tr>
			</table>
			<br />
			<p class="text-center">Menerangkan bahwa nama dan alamat kebun/lahan usaha yang
			dikelola telah memenuhi persyaratan GAP Buah dan Sayur berdasar
			PERMENTAN No. 48/Permentan/OT.140/10/2009 dengan No. Registrasi :</p>
			<table class="table table-borderless">
				<tr>
					<td style="width:25%"></td>
					<td style="border:1.5px solid #333;padding: 12px;text-align:center;">
						<h1><?php echo $fieldcertificate->CERTIFICATECODE; ?></h1>
					</td>
					<td style="width:25%"></td>
				</tr>
			</table>
			<p class="text-center">Berlaku untuk jangka waktu 2 (dua) tahun sejak tanggal ditetapkan</p>
			<br /><br />
			<p class="text-center">Ditetapkan oleh :</p>
			<table class="table table-borderlesss" style="width:70%" align="center">
				<tr>
					<td style="width:250px;">Dinas Pertanian</td>
					<td style="width:2px;">:</td>
					<td><?php echo $config->OFFICE_NAME; ?></td>
				</tr>
				<tr>
					<td style="width:250px;">Tanggal</td>
					<td style="width:2px;">:</td>
					<td><?php echo date_to_ID($fieldcertificate->ACTIVESTARTDATE); ?></td>
				</tr>
				<tr>
					<td colspan="3" style="text-align:center;">
						Kepala Dinas Pertanian Provinsi Jawa Timur<br /><br /><br /><br /><br /><br />
						<strong><u><?php echo $config->TTD1NAME; ?></u></strong><br />
						NIP. <?php echo $config->TTD1NIP; ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</div>