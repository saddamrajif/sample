<?php
  $config = $this->Mmasterdata->getConfig();
?> 
</div>
<!-- end:: Body -->
<!-- begin::Footer -->
<footer class="m-grid__item		m-footer ">
	<div class="m-container m-container--fluid m-container--full-height m-page__container">
		<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
			<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
				<span class="m-footer__copyright">
					2019 &copy; <?php echo $config->OFFICE_NAME; ?>
				</span>
			</div>
			<div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
				<ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
					<li class="m-nav__item">
						<a href="#" class="m-nav__link">
							<span class="m-nav__link-text">About</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="#" class="m-nav__link">
							<span class="m-nav__link-text">Privacy</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="#" class="m-nav__link">
							<span class="m-nav__link-text">T&C</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</footer>

<!-- end::Footer -->
</div>

<!-- end:: Page -->

<!-- begin::Scroll Top -->
<div id="m_scroll_top" class="m-scroll-top">
<i class="la la-arrow-up"></i>
</div>

<!-- end::Scroll Top -->

<!--begin::Global Theme Bundle -->
<script src="<?php echo base_url("assets/admin"); ?>/vendors/base/vendors.bundle.js" type="text/javascript"></script>
<script src="<?php echo base_url("assets/admin"); ?>/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

<!--end::Global Theme Bundle -->

<!--begin::Page Vendors -->
<script src="<?php echo base_url("assets/admin"); ?>/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
<!--end::Page Vendors -->

<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?php echo $this->config->item("google_map_api_key"); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

<script src="<?php echo base_url("assets/admin"); ?>/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url("assets/admin"); ?>/vendors/data-tables/dataTables.tableTools.js"></script>
<script type="text/javascript" src="<?php echo base_url("assets/admin"); ?>/vendors/data-tables/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="<?php echo base_url("assets/admin"); ?>/vendors/data-tables/dataTables.responsive.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap4.min.js"></script>
<script type="text/javascript" src="<?php echo base_url("assets/admin"); ?>/vendors/toastr/toastr.min.js"></script>

<script type="text/javascript" src="<?php echo base_url("assets/admin"); ?>/js/multiple-select.js"></script>
<script type="text/javascript" src="<?php echo base_url("assets/admin"); ?>/js/jquery.ajaxfileupload.js"></script>
<script type="text/javascript" src="<?php echo base_url("bower_components"); ?>/tinymce/jquery.tinymce.min.js"></script>
<script type="text/javascript" src="<?php echo base_url("bower_components"); ?>/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="<?php echo base_url("assets/admin"); ?>/js/bootbox.js"></script>
<script type="text/javascript" src="<?php echo base_url("assets/admin"); ?>/js/tagsinput.js"></script>
<script type="text/javascript" src="<?php echo base_url("assets/admin"); ?>/js/moment.min.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url("assets/admin"); ?>/js/bootstrap-datetimepicker.min.js"></script> -->
<!-- <script type="text/javascript" src="<?php echo base_url("assets/admin"); ?>/js/bootstrap-datepicker.min.js"></script> -->
<script type="text/javascript" src="<?php echo base_url("assets/admin"); ?>/js/jquery.PrintArea.js"></script>
<script type="text/javascript" src="<?php echo base_url("bower_components"); ?>/js-xlsx/dist/xlsx.core.min.js"></script>
<script type="text/javascript" src="<?php echo base_url("bower_components"); ?>/blobjs/Blob.min.js"></script>
<script type="text/javascript" src="<?php echo base_url("bower_components"); ?>/file-saverjs/FileSaver.js"></script>
<script type="text/javascript" src="<?php echo base_url("bower_components"); ?>/tableexport.js/dist/js/tableexport.min.js"></script>
<script type="text/javascript" src="<?php echo base_url("bower_components"); ?>/typeahead.js/dist/bloodhound.min.js"></script>
<script type="text/javascript" src="<?php echo base_url("bower_components"); ?>/typeahead.js/dist/typeahead.bundle.min.js"></script>
<script type="text/javascript" src="<?php echo base_url("bower_components"); ?>/typeahead.js/dist/typeahead.jquery.min.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url("assets/admin/js/jquery.validate.min.js") ?>"></script> -->
<!-- <script type="text/javascript" src="<?php echo base_url("assets/admin/js/jquery.steps.min.js") ?>"></script> -->
<!-- <script type="text/javascript" src="<?php echo base_url("assets/admin/js/jquery.number.min.js") ?>"></script> -->
<script type="text/javascript" src="<?php echo base_url("assets/admin/js/polyfiller.js") ?>"></script>
<script src="<?php echo base_url("assets/admin"); ?>/demo/default/custom/components/base/sweetalert2.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/engine.js"></script>
<script type="text/javascript">
  SweetAlert2Demo.init();
  tinymce.init({
    plugins: [
      'advlist autolink lists link image charmap print preview hr anchor pagebreak',
      'searchreplace wordcount visualblocks visualchars code fullscreen',
      'insertdatetime media nonbreaking save table contextmenu directionality',
      'emoticons paste textcolor colorpicker textpattern imagetools'
    ],
    toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image code',
    toolbar2: 'print preview media | forecolor backcolor emoticons | table',

    // image_advtab: true,
    relative_urls : false,
    remove_script_host : false,
    convert_urls : true,

    width: '100%',
    menubar : false,
    image_title: true, 
    image_dimensions: true,
    automatic_uploads: true,
    images_upload_url: './upload/tinyMCEUpload/',
    images_upload_credentials: true,
  });

  function tinyMCEUpload(fileid){
    $.ajaxFileUpload({
      url:"<?php echo site_url(); ?>/upload/tinyMCEUpload/",
      secureuri:false,
      fileElementId:fileid,
      dataType: 'json',
      data:{name:'logan', id:'id'},
      success: function (data, status){
        console.log(data);
        if(typeof(data.error) != 'undefined'){
          if(data.error != ''){
            bootbox.alert(data.error);
          } else {
            return data.msg;
          }
        }
      },
      error: function (data, status, e)
      {
        bootbox.alert(e);
      }
    })
    return false;
  }

  function ajaxupload(fileid){
    $.ajaxFileUpload({
      url:"<?php echo site_url(); ?>/upload/uploadfile/fileName/FILE_" + fileid,
      secureuri:false,
      fileElementId:'FILE_'+fileid,
      dataType: 'json',
      data:{name:'logan', id:'id'},
      success: function (data, status){
        console.log(data);
        if(typeof(data.error) != 'undefined'){
          if(data.error != ''){
            bootbox.alert(data.error);
          } else {
            $("#"+fileid).val(data.msg);
          }
        }
      },
      error: function (data, status, e)
      {
        bootbox.alert(e);
      }
    })
    return false;
  }
</script>

</body>

<!-- end::Body -->
</html>