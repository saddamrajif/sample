<?php 
$config = $this->Mmasterdata->getConfiguration();
$logged = $this->session->userdata("admin");
$dir = $this->Mmasterdata->getHomePage($logged['PRIVILEGE']);
?>
<!DOCTYPE html>

<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4 & Angular 7
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">

<!-- begin::Head -->
<head>
	<meta charset="utf-8" />
	<title>Admin - <?php echo $config->APP_NAME; ?></title>
	<meta name="description" content="Updates and statistics">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!--begin::Fonts -->
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!--end::Fonts -->

	<!--begin::Page Vendors Styles(used by this page) -->
	<link href="<?php echo base_url("assets/admin"); ?>/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url("assets/admin"); ?>/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url("assets/admin"); ?>/app/custom/wizard/wizard-v1.default.css" rel="stylesheet" type="text/css" />
	<!--RTL version:<link href="<?php echo base_url("assets/admin"); ?>/vendors/custom/fullcalendar/fullcalendar.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

	<!--end::Page Vendors Styles -->

	<!--begin::Global Theme Styles(used by all pages) -->
	<link href="<?php echo base_url("assets/admin"); ?>/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<!--RTL version:<link href="<?php echo base_url("assets/admin"); ?>/vendors/base/vendors.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
	<link href="<?php echo base_url("assets/admin"); ?>/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<!--RTL version:<link href="<?php echo base_url("assets/admin"); ?>/demo/default/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

	<!--end::Global Theme Styles -->

	<!--begin::Layout Skins(used by all pages) -->
	<link href="<?php echo base_url("assets/admin"); ?>/demo/default/skins/header/base/light.css" rel="stylesheet" type="text/css" />

	<!--RTL version:<link href="<?php echo base_url("assets/admin"); ?>/demo/default/skins/header/base/light.rtl.css" rel="stylesheet" type="text/css" />-->
	<link href="<?php echo base_url("assets/admin"); ?>/demo/default/skins/header/menu/light.css" rel="stylesheet" type="text/css" />

	<!--RTL version:<link href="<?php echo base_url("assets/admin"); ?>/demo/default/skins/header/menu/light.rtl.css" rel="stylesheet" type="text/css" />-->
	<link href="<?php echo base_url("assets/admin"); ?>/demo/default/skins/brand/dark.css" rel="stylesheet" type="text/css" />

	<!--RTL version:<link href="<?php echo base_url("assets/admin"); ?>/demo/default/skins/brand/dark.rtl.css" rel="stylesheet" type="text/css" />-->
	<link href="<?php echo base_url("assets/admin"); ?>/demo/default/skins/aside/dark.css" rel="stylesheet" type="text/css" />

	<!--RTL version:<link href="<?php echo base_url("assets/admin"); ?>/demo/default/skins/aside/dark.rtl.css" rel="stylesheet" type="text/css" />-->


	<link href="<?php echo base_url(); ?>assets/admin/css/multiple-select.css" rel="stylesheet">
	<link href="<?php echo base_url("assets/admin"); ?>/css/engine.css" rel="stylesheet" type="text/css" />

	<!--end::Layout Skins -->
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon.png" />
	
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> 
	
	<script
		src="https://code.jquery.com/jquery-2.2.4.min.js"
		integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
		crossorigin="anonymous">
	</script>

	<script type="text/javascript">
		var base_url = "<?php echo base_url(); ?>";
		var site_url = "<?php echo site_url(); ?>";
	</script>

</head>

<!-- end::Head -->

<!-- begin::Body -->
<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

	<!-- begin:: Page -->

	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
		<div class="kt-header-mobile__logo">
			<a href="<?php echo base_url(); ?>">
				<img alt="Logo" style="height: 50px;" src="<?php echo base_url("assets/img/logo-jatim-128.png"); ?>" />
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler"><span></span></button>
			<!-- <button class="kt-header-mobile__toggler" id="kt_header_mobile_toggler"><span></span></button> -->
			<button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>

	<!-- end:: Header Mobile -->
	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

			<!-- begin:: Aside -->
			<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
			<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">

				<!-- begin:: Aside -->
				<div class="kt-aside__brand kt-grid__item " id="kt_aside_brand">
					<div class="kt-aside__brand-logo">
						<a href="<?php echo base_url(); ?>">
							<img alt="Logo" src="<?php echo base_url("assets/img/logo-jatim-128.png"); ?>" style="height: 50px;" />
						</a>
					</div>
					<div class="kt-aside__brand-tools">
						<button class="kt-aside__brand-aside-toggler" id="kt_aside_toggler">
							<span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
									<path d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999) " />
									<path d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999) " />
								</g>
							</svg></span>
							<span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
									<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" />
									<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
								</g>
							</svg></span>
						</button>

							<!--
			<button class="kt-aside__brand-aside-toggler kt-aside__brand-aside-toggler--left" id="kt_aside_toggler"><span></span></button>
		-->
	</div>
</div>

<!-- end:: Aside -->

<!-- begin:: Aside Menu -->
<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
	<div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
		<ul class="kt-menu__nav ">
			<?php
			$logged = $this->session->userdata("admin");
			$homepage = $this->Mmasterdata->getAccessPrivilege($logged['PRIVILEGE']);
			$menus = $this->Mmasterdata->getMenu($logged['PRIVILEGE']);
			foreach( $menus as $menu ){
				if( $menu->PARENTMENU == "0" ){
                  // Check if has Child Menu
					$child = $this->Mmasterdata->getChildMenu( $menu->MENUID, $logged['PRIVILEGE'] );
					$childItem = "";
					$dropdownClass = "";
					$dropdownToggle = "";
					$dropdownAttr = "";
					$chevron = "";
					$href = site_url($homepage ."/" .$menu->URL);
					if( count($child) > 0 ){
						$chevron = '<i class="kt-menu__ver-arrow la la-angle-right"></i>';
						$href = "javascript:;";
						$dropdownAttr = ' aria-haspopup="true" kt-menu-submenu-toggle="hover"';
						$dropdownClass = "kt-menu__item--submenu";
						$dropdownToggle = "kt-menu__toggle";
						$childItem .= '<div class="kt-menu__submenu ">
						<span class="kt-menu__arrow">
						</span>
						<ul class="kt-menu__subnav">
						<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true">
						<span class="kt-menu__link">
						<span class="kt-menu__link-text">'.$menu->NAME.'</span>
						</span>
						</li>';
						foreach($child as $ch){
							$childItem .= '
							<li class="kt-menu__item " aria-haspopup="true">
							<a href="'.site_url($homepage ."/" . $ch->URL) .'" class="kt-menu__link main-menu">
							<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">	
							<span></span>
							</i>
							<span class="kt-menu__link-text">'.$ch->NAME.'</span>
							</a>
							</li>';

						}
						$childItem .= '</ul></div>';
					} else {
						$dropdownToggle = " main-menu"; 
					}

					echo '<li class="kt-menu__item  '.$dropdownClass.'" '.$dropdownAttr.'>
					<a href="'.$href.'" class="kt-menu__link '.$dropdownToggle.'">
					<i class="kt-menu__link-icon '.$menu->ICON.'"></i>
					<span class="kt-menu__link-text">'.$menu->NAME.'</span>
					<span id="badge_'.text2slug($menu->NAME).'"></span>
					'.$chevron.'
					</a>
					'.$childItem.'
					</li>';
				}
			}

			?>
		</ul>
	</div>
</div>

<!-- end:: Aside Menu -->
</div>

<!-- end:: Aside -->
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

	<!-- begin:: Header -->
	<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">

		<!-- begin:: Header Menu -->
		<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
		<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
			<div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">
				<h3 style="line-height: 65px;"><?php echo $config->APP_NAME; ?></h3>
			</div>
		</div>

		<!-- end:: Header Menu -->

		<!-- begin:: Header Topbar -->
		<div class="kt-header__topbar">

			<!--begin: User Bar -->
			<div class="kt-header__topbar-item kt-header__topbar-item--user">
				<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
					<div class="kt-header__topbar-user">
						<span class="kt-header__topbar-welcome kt-hidden-mobile">Hi,</span>
						<span class="kt-header__topbar-username kt-hidden-mobile"><?php $name = explode(" ",$logged['NAME']); echo isset($name[0]) ? $name[0] : $logged['NAME']; ?></span>
						<?php echo imageload($logged['PHOTO'],$logged['NAME'],base_url("uploads")); ?>
					</div>
				</div>
				<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">

					<!--begin: Head -->
					<div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-image: url(<?php echo base_url(); ?>assets/admin/media/misc/bg-1.jpg)">
						<div class="kt-user-card__avatar">
							<?php echo imageload($logged['PHOTO'],$logged['NAME'],base_url("uploads")); ?>
						</div>
						<div class="kt-user-card__name">
							<?php echo $logged['NAME']; ?> 
						</div>
						<div class="kt-user-card__badge">
							<a href="<?php echo site_url("admin/fieldcertificate"); ?>" class="main-menu"><span class="btn btn-success btn-sm btn-bold btn-font-md" id="pendingNotif">0 Pending</span></a>
						</div>
					</div>

					<!--end: Head -->

					<!--begin: Navigation -->
					<div class="kt-notification">
						<a href="<?php echo $dir . "/profile"; ?>" class="main-menu kt-notification__item">
							<div class="kt-notification__item-icon">
								<i class="flaticon2-calendar-3 kt-font-success"></i>
							</div>
							<div class="kt-notification__item-details">
								<div class="kt-notification__item-title kt-font-bold">
									Profil Pribadi
								</div>
								<div class="kt-notification__item-time">
									Update informasi akun pribadi Anda dalam aplikasi
								</div>
							</div>
						</a>
						<a href="<?php echo $dir . "/change_password" ?>" class="main-menu kt-notification__item">
							<div class="kt-notification__item-icon">
								<i class="flaticon2-rocket-1 kt-font-danger"></i>
							</div>
							<div class="kt-notification__item-details">
								<div class="kt-notification__item-title kt-font-bold">
									Ubah Password
								</div>
								<div class="kt-notification__item-time">
									Ubah Password Login Anda untuk aplikasi
								</div>
							</div>
						</a>
						<div class="kt-notification__custom">
							<a href="<?php echo site_url("logout"); ?>" class="btn btn-label-brand btn-sm btn-bold">Keluar</a>
						</div>
					</div>

					<!--end: Navigation -->
				</div>
			</div>

			<!--end: User Bar -->
		</div>

		<!-- end:: Header Topbar -->
	</div>

	<!-- end:: Header -->
	<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

		<!-- begin:: Content -->
		<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="main-content">
				
		</div>
	  <script type="text/javascript">
	  	$(document).ready(function(){
	    	$("#main-content").load( site_url + "/admin/dashboard/");
	  	});
	  </script>

		<!-- end:: Content -->
	</div>

	<!-- begin:: Footer -->
	<div class="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop">
		<div class="kt-footer__copyright">
			<?php echo date("Y") ?>&nbsp;&copy;&nbsp;<a href="http://pertanian.jatimprov.go.id" target="_blank" class="kt-link"><?php echo $config->OFFICE_NAME; ?></a>
		</div>
		<?php /*
		<div class="kt-footer__menu">
			<a href="javascript:popup('<?php echo base_url("welcome/about") ?>');" target="_blank" class="kt-footer__menu-link kt-link">About</a>
			<a href="javascript:popup('<?php echo base_url("welcome/team") ?>');" target="_blank" class="kt-footer__menu-link kt-link">Team</a>
			<a href="javascript:popup('<?php echo base_url("welcome/contact") ?>');" target="_blank" class="kt-footer__menu-link kt-link">Contact</a>
		</div>
		*/ ?>
	</div>

	<!-- end:: Footer -->
</div>
</div>
</div>

<!-- end:: Page -->

<!-- begin:: Modal -->

<div class="modal fade bd-example-modal-lg" id="engineModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body" id="modalContainer">
        
      </div>
      <div class="modal-footer" id="modalFooter">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      	<button type="button" style="display:none;" onclick="print('modalContainer');" id="btn-print" class="btn btn-primary"><i class="la la-print"></i> Print</button>
      	<button type="button" style="display:none;" id="btn-save" class="btn btn-primary"><i class="la la-save"></i> Save</button>
      </div>
    </div>
  </div>
</div>

<!-- end:: Modal -->

<!-- begin::Scrolltop -->
<div id="kt_scrolltop" class="kt-scrolltop">
	<i class="fa fa-arrow-up"></i>
</div>

<!-- end::Scrolltop -->

<!-- begin::Global Config(global config for global JS sciprts) -->
<script>
	var KTAppOptions = {
		"colors": {
			"state": {
				"brand": "#5d78ff",
				"dark": "#282a3c",
				"light": "#ffffff",
				"primary": "#5867dd",
				"success": "#34bfa3",
				"info": "#36a3f7",
				"warning": "#ffb822",
				"danger": "#fd3995"
			},
			"base": {
				"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
				"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
			}
		}
	};
</script>

<!-- end::Global Config -->

<!--begin::Global Theme Bundle(used by all pages) -->
<script src="<?php echo base_url("assets/admin"); ?>/vendors/base/vendors.bundle.js" type="text/javascript"></script>
<script src="<?php echo base_url("assets/admin"); ?>/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

<!--end::Global Theme Bundle -->

<!--begin::Page Vendors(used by this page) -->
<script src="<?php echo base_url("assets/admin"); ?>/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
<script src="//maps.google.com/maps/api/js?key=<?php echo $config->GOOGLE_MAP_API; ?>&libraries=places" type="text/javascript"></script>
<script src="<?php echo base_url("assets/admin"); ?>/vendors/custom/gmaps/gmaps.js" type="text/javascript"></script>

<!--end::Page Vendors -->

<!--begin::Page Scripts(used by this page) -->
<script src="<?php echo base_url("assets/admin"); ?>/app/custom/general/dashboard.js" type="text/javascript"></script>

<!--end::Page Scripts -->
<script type="text/javascript" src="<?php echo base_url("assets/admin"); ?>/js/jquery.ajaxfileupload.js"></script>
<script type="text/javascript" src="<?php echo base_url("bower_components"); ?>/tinymce/jquery.tinymce.min.js"></script>
<script type="text/javascript" src="<?php echo base_url("bower_components"); ?>/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="<?php echo base_url("assets/admin"); ?>/js/bootbox.js"></script>
<script type="text/javascript" src="<?php echo base_url("assets/admin"); ?>/js/tagsinput.js"></script>
<script type="text/javascript" src="<?php echo base_url("assets/admin"); ?>/js/moment.min.js"></script>

<script src="<?php echo base_url("assets/admin"); ?>/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo base_url("assets/admin"); ?>/vendors/data-tables/dataTables.tableTools.js"></script>
<script type="text/javascript" src="<?php echo base_url("assets/admin"); ?>/vendors/data-tables/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="<?php echo base_url("assets/admin"); ?>/vendors/data-tables/dataTables.responsive.js"></script>
<script type="text/javascript" src="<?php echo base_url("assets/admin"); ?>/js/multiple-select.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap4.min.js"></script>

<script type="text/javascript" src="<?php echo base_url("assets/admin"); ?>/js/jquery.PrintArea.js"></script>
<script type="text/javascript" src="<?php echo base_url("bower_components"); ?>/js-xlsx/dist/xlsx.core.min.js"></script>
<script type="text/javascript" src="<?php echo base_url("bower_components"); ?>/blobjs/Blob.min.js"></script>
<script type="text/javascript" src="<?php echo base_url("bower_components"); ?>/file-saverjs/FileSaver.js"></script>
<script type="text/javascript" src="<?php echo base_url("bower_components"); ?>/tableexport.js/dist/js/tableexport.min.js"></script>
<script type="text/javascript" src="<?php echo base_url("bower_components"); ?>/typeahead.js/dist/bloodhound.min.js"></script>
<script type="text/javascript" src="<?php echo base_url("bower_components"); ?>/typeahead.js/dist/typeahead.bundle.min.js"></script>
<script type="text/javascript" src="<?php echo base_url("bower_components"); ?>/typeahead.js/dist/typeahead.jquery.min.js"></script>

<!--begin::Global App Bundle(used by all pages) -->
<script src="<?php echo base_url("assets/admin"); ?>/app/bundle/app.bundle.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo base_url("assets/admin/js/polyfiller.js") ?>"></script>
<script src="<?php echo base_url("assets/admin"); ?>/js/engine.js" type="text/javascript"></script>

<!--end::Global App Bundle -->
<script type="text/javascript">
  // KTSweetAlert2Demo.init();
  tinymce.init({
    plugins: [
      'advlist autolink lists link image charmap print preview hr anchor pagebreak',
      'searchreplace wordcount visualblocks visualchars code fullscreen',
      'insertdatetime media nonbreaking save table contextmenu directionality',
      'emoticons paste textcolor colorpicker textpattern imagetools'
    ],
    toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image code',
    toolbar2: 'print preview media | forecolor backcolor emoticons | table',

    // image_advtab: true,
    relative_urls : false,
    remove_script_host : false,
    convert_urls : true,

    width: '100%',
    menubar : false,
    image_title: true, 
    image_dimensions: true,
    automatic_uploads: true,
    images_upload_url: './upload/tinyMCEUpload/',
    images_upload_credentials: true,
  });

  function tinyMCEUpload(fileid){
    $.ajaxFileUpload({
      url:"<?php echo site_url(); ?>/upload/tinyMCEUpload/",
      secureuri:false,
      fileElementId:fileid,
      dataType: 'json',
      data:{name:'logan', id:'id'},
      success: function (data, status){
        console.log(data);
        if(typeof(data.error) != 'undefined'){
          if(data.error != ''){
            bootbox.alert(data.error);
          } else {
            return data.msg;
          }
        }
      },
      error: function (data, status, e)
      {
        bootbox.alert(e);
      }
    })
    return false;
  }

  function ajaxupload(fileid){
    $.ajaxFileUpload({
      url:"<?php echo site_url(); ?>upload/uploadfile/fileName/FILE_" + fileid,
      secureuri:false,
      fileElementId:'FILE_'+fileid,
      dataType: 'json',
      data:{name:'logan', id:'id'},
      success: function (data, status){
        if(typeof(data.error) != 'undefined'){
          if(data.error != ''){
            bootbox.alert(data.error);
          } else {
            $("#"+fileid).val(data.msg);
          }
        }
      },
      error: function (data, status, e)
      {
        bootbox.alert(e);
      }
    })
    return false;
  }
</script>
</body>

<!-- end::Body -->
</html>