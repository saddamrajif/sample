<?php 
$config = $this->Mmasterdata->getConfiguration();
?>

<!DOCTYPE html>

<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4 & Angular 7
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">

<!-- begin::Head -->
<head>
	<meta charset="utf-8" />
	<title>Login - <?php echo $config->APP_NAME; ?></title>
	<meta name="description" content="Login page example">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!--begin::Fonts -->
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!--end::Fonts -->

	<!--begin::Page Custom Styles(used by this page) -->
	<link href="<?php echo base_url("assets/admin"); ?>/app/custom/login/login-v2.default.css" rel="stylesheet" type="text/css" />

	<!--end::Page Custom Styles -->

	<!--begin::Global Theme Styles(used by all pages) -->
	<link href="<?php echo base_url("assets/admin"); ?>/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<!--RTL version:<link href="<?php echo base_url("assets/admin"); ?>/vendors/base/vendors.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
	<link href="<?php echo base_url("assets/admin"); ?>/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<!--RTL version:<link href="<?php echo base_url("assets/admin"); ?>/demo/default/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

	<!--end::Global Theme Styles -->

	<!--begin::Layout Skins(used by all pages) -->
	<link href="<?php echo base_url("assets/admin"); ?>/demo/default/skins/header/base/light.css" rel="stylesheet" type="text/css" />

	<!--RTL version:<link href="<?php echo base_url("assets/admin"); ?>/demo/default/skins/header/base/light.rtl.css" rel="stylesheet" type="text/css" />-->
	<link href="<?php echo base_url("assets/admin"); ?>/demo/default/skins/header/menu/light.css" rel="stylesheet" type="text/css" />

	<!--RTL version:<link href="<?php echo base_url("assets/admin"); ?>/demo/default/skins/header/menu/light.rtl.css" rel="stylesheet" type="text/css" />-->
	<link href="<?php echo base_url("assets/admin"); ?>/demo/default/skins/brand/dark.css" rel="stylesheet" type="text/css" />

	<!--RTL version:<link href="<?php echo base_url("assets/admin"); ?>/demo/default/skins/brand/dark.rtl.css" rel="stylesheet" type="text/css" />-->
	<link href="<?php echo base_url("assets/admin"); ?>/demo/default/skins/aside/dark.css" rel="stylesheet" type="text/css" />

	<!--RTL version:<link href="<?php echo base_url("assets/admin"); ?>/demo/default/skins/aside/dark.rtl.css" rel="stylesheet" type="text/css" />-->

	<!--end::Layout Skins -->
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon.png" />
	
	<script type="text/javascript">
		var base_url = "<?php echo base_url(); ?>";
		var site_url = "<?php echo site_url(); ?>";
	</script>
</head>

<!-- end::Head -->

<!-- begin::Body -->
<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

	<!-- begin:: Page -->
	<div class="kt-grid kt-grid--ver kt-grid--root">
		<div class="kt-grid kt-grid--hor kt-grid--root kt-login kt-login--v2 kt-login--signin" id="kt_login">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-image: url(<?php echo base_url("assets/admin"); ?>/media/bg/bg-1.jpg);">
				<div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
					<div class="kt-login__container">
						<div class="kt-login__logo">
							<a href="<?php echo base_url(); ?>">
								<img src="<?php echo base_url("assets/img/logo-jatim-128.png"); ?>" style="height: 100px;">
							</a>
							<p class="text-white"><?php echo date("Y"); ?> - <?php echo $config->OFFICE_NAME; ?></p>
						</div>
						<div class="kt-login__signin">
							<div class="kt-login__head">
								<h3 class="kt-login__title"><?php echo $config->APP_NAME; ?> - <?php echo $config->APP_SHORTNAME; ?></h3>
							</div>
							<form class="kt-form" action="" id="loginForm">
								<div id="loginStatus">
									
								</div>
								<div class="input-group">
									<input class="form-control text-white" type="text" placeholder="Username / Email" name="username" id="username" autocomplete="off">
								</div>
								<div class="input-group">
									<input class="form-control text-white" type="password" placeholder="Password" name="password" id="password">
								</div>
								<div class="row kt-login__extra">
									<div class="col">
										
									</div>
									<div class="col kt-align-right">
										<a href="javascript:;" id="kt_login_forgot" class="kt-link kt-login__link">Forget Password ?</a>
									</div>
								</div>
								<div class="kt-login__actions">
									<button id="kt_login_signin_submit" class="btn btn-pill kt-login__btn-primary">Sign In</button>
								</div>
							</form>
						</div>
						<div class="kt-login__forgot">
							<div class="kt-login__head">
								<h3 class="kt-login__title">Lupa Password ?</h3>
								<div class="kt-login__desc">Masukkan Email Anda untuk reset Password:</div>
							</div>
							<form class="kt-form" action="">
								<div id="resetStatus"></div>
								<div class="input-group">
									<input class="form-control text-white" type="text" placeholder="Email" name="email" id="email" autocomplete="off">
								</div>
								<div class="kt-login__actions">
									<button id="kt_login_forgot_submit" class="btn btn-pill kt-login__btn-primary">Request</button>&nbsp;&nbsp;
									<button id="kt_login_forgot_cancel" class="btn btn-pill kt-login__btn-secondary">Cancel</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- end:: Page -->

	<!-- begin::Global Config(global config for global JS sciprts) -->
	<script>
		var KTAppOptions = {
			"colors": {
				"state": {
					"brand": "#5d78ff",
					"dark": "#282a3c",
					"light": "#ffffff",
					"primary": "#5867dd",
					"success": "#34bfa3",
					"info": "#36a3f7",
					"warning": "#ffb822",
					"danger": "#fd3995"
				},
				"base": {
					"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
					"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
				}
			}
		};
	</script>

	<!-- end::Global Config -->

	<!--begin::Global Theme Bundle(used by all pages) -->
	<script src="<?php echo base_url("assets/admin"); ?>/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<?php echo base_url("assets/admin"); ?>/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

	<!--end::Global Theme Bundle -->

	<!--begin::Page Scripts(used by this page) -->
	<script type="text/javascript">
		var login = $('#kt_login');
		$("#loginForm").submit(function( e ){
			e.preventDefault();
			var form = $("#loginForm");
			var btn = $("#kt_login_signin_submit");
			btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
			form.validate({
				rules: {
					username: {
						required: true
					},
					password: {
						required: true
					}
				}
			});

			if (! form.valid() ) {
        	btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false); // remove
        	return false;
        }

        $("#loginStatus").html( "<div class='alert'>Memproses...</div>" );
        var data = $(this).serialize();
        var target = site_url + "adminlogin/doLogin";
        $.post(target,data,function( e ){
          // $("#loginStatus").fadeOut();
          $("#loginStatus").html( e );
          // $("#loginStatus").fadeIn();
        });
        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false); // remove
        return false;
      });

		$("#kt_login_forgot_submit").click(function(e){
			e.preventDefault();

			var btn = $(this);
			var form = $(this).closest('form');

			form.validate({
				rules: {
					email: {
						required: true,
						email: true
					}
				}
			});

			if (!form.valid()) {
				return false;
			}

			btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

			var target = site_url + "/lostpassword/index/";
			var datapost = form.serialize();
			$.post(target, datapost, function( e ){
				setTimeout(function() {
        		btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false); // remove
            form.clearForm(); // clear form
            form.validate().resetForm(); // reset validation states

            // displaySignInForm();
            login.removeClass('kt-login--forgot');
            login.removeClass('kt-login--signup');

            login.addClass('kt-login--signin');
            KTUtil.animateClass(login.find('.kt-login__signin')[0], 'flipInX animated');
            var signInForm = login.find('.kt-login__signin form');
            signInForm.clearForm();
            signInForm.validate().resetForm();

            $("#loginStatus").html( e );

            // showErrorMsg(signInForm, 'success', 'Cool! Password recovery instruction has been sent to your email.');
          }, 2000);
			});
		});
      // $("#captcha_container").click(function(){
      //   $(this).load(site_url + "login/loadCaptcha");
      // });
      // $("#captcha_container").load(site_url + "login/loadCaptcha");
    </script>
    
    <script src="<?php echo base_url("assets/admin"); ?>/app/custom/login/login-general.js" type="text/javascript"></script>

    <!--end::Page Scripts -->

    <!--begin::Global App Bundle(used by all pages) -->
    <script src="<?php echo base_url("assets/admin"); ?>/app/bundle/app.bundle.js" type="text/javascript"></script>

    <!--end::Global App Bundle -->
  </body>

  <!-- end::Body -->
  </html>