<style>
	.print{
		font-family:  Lato;
	}
	.print p{
		color: #333;
		font-size: 16pt;
		margin-bottom: 24px;
		line-height: 20pt;
	}
	.print tr td{
		color: #333;
		font-size: 16pt;	
	}
</style>
<div class="print">
<h4 class="text-center">FORMULIR PERMOHONAN REGISTRASI AWAL</h4><br />
<table class="table table-borderless table-sm">
	<tr>
		<td style="width:200px">Nomor</td>
		<td>:</td>
		<td>........................</td>
		<td style="width:400px;" rowspan="3"><?php echo $fieldcertificate->CITY . ", " . date_to_ID($fieldcertificate->SUBMISSIONDATE) ?></td>
	</tr>
	<tr>
		<td>Lampiran</td>
		<td>:</td>
		<td>1 (satu) berkas</td>
	</tr>
	<tr>
		<td>Perihal</td>
		<td>:</td>
		<td>Permohonan Registrasi Kebun/lahan usaha Buah dan Sayur GAP</td>
	</tr>
</table>
<table class="table table-borderless table-sm">
	<tr>
		<td>
			<p>
				Kepada Yth. <br />
				Kepala Dinas Pertanian Kabupaten/Kota <?php echo $fieldcertificate->CITY; ?> <br />
				Di <br />
				<?php echo $fieldcertificate->CITY; ?>
			</p>
			<p>Dengan hormat,</p>
			<p>
				Bersama ini kami sampaikan permohonan agar kiranya kebun/lahan usaha buah dan
				sayur yang kami kelola dapat diregistrasi sebagai kebun/lahan usaha buah dan sayur
				GAP sesuai dengan aturan yang berlaku.
			</p>
			<p>
				Adapun data dan informasi teknis mengenai kebun/lahan usaha yang akan diregistrasi 
				sebagaimana terlampir.
			</p>
			<p>
				Selanjutnya kami mohon kesediaannya untuk permohonan ini dapat memproses lebih lanjut
			</p>
			<p>	
				Demikian, atas perhatiannya disampaikan terimakasih.
			</p>
			<p>
				Hormat kami
			</p>
			<p>
				Pemohon,
				<br /><br /><br /><br /><br /><br />
				<?php echo $fieldcertificate->FARMER; ?>
			</p>
		</td>
	</tr>
	<tr>
		<td>
			Tembusan Kepada Yth:<br />
			- Kepala Dinas Pertanian Provinsi Jawa Timur
		</td>
	</tr>
</table>

<div style="page-break-after: always;"></div>
<h3 style="text-align:center">DATA PERMOHONAN REGISTRASI AWAL</h3>
<br />
<h3>A. Data Pemohon</h3>
	<table class="table">
		<tr>
			<td>Jenis Pengajuan Registrasi</td>
			<td style="width: 2px;">:</td>
			<td><?php echo $fieldcertificate->APPLICANTTYPE=='PTN' ? "<i class='la la-check-square'></i>" : "<i class='la la-square-o'></i>" ; ?> Perorangan &nbsp;&nbsp;&nbsp;&nbsp; <?php echo $fieldcertificate->APPLICANTTYPE=='KPT' ? "<i class='la la-check-square'></i>" : "<i class='la la-square-o'></i>" ; ?> Kelompok</td>
		</tr>
		<tr>
			<td>Nama</td>
			<td style="width: 2px;">:</td>
			<td><?php echo $fieldcertificate->FARMER; ?></td>
		</tr>
		<tr>
			<td>Alamat</td>
			<td style="width: 2px;">:</td>
			<td><?php echo $fieldcertificate->ADDRESS; ?></td>
		</tr>
		<tr>
			<td>Telepon</td>
			<td style="width: 2px;">:</td>
			<td><?php echo $fieldcertificate->MOBILE; ?></td>
		</tr>
		<tr>
			<td>Alamat Email</td>
			<td style="width: 2px;">:</td>
			<td><?php echo $fieldcertificate->EMAIL; ?></td>
		</tr>
		<tr>
			<td>Alamat/Lokasi Kebun/Lahan Usaha</td>
			<td style="width: 2px;">:</td>
			<td><?php echo $field->FIELDADDRESS; ?></td>
		</tr>
		<tr>
			<td>Luas Kebun / Lahan Usaha</td>
			<td style="width: 2px;">:</td>
			<td><?php echo $field->FIELDSIZE; ?> ha</td>
		</tr>
		<tr>
			<td>Komoditas yang akan diregistrasi</td>
			<td style="width: 2px;">:</td>
			<td><?php echo $fieldcertificate->COMMODITY; ?></td>
		</tr>
		
	</table>
<h3>B. Informasi Awal Kebun / Lahan Usaha</h3>
<table class="table table-bordered">
	<thead>
		<tr>
			<td style="background: #ddd;font-weight: bold;">PERTANYAAN</td>
			<td style="width:100px; background: #ddd;font-weight: bold;">Ya</td>
			<td style="width:100px; background: #ddd;font-weight: bold;">Tidak</td>
		</tr>
	</thead>
	<tbody>
		<?php 
		$seq = 1;
		foreach($certificateterms as $terms){ ?>
		<tr>
			<td><?php echo $seq . ". " . $terms['NAME']; ?></td>
			<td><?php echo ($terms['CERTIFICATETERMSTATUS'] == 1 ) ? "<i class='la la-check'></li>" : "" ?></td>
			<td><?php echo ($terms['CERTIFICATETERMSTATUS'] == 0 ) ? "<i class='la la-check'></li>" : "" ?></td>
		</tr>
	<?php $seq++; } ?>
	</tbody>
</table>
<h3>Gambar Denah Pencapaian Lokasi Kebun / Lahan Usaha</h3>
<img src="<?php echo base_url( image_check($field->FIELDMAP) ); ?>" style="width: 100%;" />

<table class="table table-bordered">
	<tr>
		<td background="#ddd">
			Persetujuan dan Kesepakatan
		</td>
	</tr>
	<tr>
		<td>
			Dengan ini saya menyatakan bahwa informasi yang saya berikan di atas adalah benar, dan saya setuju untuk mengikuti aturan yang berlaku pada proses registrasi kebun / lahan usaha.
			<br />
			<table class="table table-borderless">
				<tr>
					<td style="width: 200px;">Nama</td>
					<td style="width: 2px;">:</td>
					<td>..............................</td>
				</tr>
				<tr>
					<td>Jabatan</td>
					<td>:</td>
					<td>..............................</td>
				</tr>
				<tr>
					<td>Tempat, Tanggal</td>
					<td>:</td>
					<td>..............................</td>
				</tr>
			</table>
			<br /><br /><br /><br />
			<u>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</u><br />
			Nama Jelas & Tanda Tangan
		</td>
	</tr>
</table>
<div style="page-break-after: always;"></div>

<?php 
	$config = $this->Mmasterdata->getConfiguration();
?>
<table class="table table-borderless" style="border:3px solid #555;">
	<tr>
		<td style="border-bottom: 2px solid #444;">
			<table id="table-header" class="table no-border" cellspacing="0" cellpadding="5" border="0">
				<tbody>
					<tr>
						<td style="width:100px;">
							<img src="<?php echo base_url("assets") ?>/img/logo-jatim-128.png" style="width:100px">
						</td>
						<td>
							<h4 class="text-center">PEMERINTAH PROVINSI JAWA TIMUR</h4>
							<h3 class="text-center"><?php echo $config->OFFICE_NAME; ?></h3>
							<p class="text-center"><?php echo $config->OFFICE_ADDRESS ?></p>
						</td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<h2 class="text-center">SURAT KETERANGAN REGISTRASI</h2>
			<p class="text-center font-weight-bold">NOMOR : <?php echo $fieldcertificate->CERTIFICATENUMBER; ?></p>
			<br /><br />
			<p class="text-center">Diberikan kepada :</p>
			<table class="table table-borderless table-sm" align="center" style="width: 60%">
				<tr>
					<td colspan="3" style="text-align:center;"><h3><?php echo $fieldcertificate->FARMER . " / KELOMPOK TANI "; ?>"<?php echo $fieldcertificate->FARMERGROUP; ?>"</h3></td>
				</tr>
				<tr>
					<td style="width:200px;">Komoditas</td>
					<td>:</td>
					<td><?php echo $fieldcertificate->COMMODITY; ?></td>
				</tr>
				<tr>
					<td>Luas Lahan</td>
					<td>:</td>
					<td><?php echo $fieldsize->FIELDSIZE; ?> ha</td>
				</tr>
				<tr>
					<td>Alamat Kebun/Lahan Usaha</td>
					<td>:</td>
					<td><?php echo $fieldsize->FIELDADDRESS; ?></td>
				</tr>
			</table>
			<br />
			<p class="text-center">Menerangkan bahwa nama dan alamat kebun/lahan usaha yang
			dikelola telah memenuhi persyaratan GAP Buah dan Sayur berdasar
			PERMENTAN No. 48/Permentan/OT.140/10/2009 dengan No. Registrasi :</p>
			<table class="table table-borderless">
				<tr>
					<td style="width:25%"></td>
					<td style="border:1.5px solid #333;padding: 12px;text-align:center;">
						<h1><?php echo $fieldcertificate->CERTIFICATECODE; ?></h1>
					</td>
					<td style="width:25%"></td>
				</tr>
			</table>
			<p class="text-center">Berlaku untuk jangka waktu 2 (dua) tahun sejak tanggal ditetapkan</p>
			<br /><br />
			<p class="text-center">Ditetapkan oleh :</p>
			<table class="table table-borderlesss" style="width:70%" align="center">
				<tr>
					<td style="width:250px;">Dinas Pertanian</td>
					<td style="width:2px;">:</td>
					<td><?php echo $config->OFFICE_NAME; ?></td>
				</tr>
				<tr>
					<td style="width:250px;">Tanggal</td>
					<td style="width:2px;">:</td>
					<td><?php echo $fieldcertificate->ACTIVESTARTDATE != "0000-00-00" ? date_to_ID($fieldcertificate->ACTIVESTARTDATE) : "" ; ?></td>
				</tr>
				<tr>
					<td colspan="3" style="text-align:center;">
						Kepala Dinas Pertanian Provinsi Jawa Timur<br /><br /><br /><br /><br /><br />
						<strong><u><?php echo $config->TTD1NAME; ?></u></strong><br />
						NIP. <?php echo $config->TTD1NIP; ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</div>