<div class="kt-grid  kt-wizard-v1 kt-wizard-v1--white" id="farmer_wizard" data-ktwizard-state="step-first">
	<div class="kt-grid__item">

		<!--begin: Form Wizard Nav -->
		<div class="kt-wizard-v1__nav">
			<div class="kt-wizard-v1__nav-items">
				<a class="kt-wizard-v1__nav-item" href="#" data-ktwizard-type="step" data-ktwizard-state="current">
					<div class="kt-wizard-v1__nav-body">
						<div class="kt-wizard-v1__nav-icon">
							<i class="la la-user"></i>
						</div>
						<div class="kt-wizard-v1__nav-label">
							1) Data Petani
						</div>
					</div>
				</a>
				<a class="kt-wizard-v1__nav-item" href="#" data-ktwizard-type="step">
					<div class="kt-wizard-v1__nav-body">
						<div class="kt-wizard-v1__nav-icon">
							<i class="la la-photo"></i>
						</div>
						<div class="kt-wizard-v1__nav-label">
							2) Data Lahan
						</div>
					</div>
				</a>
				<a class="kt-wizard-v1__nav-item" href="#" data-ktwizard-type="step">
					<div class="kt-wizard-v1__nav-body">
						<div class="kt-wizard-v1__nav-icon">
							<i class="fa fa-chess"></i>
						</div>
						<div class="kt-wizard-v1__nav-label">
							3) Alsintan
						</div>
					</div>
				</a>
				<a class="kt-wizard-v1__nav-item" href="#" data-ktwizard-type="step">
					<div class="kt-wizard-v1__nav-body">
						<div class="kt-wizard-v1__nav-icon">
							<i class="la la-history"></i>
						</div>
						<div class="kt-wizard-v1__nav-label">
							4) Masa Tanam / Panen
						</div>
					</div>
				</a>

			</div>
		</div>

		<!--end: Form Wizard Nav -->
	</div>
	<div class="kt-grid__item kt-grid__item--fluid kt-wizard-v1__wrapper">

		<!--begin: Form Wizard Form-->
		<div class="kt-form" id="kt_form">

			<!--begin: Form Wizard Step 1-->
			<div class="kt-wizard-v1__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
				<div class="kt-heading kt-heading--md">Masukkan data Detail Petani</div>
				<div class="kt-form__section kt-form__section--first">
					<div id="farmer_form">

					</div>
					<script type="text/javascript">
						$(function(){
							$("#farmer_form").load(site_url + "/admin/farmer_part/<?php echo $action; ?>");
						});
					</script>
				</div>
			</div>
			<!--end: Form Wizard Step 1-->

			<!--begin: Form Wizard Step 2-->
			<div class="kt-wizard-v1__content" data-ktwizard-type="step-content">
				<div class="kt-heading kt-heading--md">Masukkan data Kebun / Lahan Usaha</div>
				<div class="kt-form__section kt-form__section--first">
					<div id="farmer_field">
									
					</div>
				</div>
			</div>
			<!--end: Form Wizard Step 2-->

			<!--begin: Form Wizard Step 3-->
			<div class="kt-wizard-v1__content" data-ktwizard-type="step-content">
				<div class="kt-heading kt-heading--md">Masukkan data Alsintan</div>
				<div class="kt-form__section kt-form__section--first">
					<div id="farmer_tools">
									
					</div>
				</div>
			</div>
			<!--end: Form Wizard Step 3-->

			<!--begin: Form Wizard Step 4-->
			<div class="kt-wizard-v1__content" data-ktwizard-type="step-content">
				<div class="kt-heading kt-heading--md">Tanggal Tanam dan Panen</div>
				<div class="kt-form__section kt-form__section--first">
					<div id="farmer_planharvest">
									
					</div>
				</div>
			</div>
			<!--end: Form Wizard Step 4-->

			<!--begin: Form Actions -->
			<div class="kt-form__actions">
				<div class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-prev">
					<< Sebelumnya
				</div>
				<div class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-submit">
					Simpan
				</div>
				<div class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-next">
					Selanjutnya >>
				</div>
			</div>
		</div>
		<!--end: Form Wizard Form-->
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		wizard = new KTWizard('farmer_wizard', {
			startStep: 1,
    	manualStepForward: false
		});
		wizard.on('beforeNext', function(wizardObj) {
			var step = wizard.getStep();
			switch( step ){
				case 1:
					var posting = $("#farmer_form form").submit();
					$('[data-ktwizard-type=action-next]').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
					setTimeout("$('[data-ktwizard-type=action-next]').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').removeAttr('disabled')",500);
					$("#farmer_field").load( site_url + "admin/farmer_field_part");
					break;
				case 2:
					$('[data-ktwizard-type=action-next]').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
					setTimeout("$('[data-ktwizard-type=action-next]').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').removeAttr('disabled')",500);
					$("#farmer_tools").load( site_url + "admin/farmer_tools_part/");
					break;
				case 3:
					$('[data-ktwizard-type=action-next]').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
					setTimeout("$('[data-ktwizard-type=action-next]').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').removeAttr('disabled')",500);
					$("#farmer_planharvest").load( site_url + "admin/farmer_planharvest_part");
					break;
			}
		});
		$("[data-ktwizard-type='action-submit']").on("click",function(){
			swal.fire({
				title: "",
				text: "Data Petani Telah Update",
				type: "success",
				confirmButtonClass: "btn btn-primary"
			});
			<?php 
			$logged = $this->session->userdata("admin");
			if( $logged['PRIVILEGE'] == "PTN"){ ?>
				loadcontent("engine-content", site_url + "admin/farmerprofile");
			<?php } else { ?>
				loadcontent("engine-content", site_url + "admin/farmer");
			<?php } ?>
		});
	});

	/*function validateStep( data, e ){
		if( data != true ){
			e.goPrev();
			swal.fire({
				title: "",
				text: data,
				type: "error",
				confirmButtonClass: "btn btn-secondary"
			});
		}
	}*/
</script>