<?php $this->load->view("public/header"); ?>
<div class="ogami-breadcrumb">
  <div class="container">
    <ul>
      <li> <a class="breadcrumb-link" href="<?php echo base_url(); ?>"> <i class="fas fa-home"></i>Home</a></li>
      <li> <a class="breadcrumb-link active" href="<?php echo site_url("shop"); ?>">Shop</a></li>
    </ul>
  </div>
</div>
<!-- End breadcrumb-->
<div class="shop-layout">
  <div class="container">
    <div class="row">
      <div class="col-xl-3">
        <div class="shop-sidebar">
          <?php $this->load->view("public/sidebar"); ?>
        </div>
        <div class="filter-sidebar--background" style="display: none"></div>
      </div>
      <div class="col-xl-9">
        <div class="shop-grid-list">
          <div class="shop-products">
            <div class="shop-products_top mini-tab-title underline">
              <div class="row align-items-center">
                <div class="col-6 col-xl-4">
                  <h2 class="title">Temukan Kebutuhanmu</h2>
                </div>
                <div class="col-12 col-xl-8">
                  <div class="product-option">
                    <div class="product-filter">
                      <form id="sortForm" method="POST">
                        <select class="select-form" id="SORT" name="SORT">
                          <option value="az" <?php echo ($order_by['field'] == "products.NAME" && $order_by['seq'] == "ASC") ? "selected" : null ; ?>>A to Z</option>
                          <option value="za" <?php echo ($order_by['field'] == "products.NAME" && $order_by['seq'] == "DESC") ? "selected" : null ; ?>>Z to A</option>
                          <option value="hp" <?php echo ($order_by['field'] == "products.PRICE" && $order_by['seq'] == "DESC") ? "selected" : null ; ?>>Harga Termahal</option>
                          <option value="lp" <?php echo ($order_by['field'] == "products.PRICE" && $order_by['seq'] == "ASC") ? "selected" : null ; ?>>Harga Termurah</option>
                        </select>
                        <select class="select-form" id="LIMIT" name="LIMIT">
                          <option value="12" <?php echo ($limit == 12) ? "selected" : null ; ?>>Show 12</option>
                          <option value="30" <?php echo ($limit == 30) ? "selected" : null ; ?>>Show 30</option>
                          <option value="90" <?php echo ($limit == 90) ? "selected" : null ; ?>>Show 90</option>
                        </select>
                      </form>
                    </div>
                    <div class="view-method">
                      <p class="active" id="grid-view"><i class="fas fa-th-large"></i></p>
                      <p id="list-view"><i class="fas fa-list"></i></p>
                    </div>
                  </div>
                </div>
              </div>
              <!--Using column-->
            </div>
            <div class="shop-products_bottom">
              <div class="row no-gutters-sm" id="productList">
                <?php 
                if( count($products) == 0 ){
                  echo "<div class='alert alert-danger'>Maaf, saat ini belum ada produk tersedia</div>";
                } else {
                  foreach( $products as $pro){ ?>
                <div class="col-6 col-md-4">
                  <div class="product">
                    <div class="product-img_block">
                      <a class="product-img" href="<?php echo site_url("shop/detail/" . $pro->PRODUCTID . "/" . text2slug($pro->NAME)) ?>">
                        <img src="<?php echo base_url(image_check($pro->IMAGE1)) ?>" alt="<?php echo $pro->NAME; ?>">
                      </a>
                      <?php /* <button class="quickview no-round-btn smooth">Quick view</button> */ ?>
                    </div>
                    <div class="product-info_block">
                      <h5 class="product-type"><?php echo $pro->COMMODITYCATEGORY ?></h5>
                      <a class="product-name" href="<?php echo site_url("shop/detail/" . $pro->PRODUCTID . "/" . text2slug($pro->NAME)) ?>"><?php echo $pro->NAME; ?></a>
                      <h3 class="product-price">Rp <?php echo number_format($pro->PRICE,0,",",".") ?>
                        <del>Rp <?php echo number_format($pro->PRICE_ST,0,",",".") ?></del>
                      </h3>
                      <!-- <h5 class="product-rated"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star-half"></i><span>(5)</span></h5> -->
                      <p class="product-describe"><?php echo get_excerpt($pro->DESCRIPTION); ?></p>
                      <!-- <h5 class="product-avaiable">Avability: <span>5 In stock</span></h5> -->
                      <!-- <button class="add-to-wishlist button-borderless"><i class="icon_heart_alt"></i></button> -->
                    </div>
                    <div class="product-select_list">
                      <!-- <p class="delivery-status">Gratis Ongkir</p> -->
                      <h3 class="product-price"> 
                        <del>Rp <?php echo number_format($pro->PRICE_ST,0,",",".") ?></del>Rp <?php echo number_format($pro->PRICE,0,",",".") ?>
                      </h3>
                      <!-- <button class="add-to-cart normal-btn outline">Add to Cart</button>
                      <button class="add-to-compare normal-btn outline">+ Add to Compare</button> -->
                    </div>
                  </div>
                </div>
                <?php } } ?>
                
              </div>
            </div>
            <div class="shop-pagination">
              <?php 
                echo $pagination;
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $("#SORT").on("change", function(){
      $("#sortForm").submit();
    });
    $("#LIMIT").on("change", function(){
      $("#sortForm").submit();
    });
  });
</script>
<!-- End shop layout-->
<?php $this->load->view("public/footer"); ?>