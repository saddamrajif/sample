<?php $this->load->view("public/header"); ?>
<div class="ogami-breadcrumb">
  <div class="container">
    <ul>
      <li> <a class="breadcrumb-link" href="<?php echo base_url(); ?>"> <i class="fas fa-home"></i>Home</a></li>
      <li> <a class="breadcrumb-link active" href="#">Lupa Password</a></li>
    </ul>
  </div>
</div>
<!-- End breadcrumb-->
<div class="account">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-6 mx-auto">
        <h1 class="title">Lupa Password</h1>
        <form id="lostPassword">
          <label for="user-name">Masukkan Email Anda *</label>
          <input class="no-round-input" id="email" name="email" type="email">
          <div class="account-method">
            <div class="account-forgot"><a href="<?php echo site_url("login"); ?>">Login</a></div>
          </div>
          <div id="lostStatus"></div>
          <div class="account-function">
            <button class="no-round-btn">Reset</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(function(){
    $("#lostPassword").submit(function( e ){
      e.preventDefault();
      $("#lostStatus").html( "<div class='alert alert-info'>Mereset Password...</div>" );
      var data = $(this).serialize();
      var target = site_url + "lostpassword/index";
      $.post(target,data,function( e ){
        $("#lostStatus").html( e );
      });
      return false;
    });
  });
</script>
<?php $this->load->view("public/footer"); ?>