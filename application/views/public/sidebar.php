<button class="no-round-btn" id="filter-sidebar--closebtn">Close sidebar</button>
<div class="shop-sidebar_department">
  <div class="department_top mini-tab-title underline">
    <h2 class="title">Kategori</h2>
  </div>
  <div class="department_bottom">
    <ul>
      <?php
      $category = $this->Mmasterpublic->getCommodityCategory();
      foreach( $category as $cat ){
        echo '<li> <a class="department-link" href="'.site_url("shop/category/" . $cat->SLUG).'">'.$cat->NAME.'</a></li>';
      } ?>
    </ul>
  </div>
</div>
<?php /* <div class="shop-sidebar_price-filter">
  <div class="price-filter_top mini-tab-title underline">
    <h2 class="title">Filter By Price</h2>
  </div>
  <div class="price-filter_bottom">
    <p>
      <label for="amount">Price range:</label>
      <div class="filter-group">
        <input id="amount" type="text" readonly="">
        <button class="normal-btn">Fiter</button>
      </div>
    </p>
    <div id="slider-range"></div>
  </div>
  </div>*/ ?>
  <?php /* <div class="shop-sidebar_color-filter">
    <div class="color-filter_top mini-tab-title underline">
      <h2 class="title">Color</h2>
    </div>
    <div class="color-filter_bottom">
      <div class="row">
        <div class="col-6">
          <div class="color">
            <div class="visible-color" style="background-color: black;"></div><a href="shop_grid+list_3col.html">Black (12)</a>
          </div>
          <div class="color">
            <div class="visible-color" style="background-color: red;"></div><a href="shop_grid+list_3col.html">Red (4)</a>
          </div>
          <div class="color">
            <div class="visible-color" style="background-color: orange;"></div><a href="shop_grid+list_3col.html">Orange (8)</a>
          </div>
        </div>
        <div class="col-6">
          <div class="color">
            <div class="visible-color" style="background-color: blue;"></div><a href="shop_grid+list_3col.html">Blue (4)</a>
          </div>
          <div class="color">
            <div class="visible-color" style="background-color: green;"></div><a href="shop_grid+list_3col.html">Green (9)</a>
          </div>
          <div class="color">
            <div class="visible-color" style="background-color: pink;"></div><a href="shop_grid+list_3col.html">Pink (12)</a>
          </div>
        </div>
      </div>
    </div>
  </div> */ ?>
  <?php /* <div class="shop-sidebar_size">
    <div class="size_top mini-tab-title underline">
      <h2 class="title">Popular size</h2>
    </div>
    <div class="size_bottom">
      <form>
        <div class="size">
          <input type="checkbox" id="large">
          <label for="large">Large</label>
        </div>
        <div class="size">
          <input type="checkbox" id="medium">
          <label for="medium">Medium</label>
        </div>
        <div class="size">
          <input type="checkbox" id="small">
          <label for="small">Small</label>
        </div>
        <div class="size">
          <input type="checkbox" id="tiny">
          <label for="tiny">Tiny</label>
        </div>
      </form>
    </div>
  </div> */ ?>
  <div class="shop-sidebar_tag">
    <div class="tag_top mini-tab-title underline">
      <h2 class="title">Label Produk</h2>
    </div>
    <div class="tag_bottom">
      <?php 
        $tags = $this->Mmasterpublic->getProductTag();
        foreach($tags as $t){
          echo "<a class='tag-btn' href='".site_url("shop/tag/" . $t->NAME)."'>".$t->NAME."</a>";
        }
      ?>
    </div>
  </div>