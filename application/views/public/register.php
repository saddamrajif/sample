<?php $this->load->view("public/header"); ?>
      <div class="ogami-breadcrumb">
        <div class="container">
          <ul>
            <li> <a class="breadcrumb-link" href="<?php echo base_url() ?>"> <i class="fas fa-home"></i>Home</a></li>
            <li> <a class="breadcrumb-link active" href="#">Register</a></li>
          </ul>
        </div>
      </div>
      <!-- End breadcrumb-->
      <div class="account">
        <div class="container">
          <div class="row">
            <div class="col-12 col-md-6 mx-auto">
              <h1 class="title">Daftar</h1>
              <form method="POST" id="registerForm">
                <label for="user-name">NIK *</label>
                <input required class="no-round-input" id="NIK" name="NIK" type="text">
                <label for="user-name">Nama *</label>
                <input required class="no-round-input" id="NAME" name="NAME" type="text">
                <label for="user-name">Masukkan Email Anda *</label>
                <input required class="no-round-input" id="EMAIL" name="EMAIL" type="text">
                <label for="user-name">No. HP *</label>
                <input required class="no-round-input" id="MOBILE" name="MOBILE" type="text">
                <label for="password">Password *</label>
                <input required class="no-round-input" id="PASSWORD" name="PASSWORD" type="password">
                <label for="confirm">Ulangi Password *</label>
                <input required class="no-round-input" id="CONFIRM" name="CONFIRM" type="password">
                <div id="registerStatus"></div>
                <div class="account-function">
                  <button class="no-round-btn">Daftar</button>
                  <a class="create-account" href="<?php echo site_url("login"); ?>">Atau login jika sudah terdaftar</a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <script type="text/javascript">
        $(function(){
          $("#registerForm").submit(function( e ){
            e.preventDefault();
            $("#registerStatus").html( "<div class='alert alert-info'>Memproses Pendaftaran...</div>" );
            var data = $(this).serialize();
            var target = site_url + "register/doRegister";
            $.post(target,data,function( e ){
              $("#registerStatus").html( e );
            });
            return false;
          });
        });
      </script>
      <!-- End account-->
      <?php $this->load->view("public/footer"); ?>