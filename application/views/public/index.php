<?php $this->load->view("public/header"); ?>
<div class="slider">
  <div class="full-fluid">
    <div class="slider_wrapper">
      <?php 
      $seq = 1;
      foreach($slider as $sl){ ?>
        <div class="slider-block" style="background-image: url('<?php echo base_url("uploads/" . $sl->BACKGROUND) ?>')">
          <div class="slider-content">
            <div class="container">
              <div class="row align-items-center justify-content-center">
                <div class="col-12 col-md-5 col-xl-6">
                  <div class="slider-text d-flex flex-column align-items-center align-items-md-start">
                    <h5 data-animation="fadeInUp" data-delay=".2s"><?php echo $sl->SMALLTEXT; ?></h5>
                    <h1 data-animation="fadeInUp" data-delay=".3s"><?php echo $sl->BIGTEXT; ?></h1>
                    <h3 data-animation="fadeInUp" data-delay=".4s"><span>mulai</span> Rp <?php echo $sl->PRICE ?></h3>
                    <a class="normal-btn" href="<?php echo $sl->BUTTONURL; ?>" data-animation="fadeInUp" data-delay=".4s"><?php echo $sl->BUTTONTEXT; ?></a>
                  </div>
                </div>
                <div class="col-12 col-md-6">
                  <div class="slider-img" data-animation="zoomIn" data-delay=".1s">
                    <img src="<?php echo base_url("assets/public/") ?>/images/homepage01/slider_subbackground_1.png" alt="">
                    <?php if($seq == 1){ echo '<div class="prallax-img">'; } ?>
                    <?php if($seq == 1){ echo '<div id="img-block">'; } ?><img class="img" src="<?php echo base_url("uploads/" . $sl->IMAGE) ?>" alt="" data-depth="1"><?php if($seq == 1){ echo '</div>'; } ?>
                    <?php if($seq == 1){ echo '</div>'; }?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php 
        $seq++;
      } ?>
    </div>

    <div class="benefit-block">
      <div class="container">
        <div class="our-benefits">
          <div class="row">
            <div class="col-12 col-md-4">
              <div class="benefit-detail d-flex flex-column align-items-center"><img class="benefit-img" src="<?php echo base_url("assets/public/") ?>/images/homepage01/benefit-icon1.png" alt="">
                <h5 class="benefit-title">Gratis Ongkir</h5>
                <p class="benefit-describle">Semua Transaksi Dilakukan COD</p>
              </div>
            </div>
            <div class="col-12 col-md-4">
              <div class="benefit-detail d-flex flex-column align-items-center"><img class="benefit-img" src="<?php echo base_url("assets/public/") ?>/images/homepage01/benefit-icon2.png" alt="">
                <h5 class="benefit-title">Waktu Fleksible</h5>
                <p class="benefit-describle">Lakukan Transaksi Kapanpun</p>
              </div>
            </div>
            <div class="col-12 col-md-4">
              <div class="benefit-detail boderless d-flex flex-column align-items-center"><img class="benefit-img" src="<?php echo base_url("assets/public/") ?>/images/homepage01/benefit-icon3.png" alt="">
                <h5 class="benefit-title">Bayar Langsung</h5>
                <p class="benefit-describle">Bayar Langsung ke Pembeli</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End slider-->
<div class="items-category">
  <div class="container">
    <div class="row">
      <?php foreach($category as $cat){ ?>
        <div class="col-12 col-sm-6 col-md-3">
          <a class="product-item d-flex flex-column align-items-center justify-content-center" href="<?php echo site_url("shop/category/" . $cat->SLUG); ?>" style="background-image: url(assets/images/homepage01/item_category_1.png)">
            <div class="categories-img">
              <img src="<?php echo base_url("uploads/" . $cat->IMAGE) ?>" alt="<?php echo $cat->NAME; ?>">
            </div>
            <h2><?php echo $cat->NAME; ?></h2>
            <p><?php echo number_format($cat->TOTPRODUCT,0,",",".") ?> Items</p>
          </a>
        </div>
      <?php } ?>
    </div>
  </div>
</div>
<!-- End items-category-->
<div class="feature-products">
  <div class="container">
    <div class="row">
      <div class="col-12 text-center">
        <h1 class="title mx-auto">Produk Unggulan</h1>
      </div>
      <div class="col-12">
        <div id="tab">
          <ul class="tab-control">
            <li><a class="active" href="#tab-all">All</a></li>
            <?php 
              foreach($category as $cat){
                echo '<li><a href="#tab-'.$cat->COMMODITYCATEGORYID.'">'.$cat->NAME.'</a></li>';
              }
            ?>
          </ul>

          <div id="tab-all">
            <div class="row no-gutters-sm">
              <?php
                if(count( $featured ) == 0){
                  echo "<div class='col-md-12'><div class='alert alert-danger'>Tidak ada produk tersedia</div></div>";
                } else {
              ?>
              <?php foreach($featured as $fe){ ?>
              <div class="col-6 col-md-4 col-lg-3">
                <div class="product"><a class="product-img" href="<?php echo site_url("shop/detail/".$fe->PRODUCTID."/".text2slug($fe->NAME)); ?>">
                  <img src="<?php echo image_check($fe->IMAGE1); ?>" alt=""></a>
                  <h5 class="product-type"><?php echo $fe->COMMODITYCATEGORY ?></h5>
                  <h3 class="product-name"><?php echo $fe->NAME ?></h3>
                  <h3 class="product-price">Rp <?php echo number_format($fe->PRICE,0,",","."); ?> 
                    <del>Rp <?php echo number_format($fe->PRICE_ST,0,",","."); ?></del>
                  </h3>
                  <div class="product-select">
                    <?php /* <button class="add-to-wishlist round-icon-btn"> <i class="icon_heart_alt"></i></button>
                    <button class="add-to-cart round-icon-btn">  <i class="icon_bag_alt"></i></button>
                    <button class="add-to-compare round-icon-btn"><i class="fas fa-random"></i></button>
                    */ ?>
                    <button class="quickview round-icon-btn"> <i class="far fa-eye"></i></button>
                  </div>
                </div>
              </div>
              <?php }} ?>
            </div>
          </div>
          
          <?php 
            foreach($category as $cat){
          ?>
          <div id="tab-<?php echo $cat->COMMODITYCATEGORYID; ?>">
            <div class="row no-gutters-sm">
              <?php 
                $CFeatured = $this->Mmasterpublic->getFeaturedProduct($cat->COMMODITYCATEGORYID);
                if(count( $CFeatured ) == 0){
                  echo "<div class='col-md-12'><div class='alert alert-danger'>Tidak ada produk tersedia</div></div>";
                } else {
                foreach($CFeatured as $fe){
              ?>
              <div class="col-6 col-md-4 col-lg-3">
                <div class="product"><a class="product-img" href="<?php echo site_url("shop/detail/".$fe->PRODUCTID."/".text2slug($fe->NAME)); ?>">
                  <img src="<?php echo image_check($fe->IMAGE1); ?>" alt=""></a>
                  <h5 class="product-type"><?php echo $fe->COMMODITYCATEGORY ?></h5>
                  <h3 class="product-name"><?php echo $fe->NAME ?></h3>
                  <h3 class="product-price">Rp <?php echo number_format($fe->PRICE,0,",","."); ?> 
                    <del>Rp <?php echo number_format($fe->PRICE_ST,0,",","."); ?></del>
                  </h3>
                  <div class="product-select">
                    <?php /* <button class="add-to-wishlist round-icon-btn"> <i class="icon_heart_alt"></i></button>
                    <button class="add-to-cart round-icon-btn">  <i class="icon_bag_alt"></i></button>
                    <button class="add-to-compare round-icon-btn"><i class="fas fa-random"></i></button>
                    */ ?>
                    <button class="quickview round-icon-btn"> <i class="far fa-eye"></i></button>
                  </div>
                </div>
              </div>
              <?php }} ?>
            </div>
          </div>
          <?php
            }
          ?>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End feature-products-->
<div class="banner"> 
  <div class="full-fluid">
    <div class="banner-block">
      <div class="row no-gutters">
        <div class="col-12 col-lg-3">
          <div class="banner-block_detail" style="background-image: url('<?php echo base_url("assets/public/") ?>/images/homepage01/banner_1.png')"><img src="<?php echo base_url("assets/public/") ?>/images/homepage01/banner_img_1.png" alt=""></div>
        </div>
        <div class="col-12 col-lg-6">
          <div class="banner-block_detail" style="background-image: url('<?php echo base_url("assets/public/") ?>/images/homepage01/banner_2.png')"><img src="<?php echo base_url("assets/public/") ?>/images/homepage01/banner_img_2.png" alt=""></div>
        </div>
        <div class="col-12 col-lg-3">
          <div class="banner-block_detail" style="background-image: url('<?php echo base_url("assets/public/") ?>/images/homepage01/banner_3.png')"><img src="<?php echo base_url("assets/public/") ?>/images/homepage01/banner_img_3.png" alt=""></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End banner	-->
<div class="deal-of-week">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-12 text-center order-1">
        <h1 class="title green-underline mx-auto">Mengapa Belanja di Go! Tani Jatim</h1>
      </div>
      <div class="col-10 col-md-6 col-lg-4 order-3 order-md-2 order-lg-2">
        <div class="row">
          <div class="col-12">
            <div class="featured-use text-md-right">
              <div class="featured-use_intro order-2 order-md-1">
                <h5>Segar Langsung Dari Petani</h5>
                <p>Buah dan sayur yang dijual adalah hasil langsung dari petani di Jawa Timur</p>
              </div>
              <div class="featured-use_icon text-md-right order-1 order-md-2 featured-use_icon-left">
                <div class="icon-detail"><img src="<?php echo base_url("assets/public/") ?>/images/homepage01/dow_icon_1.png" alt=""></div>
              </div>
            </div>
          </div>
          <div class="col-12">
            <div class="featured-use text-md-right">
              <div class="featured-use_intro order-2 order-md-1">
                <h5>Resmi Dari Pemerintah</h5>
                <p>Petani yang menjual produknya telah terverifikasi oleh Dinas Pertanian</p>
              </div>
              <div class="featured-use_icon text-md-right order-1 order-md-2 featured-use_icon-left">
                <div class="icon-detail"><img src="<?php echo base_url("assets/public/") ?>/images/homepage01/dow_icon_2.png" alt=""></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-12 col-lg-4 order-2 order-md-4 order-lg-3 text-center">
        <div class="week-product_img"><img class="img-fluid" src="<?php echo base_url("assets/public/") ?>/images/homepage01/deal-of-week.png" alt="">
          <!-- <p>Only<br><span>$19.00</span></p> -->
        </div>
      </div>
      <div class="col-10 col-md-6 col-lg-4 order-4 order-md-3 order-lg-4">
        <div class="row">
          <div class="col-12">
            <div class="featured-use">
              <div class="featured-use_intro order-2">
                <h5>Kesehatan Badan</h5>
                <p>Konsumsi buah dan sayuran segar dari petani menjaga kesehatan badan, dan mensejahterakan petani.</p>
              </div>
              <div class="featured-use_icon order-1 ml-0">
                <div class="icon-detail"><img src="<?php echo base_url("assets/public/") ?>/images/homepage01/dow_icon_3.png" alt=""></div>
              </div>
            </div>
          </div>
          <div class="col-12">
            <div class="featured-use">
              <div class="featured-use_intro order-2">
                <h5>Transaksi Aman</h5>
                <p>Pastikan transaksi dilakukan dengan bertemu langsung dengan petani, tanpa melalui pihak ketiga.</p>
              </div>
              <div class="featured-use_icon order-1 ml-0">
                <div class="icon-detail"><img src="<?php echo base_url("assets/public/") ?>/images/homepage01/dow_icon_4.png" alt=""></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- <div class="col-12 text-center order-5">
        <div class="d-flex justify-content-center" id="event-countdown"></div>
      </div> -->
    </div>
  </div>
</div>
<!-- End deak of the week-->
<?php $this->load->view("public/footer"); ?>