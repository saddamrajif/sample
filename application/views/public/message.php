<?php $this->load->view("public/header"); ?>
<div class="ogami-breadcrumb">
  <div class="container">
    <ul>
      <li> <a class="breadcrumb-link" href="<?php echo base_url(); ?>"> <i class="fas fa-home"></i>Home</a></li>
      <li> <a class="breadcrumb-link active" href="#">Notifikasi</a></li>
    </ul>
  </div>
</div>
<!-- End breadcrumb-->
<div class="account">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-6 mx-auto">
        <h1 class="title text-left"><?php echo $message ?></h1>
        <p><?php echo $description ?></p>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view("public/footer"); ?>