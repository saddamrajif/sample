<?php $this->load->view("public/header"); ?>
<div class="ogami-breadcrumb">
  <div class="container">
    <ul>
      <li> <a class="breadcrumb-link" href="<?php echo base_url(); ?>"> <i class="fas fa-home"></i>Home</a></li>
      <li> <a class="breadcrumb-link active" href="#">Login</a></li>
    </ul>
  </div>
</div>
<!-- End breadcrumb-->
<div class="account">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-6 mx-auto">
        <h1 class="title">Login</h1>
        <form id="loginForm">
          <label for="user-name">No HP atau Email *</label>
          <input class="no-round-input" id="username" name="username" type="text">
          <label for="password">Password *</label>
          <input class="no-round-input" id="password" name="password" type="password">
          <div class="account-method">
            <div class="account-forgot"><a href="<?php echo site_url("lostpassword"); ?>">Lupa Password Anda</a></div>
          </div>
          <div id="loginStatus"></div>
          <div class="account-function">
            <button class="no-round-btn">Sign in</button>
            <a class="create-account" href="<?php echo site_url("register"); ?>">Daftar Sebagai Petani</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(function(){
    $("#loginForm").submit(function( e ){
      e.preventDefault();
      $("#loginStatus").html( "<div class='alert alert-info'>Proses Login...</div>" );
      var data = $(this).serialize();
      var target = site_url + "login/doLogin";
      $.post(target,data,function( e ){
        $("#loginStatus").html( e );
      });
      return false;
    });
  });
</script>
<?php $this->load->view("public/footer"); ?>