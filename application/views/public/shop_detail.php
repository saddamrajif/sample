<?php $this->load->view("public/header"); ?>
<div class="ogami-breadcrumb">
  <div class="container">
    <ul>
      <li> <a class="breadcrumb-link" href="<?php echo site_url(); ?>"> <i class="fas fa-home"></i>Home</a></li>
      <li> <a class="breadcrumb-link" href="<?php echo site_url("shop"); ?>">Shop</a></li>
      <li> <a class="breadcrumb-link active" href="<?php echo current_url(); ?>"><?php echo $product->NAME ?></a></li>
    </ul>
  </div>
</div>
<!-- End breadcrumb-->
<div class="shop-layout">
  <div class="container">
    <div class="row">
      <div class="col-xl-3">
        <div class="shop-sidebar">
          <?php $this->load->view("public/sidebar"); ?>
        </div>
        <div class="filter-sidebar--background" style="display: none"></div>
      </div>
      <div class="col-xl-9">
        <div class="shop-detail">
          <div class="row">
            <div class="col-12 col-lg-6">
              <div class="shop-detail_img">
                <button class="round-icon-btn" id="zoom-btn"> <i class="icon_zoom-in_alt"></i></button>
                <div class="big-img">
                  <div class="big-img_block"><img src="<?php echo base_url("uploads/" . $product->IMAGE1) ?>" alt="product image"></div>
                  <?php if( $product->IMAGE2 <> "" ){ ?>
                  <div class="big-img_block"><img src="<?php echo base_url("uploads/" . $product->IMAGE2) ?>" alt="product image"></div>
                  <?php } ?>
                  <?php if( $product->IMAGE3 <> "" ){ ?>
                  <div class="big-img_block"><img src="<?php echo base_url("uploads/" . $product->IMAGE3) ?>" alt="product image"></div>
                  <?php } ?>
                </div>
                <div class="slide-img">
                  <div class="slide-img_block"><img src="<?php echo base_url("uploads/" . $product->IMAGE1) ?>" alt="product image"></div>
                  <?php if( $product->IMAGE2 <> "" ){ ?>
                  <div class="slide-img_block"><img src="<?php echo base_url("uploads/" . $product->IMAGE2) ?>" alt="product image"></div>
                  <?php } ?>
                  <?php if( $product->IMAGE3 <> "" ){ ?>
                  <div class="slide-img_block"><img src="<?php echo base_url("uploads/" . $product->IMAGE3) ?>" alt="product image"></div>
                  <?php } ?>
                </div>
              </div>
              <div class="img_control"></div>
            </div>
            <div class="col-12 col-lg-6">
              <div class="shop-detail_info">
                <h5 class="product-type color-type"><?php echo $product->COMMODITYCATEGORY; ?></h5>
                <h2 class="product-name"><?php echo $product->NAME ?></h2>
                <p class="product-describe"><?php echo get_excerpt($product->DESCRIPTION) ?> </p><a class="product-more" href="#description" onclick="$('#tab-1').click();">Selengkapnya <i class="arrow_carrot-2right"></i></a>
                <table class="table table-borderless product-describe">
                  <tr>
                    <td style="width:100px;">Dilihat</td>
                    <td style="width:2px;">:</td>
                    <td><?php echo $product->VIEWED; ?> kali</td>
                  </tr>
                  <tr>
                    <td>Dihubungi</td>
                    <td>:</td>
                    <td><?php echo $product->CONTACTED; ?> kali</td>
                  </tr>
                </table>
                <div class="price-rate">
                  <h3 class="product-price"> 
                    <del>Rp <?php echo number_format($product->PRICE_ST,0,",",".") ?></del>Rp <?php echo number_format($product->PRICE,0,",",".") ?>
                  </h3>
                  <!-- <h5 class="product-rated"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star-half"></i><span>(15)</span></h5> -->
                </div>
                <?php /*
                <!-- <div class="color-select">
                  <h5>Select Color:</h5><a class="color bg-danger" href="#"></a><a class="color bg-success" href="#"></a><a class="color bg-info" href="#"></a>
                </div> -->
                <!-- <div class="quantity-select">
                  <label for="quantity">Quatity:</label>
                  <input class="no-round-input" id="quantity" type="number" min="0" value="1">
                </div> -->
                */ ?>
                <div class="product-select">
                  <?php 
                    if(substr($product->MOBILE, 0,1) == "0"){
                      $product->MOBILE = "62" . substr($product->MOBILE, 1);
                    }
                  ?>
                  <button class="add-to-cart normal-btn outline" onclick="contacting('phone');"><i class="fa fa-phone"></i> Telepon</button>
                  <button class="add-to-compare normal-btn outline" onclick="contacting('wa');"><i class="fab fa-whatsapp"></i> Whatsapp</button>
                </div>
                <div class="product-share">
                  <h5>Share link:</h5>
                  <div class="sharethis-inline-share-buttons"></div>
                </div>
              </div>
            </div>
            <div class="col-12">
              <div class="shop-detail_more-info" id="description">
                <div id="tab-so3">
                  <ul class="mb-0">
                    <li class="active"><a href="#tab-1">DESKRIPSI</a></li>
                    <li><a href="#tab-2">TOKO / PENJUAL</a></li>
                    <li> <a href="#tab-3">KOMENTAR FB</a></li>
                  </ul>
                  <div id="tab-1">
                    <div class="description-block">
                      <div class="description-item_block">
                        <div class="row align-items-center">
                          <div class="description-item_text">
                            <?php echo $product->DESCRIPTION; ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="tab-2">
                    <div class="specifications_block">
                      <div class="specification_item">
                        <h4 class="store-title">Identitas Toko / Penjual</h4>
                        <!-- <table class="shop_attributes table-borderless"> -->
                        <table class="table table-borderless">
                          <tbody>
                            <tr>
                              <th style="width:150px">Nama</th>
                              <th style="width:2px">:</th>
                              <td><?php echo $product->STORE; ?></td>
                            </tr>
                            <tr>
                              <th>Alamat</th>
                              <th>:</th>
                              <td><?php echo $product->ADDRESS; ?></td>
                            </tr>
                            <tr>
                              <th>Kontak</th>
                              <th>:</th>
                              <td><?php echo $product->EMAIL; ?><br /><?php echo $product->MOBILE; ?></td>
                            </tr>
                            <tr>
                              <th>Kota</th>
                              <th>:</th>
                              <td><?php echo $product->CITY; ?></td>
                            </tr>
                            <tr>
                              <th>Status Toko</th>
                              <th>:</th>
                              <td><?php 
                                $status = $this->Mmasterpublic->getStoreStatus();
                                echo $status[$product->STORESTATUS]; 
                              ?></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="specification_item">
                        <h4 class="store-title">Masa Tanam / Panen</h4>
                        <table class="table table-bordered">
                          <tbody>
                            <tr>
                              <th style="width: 30px;">No</th>
                              <th>Nama</th>
                              <th>Lahan</th>
                              <th>Tgl. Tanam</th>
                              <th>Tgl. Panen</th>
                            </tr>
                            <?php
                              $seq = 1;
                              foreach($planharvestperiod as $pp){
                                echo "<tr>
                                  <td>".$seq.".</td>
                                  <td>".$pp->NAME."</td>
                                  <td>".$pp->FIELD."</td>
                                  <td>".date_to_ID($pp->PLANDATE)."</td>
                                  <td>".date_to_ID($pp->HARVESTDATE)."</td>
                                </tr>";
                                $seq++;
                              }
                            ?>
                          </tbody>
                        </table>
                      </div>
                      <div class="specification_item">
                        <h4 class="store-title">Alsintan</h4>
                        <table class="table table-bordered">
                          <tbody>
                            <tr>
                              <th style="width: 30px;">No</th>
                              <th>Nama</th>
                              <th>Jenis</th>
                              <th>Jumlah</th>
                            </tr>
                            <?php
                              $seq = 1;
                              foreach($alsintan as $al){
                                echo "<tr>
                                  <td>".$seq.".</td>
                                  <td>".$al->NAME."</td>
                                  <td>".$al->FARMERTOOLTYPE."</td>
                                  <td>".$al->QUANTITY."</td>
                                </tr>";
                                $seq++;
                              }
                            ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div id="tab-3"> 
                    <div class="customer-reviews_block">
                      <div class="fb-comments" data-href="<?php echo current_url(); ?>" data-width="100%" data-numposts="5"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  function contacting( type ){
    alert(type);
    var target = site_url + "shop/updatecontact/";
    var datapost = {
      PRODUCTID: "<?php echo $product->PRODUCTID; ?>"
    }
    $.post( target, datapost, function(e){
      if( type == "phone" ){
        window.location.href='tel:<?php echo $product->MOBILE; ?>';
      } else if( type == "wa" ){
        window.location.href='https://wa.me/<?php echo $product->MOBILE; ?>?text=Saya berminat dengan <?php echo $product->NAME ?>';
      }
    });
  }
</script>
<?php $this->load->view("public/footer"); ?>