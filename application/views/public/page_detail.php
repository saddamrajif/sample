<?php $this->load->view("public/header"); ?>
      <div class="ogami-breadcrumb">
        <div class="container">
          <ul>
            <li> <a class="breadcrumb-link" href="index.html"> <i class="fas fa-home"></i>Home</a></li>
            <li> <a class="breadcrumb-link active" href="#">Blog detail</a></li>
          </ul>
        </div>
      </div>
      <!-- End breadcrumb-->
      <div class="blog-layout">
        <div class="container">
          <div class="row">
            <div class="col-xl-3">
              <div class="blog-sidebar">
                <?php $this->load->view("public/sidebar-blog"); ?>
              </div>
              <div class="filter-sidebar--background" style="display: none"></div>
            </div>
            <div class="col-12 col-xl-9">
              <div class="blog-detail">
                <div class="row">
                  <div class="col-12">
                    <div class="blog-detail_block">
                      <h1 class="blog-title" href="<?php echo site_url("blog/detail/" . $blog->POSTID . "/" . text2slug($blog->TITLE)); ?>"><?php echo $blog->TITLE; ?></h1>
                      <?php if($blog->IMAGE != ""){ ?>
                      <div class="blog-img"><img src="<?php echo base_url( image_check($blog->IMAGE) ); ?>" alt="blog image"></div>
                      <?php } ?>
                      <div class="blog-describe">
                        <?php echo $blog->CONTENT; ?>
                      </div>
                    </div>
                    <div class="blog-detail_footer">
                      <div class="row align-items-sm-center">
                        <div class="col-12 col-sm-6 ">
                          <div class="blog-detail_share">
                            <div class="sharethis-inline-share-buttons"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <?php /*
                    <div class="blog-detail_comment">
                      <div class="fb-comments" data-href="<?php echo current_url(); ?>" data-width="100%" data-numposts="5"></div>
                    </div>
                    */ ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End blog layout-->
      <?php $this->load->view("public/footer"); ?>