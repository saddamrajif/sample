<?php 
$config = $this->Mmasterdata->getConfiguration();
$social = $this->Mmasterpublic->getSocialMedia();
$logged = $this->session->userdata("login");
$partner = $this->Mmasterpublic->getPartner();
$related = $this->Mmasterpublic->getRelatedLink();
?>
<div class="partner">
  <div class="container">
    <div class="partner_block d-flex justify-content-between" data-slick="{&quot;slidesToShow&quot;: 6}">
      <?php foreach($partner as $part){ ?>
      <div class="partner--logo" href="<?php echo $part->URL ?>"> <a target="_blank" title="<?php echo $part->NAME ?>" href="<?php echo $part->URL ?>"><img src="<?php echo base_url("uploads/" . $part->IMAGE) ?>" alt="<?php echo $part->NAME ?>"></a></div>
      <?php } ?>
    </div>
  </div>
</div>
<!-- End partner--> 
<footer>
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-4 text-sm-center text-md-left">
        <div class="footer-logo"><img class="logo-footer" src="<?php echo base_url("uploads/" . $config->APP_LOGO_FOOTER) ?>" alt=""></div>
        <div class="footer-contact">
          <p>Alamat: <?php echo $config->OFFICE_ADDRESS; ?></p>
          <p>Telp: <?php echo $config->OFFICE_PHONE; ?></p>
          <p>Email: <?php echo $config->OFFICE_EMAIL; ?></p>
        </div>
        <div class="footer-social">
          <?php foreach($social as $sc){
            echo '<a class="round-icon-btn" target="_blank" href="'.$sc->URL.'"><i class="'.$sc->ICON.'"></i> </a>';
          } ?>
         </div>
        </div>
        <div class="col-md-8">
          <div class="row">
            <div class="col-12 col-sm-4 text-sm-center text-md-left">
              <div class="footer-quicklink">
                <h5>Blog</h5>
                <?php 
                  $NCategory = $this->Mmasterpublic->getNewsCategory(); 
                  foreach($NCategory as $nc){
                    echo '<a href="'.site_url("blog/category/" . $nc->SLUG).'">'.$nc->NAME.'</a>';
                  }
                ?>
              </div>
            </div>
            <div class="col-12 col-sm-4 text-sm-center text-md-left">
              <div class="footer-quicklink">
                <h5>Halaman</h5>
                <?php 
                  $pages = $this->Mmasterpublic->getPage(); 
                  if( count($pages) > 0 ){
                    foreach($pages as $p){
                      echo '<a href="'.site_url("page/detail/" . $p->POSTID . "/" . text2slug($p->TITLE)).'">'.$p->TITLE.'</a>';
                    }
                  }
                  // echo '<a href="'.site_url("page/contact/").'">Kontak</a>';
                ?>
              </div>
            </div>
            <div class="col-12 col-sm-4 text-sm-center text-md-left">
              <div class="footer-quicklink">
                <h5>Link Terkait</h5>
                <?php foreach($related as $rel){
                    echo "<a target='_blank' href='".$rel->URL."'>".$rel->NAME."</a>";
                } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="newletter">
      <div class="container">
        <div class="row justify-content-between align-items-center">
          <div class="col-12 col-md-7">
            <div class="newletter_text text-center text-md-left">
              <h5>Begabung Dengan Info Terbaru</h5>
              <p>Dapatkan informasi dan promosi terbaru melalui email.</p>
            </div>
          </div>
          <div class="col-12 col-md-5">
            <div class="newletter_input">
              <input class="round-input" type="text" placeholder="Enter your email">
              <button>Langganan</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-credit">
      <div class="container">
        <div class="footer-creadit_block d-flex flex-column flex-md-row justify-content-start justify-content-md-between align-items-baseline align-items-md-center">
          <p class="author">Copyright © <?php echo date("Y"); ?> <?php echo $config->OFFICE_NAME ?>.</p><img class="payment-method" src="<?php echo base_url("assets/public/") ?>/images/payment.png" alt="">
        </div>
      </div>
    </div>
  </footer>
  <!-- End footer-->
</div>
<script src="<?php echo base_url("assets/public/") ?>/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url("assets/public/") ?>/js/jquery.countdown.min.js"></script>
<script src="<?php echo base_url("assets/public/") ?>/js/slick.min.js"></script>
<script src="<?php echo base_url("assets/public/") ?>/js/jquery.easing.js"></script>
<script src="<?php echo base_url("assets/public/") ?>/js/jquery.scrollUp.min.js"></script>
<script src="<?php echo base_url("assets/public/") ?>/js/jquery.zoom.min.js"></script>
<script src="<?php echo base_url("assets/public/") ?>/js/parallax.js"></script>
<script src="<?php echo base_url("assets/public/") ?>/js/jquery.fancybox.js"></script>
<script src="<?php echo base_url("assets/public/") ?>/js/numscroller-1.0.js"></script>
<script src="<?php echo base_url("assets/public/") ?>/js/vanilla-tilt.min.js"></script>
<script src="<?php echo base_url("assets/public/") ?>/js/main.js"></script>
<?php if( $this->router->fetch_class() == "welcome" ){ ?>
  <script>
    var sence = document.getElementById('img-block')
    var parallaxInstance = new Parallax(sence, {
     hoverOnly: true,
   });
 </script>
<?php } ?>
</body>
</html>