<button class="no-round-btn" id="filter-sidebar--closebtn">Close sidebar</button>
<div class="blog-sidebar_search">
  <div class="search_block">
    <form action="<?php echo site_url("blog/search"); ?>" method="post">
      <input class="no-round-input" value="<?php echo isset($query) ? $query : null ?>" name="BLOG_SEARCH" id="BLOG_SEARCH" type="text" placeholder="Cari Informasi...">
    </form>
  </div>
</div>
<div class="blog-sidebar_categories">
  <div class="categories_top mini-tab-title underline">
    <h2 class="title">Kategori</h2>
  </div>
  <div class="categories_bottom">
    <ul>
      <?php 
      $categories = $this->Mmasterpublic->getNewsCategory();
      foreach($categories as $cat){
        echo "<li> <a class='category-link' href='".site_url("blog/category/" . $cat->SLUG)."'>".$cat->NAME."</a></li>";
      }
      ?>
    </ul>
  </div>
</div>
<div class="blog-sidebar_recent-post">
  <div class="recent-post_top mini-tab-title underline">
    <h2 class="title">Berita Terkini</h2>
  </div>
  <div class="recent-post_bottom">
    <?php 
    $recent = $this->Mmasterpublic->getRecentNews();
    foreach($recent as $r){
      ?>
      <div class="mini-post_block">
        <div class="mini-post_img"><a href="<?php echo site_url("blog/detail/" . $r->POSTID . "/" . text2slug($r->TITLE)); ?>"><img src="<?php echo base_url( image_check($r->IMAGE) ) ?>" alt="blog image"></a></div>
        <div class="mini-post_text"><a href="<?php echo site_url("blog/detail/" . $r->POSTID . "/" . text2slug($r->TITLE)); ?>"><?php echo $r->TITLE; ?></a>
          <h5><?php echo date_to_ID($r->CREATEAT) ?></h5>
        </div>
      </div>
    <?php } ?>
  </div>
</div>
                <?php /* 
                <!-- <div class="blog-sidebar_tags">
                  <div class="tags_top mini-tab-title underline">
                    <h2 class="title">Search By Tags</h2>
                  </div>
                  <div class="tags_bottom"><a class="tag-btn" href="shop_grid+list_3col.html">organic</a><a class="tag-btn" href="shop_grid+list_3col.html">vegatable</a><a class="tag-btn" href="shop_grid+list_3col.html">fruits</a><a class="tag-btn" href="shop_grid+list_3col.html">fresh meat</a><a class="tag-btn" href="shop_grid+list_3col.html">fastfood</a><a class="tag-btn" href="shop_grid+list_3col.html">natural</a></div>
                </div> -->
                */ ?>