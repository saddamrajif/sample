<?php 
  $config = $this->Mmasterdata->getConfiguration();
  $social = $this->Mmasterpublic->getSocialMedia();
  $logged = $this->session->userdata("login");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title><?php echo $config->APP_NAME; ?> - <?php echo $config->OFFICE_NAME; ?></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <meta name="keywords" content="blog, business, clean, clear, cooporate, creative, design web, flat, marketing, minimal, portfolio, shop, shopping, unique">
    <meta name="author" content="<?php echo $config->OFFICE_NAME ?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/public/") ?>/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/public/") ?>/css/custom_bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/public/") ?>/css/normalize.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/public/") ?>/css/fontawesome.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/public/") ?>/css/elegant.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/public/") ?>/css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/public/") ?>/css/slick.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/public/") ?>/css/scroll.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/public/") ?>/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/public/") ?>/css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/public/") ?>/css/siedoo.css">
    <link rel="shortcut icon" href="<?php echo base_url("assets/img/") ?>/favicon.png">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript">
      var base_url = "<?php echo base_url(); ?>";
      var site_url = "<?php echo site_url(); ?>";
    </script>
    <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5cbb4c963ef35400190486f3&product=inline-share-buttons' async='async'></script>
  </head>
  <body>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.2&appId=1694620940837355&autoLogAppEvents=1"></script>
    <div id="main">
      <header> 
        <div class="header-block d-flex align-items-center">
          <div class="container">
            <div class="row">
              <div class="col-12 col-md-6">
                <div class="header-left d-flex flex-column flex-md-row align-items-center">
                  <p class="d-flex align-items-center"><a href="mailto:<?php echo $config->OFFICE_EMAIL ?>"><i class="fas fa-envelope"></i><?php echo $config->OFFICE_EMAIL ?></a></p>
                  <p class="d-flex align-items-center"><a href="tel:<?php echo $config->OFFICE_PHONE ?>"><i class="fas fa-phone"></i><?php echo $config->OFFICE_PHONE ?></a></p>
                </div>
              </div>
              <div class="col-12 col-md-6">
                <div class="header-right d-flex flex-column flex-md-row justify-content-md-end justify-content-center align-items-center">
                  <div class="social-link d-flex">
                    <?php 
                      foreach( $social as $sc){
                        echo '<a target="_blank" href="'.$sc->URL.'"><i class="'.$sc->ICON.'"></i></a>';
                      }
                    ?>
                  </div>
                  <?php if( !isset($logged['USERID']) ){ ?>
                  <div class="login d-flex">
                    <a href="<?php echo site_url("login"); ?>"><i class="fas fa-user"></i>Login / Daftar</a>
                  </div>
                  <?php } else { ?>
                  <div class="language">
                    <div class="selected-language">Hallo, <?php echo $logged['NAME'] ?><i class="arrow_carrot-down"></i>
                      <ul class="list-language">
                        <li><a href="<?php echo site_url("admin"); ?>">Akun Dashboard</a></li>
                        <li><a href="<?php echo site_url("logout"); ?>">Logout</a></li>
                      </ul>
                    </div>
                  </div>
                  <?php } ?> 
                </div>
              </div>
            </div>
          </div>
        </div>
        <nav class="navigation d-flex align-items-center">
          <div class="container">
            <div class="row">
              <div class="col-2"><a class="logo" href="<?php echo base_url(); ?>"><img class="logo-desktop" src="<?php echo base_url("uploads/" . $config->APP_LOGO_HEADER) ?>" alt="Logo Jatim"></a></div>
              <div class="col-8">
                <div class="navgition-menu d-flex align-items-center justify-content-center">
                  <ul class="mb-0">
                    <li class="toggleable"> <a class="menu-item active" href="<?php echo base_url(); ?>">Home</a></li>
                    <li class="toggleable"> <a class="menu-item" href="<?php echo site_url("shop"); ?>">Shop</a>
                      <?php /*
                      <ul class="sub-menu shop d-flex">
                        <div class="nav-column">
                          <h2>Layout shop</h2>
                          <li><a href="shop_grid+list_fullwidth.html">Shop grid fullwidth</a></li>
                          <li><a href="shop_grid+list_fullwidth.html">Shop list fullwidth</a></li>
                          <li><a href="<?php echo site_url("shop"); ?>">shop grid 3 column</a></li>
                          <li><a href="<?php echo site_url("shop"); ?>">shop list 3 column</a></li>
                        </div>
                        <div class="nav-column">
                          <h2>Detail shop</h2>
                          <li><a href="shop_detail_fullwidth.html">shop detail fullwidth</a></li>
                          <li><a href="shop_detail.html">shop detail sidebar</a></li>
                        </div>
                        <div class="nav-column">
                          <h2>Pages shop</h2>
                          <li><a href="shop_checkout.html">checkout</a></li>
                          <li><a href="shop_order_complete.html">order complete</a></li>
                          <li><a href="shop_wishlist.html">wishlist</a></li>
                          <li><a href="shop_compare.html">compare</a></li>
                          <li><a href="shop_cart.html">cart</a></li>
                        </div>
                      </ul>
                      */ ?>
                    </li>
                    <li class="toggleable"><a class="menu-item" href="<?php echo site_url("blog"); ?>">Blog</a>
                      <ul class="sub-menu">
                        <?php 
                          $category = $this->Mmasterpublic->getNewsCategory();
                          foreach($category as $cat){ ?>
                            <li><a href="<?php echo site_url("blog/category/" . $cat->SLUG); ?>"><?php echo $cat->NAME ?></a></li>
                        <?php
                          }
                        ?>
                      </ul>
                    </li>
                    <li class="toggleable"><a class="menu-item" href="<?php echo site_url("page/detail/13/tentang"); ?>">Tentang</a></li>
                  </ul>
                </div>
              </div>
              <div class="col-2">
                <!-- <div class="product-function d-flex align-items-center justify-content-end">
                  <div id="wishlist"><a class="function-icon icon_heart_alt" href=""></a></div>
                  <div id="cart"><a class="function-icon icon_bag_alt" href=""><span>$150.00</span></a></div>
                </div> -->
              </div>
            </div>
          </div>
        </nav>
        <div id="mobile-menu">
          <div class="container">
            <div class="row">
              <div class="col-3">
                <div class="mobile-menu_block d-flex align-items-center"><a class="mobile-menu--control" href="#"><i class="fas fa-bars"></i></a>
                  <div id="ogami-mobile-menu">
                    <button class="no-round-btn" id="mobile-menu--closebtn">Close menu</button>
                    <div class="mobile-menu_items">
                      <ul class="mb-0 d-flex flex-column">
                        <li class="toggleable"> <a class="menu-item active" href="<?php echo base_url(); ?>">Home</a></li>
                        <li class="toggleable"><a class="menu-item" href="<?php echo site_url("shop"); ?>">Shop</a></li>
                        <li class="toggleable"> <a class="menu-item" href="<?php echo site_url("blog"); ?>">Blog</a><span class="sub-menu--expander"><i class="icon_plus"></i></span>
                          <ul class="sub-menu">
                            <li><a href="<?php echo site_url("blog/category/informasi"); ?>">Informasi</a></li>
                            <li><a href="<?php echo site_url("blog/category/tips"); ?>">Tips</a></li>
                          </ul>
                        </li>
                        <li class="toggleable"><a class="menu-item" href="<?php echo site_url("page/tentang"); ?>">Tentang</a></li>
                      </ul>
                    </div>
                    <div class="mobile-login">
                      <h2>My account</h2>
                      <?php if( !isset($logged['USERID']) ){ ?>
                      <a href="<?php echo site_url("login"); ?>">Login / Daftar</a>
                      <?php } else { ?>
                      <a href="<?php echo site_url("admin"); ?>">Akun Dashboard</a>
                      <a href="<?php echo site_url("logout"); ?>">Logout</a>
                      <?php } ?>
                    </div>
                    <div class="mobile-social">
                      <?php foreach( $social as $sc){ ?>
                      <a href="<?php echo $sc->URL ?>"><i class="<?php echo $sc->ICON ?>"> </i></a>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="ogamin-mobile-menu_bg"></div>
                </div>
              </div>
              <div class="col-6">
                <div class="mobile-menu_logo text-center d-flex justify-content-center align-items-center"><a href="<?php echo site_url(); ?>"><img class="logo-mobile" src="<?php echo base_url("uploads/" . $config->APP_LOGO_HEADER) ?>" alt=""></a></div>
              </div>
              <div class="col-3">
                <div class="mobile-product_function d-flex align-items-center justify-content-end"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="navigation-filter"> 
          <div class="container">
            <div class="row">
              <div class="col-12 col-md-4 col-lg-4 col-xl-3 order-2 order-md-1">
                <div class="department-menu_block">
                  <div class="department-menu d-flex justify-content-between align-items-center"><i class="fas fa-bars"></i>Kategori Belanja<span><i class="arrow_carrot-down"></i></span></div>
                  <div class="department-dropdown-menu">
                    <ul>
                      <?php
                        $category = $this->Mmasterpublic->getCommodityCategory();
                        $seq = 2;
                        foreach( $category as $cat ){
                          echo '<li><a href="'.site_url("shop/category/" . $cat->SLUG).'"> <i class="icon-'.$seq.'"></i>'.$cat->NAME.'</a></li>';
                          $seq++;
                        }
                      ?>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-md-8 order-md-2">
                <div class="website-search">
                  <form name="searchForm" id="searchForm" method="POST" action="<?php echo site_url("shop/index"); ?>">
                    <div class="row no-gutters">
                      <div class="col-md-10 col-xs-10">
                        <div class="search-input">
                          <input class="no-round-input no-border" type="text" name="QUERY" id="QUERY" placeholder="Apa yang Anda Butuhkan ?">
                        </div>
                      </div>
                      <div class="col-md-2 col-xs-2">
                        <button class="no-round-btn">Cari</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
      <!-- End header-->