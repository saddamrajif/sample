<?php $this->load->view("public/header"); ?>
<div class="ogami-breadcrumb">
  <div class="container">
    <ul>
      <li> <a class="breadcrumb-link" href="<?php echo base_url(); ?>"> <i class="fas fa-home"></i>Home</a></li>
      <li> <a class="breadcrumb-link active" href="<?php echo site_url("blog"); ?>">Blog</a></li>
    </ul>
  </div>
</div>
<!-- End breadcrumb-->
<div class="blog-layout">
  <div class="container">
    <div class="row">
      <div class="col-xl-3">
        <div class="blog-sidebar">
          <?php $this->load->view("public/sidebar-blog"); ?>
        </div>
        <div class="filter-sidebar--background" style="display: none"></div>
      </div>
      <div class="col-12 col-xl-9">
        <div class="blog-list">
          <?php 
            if(isset($query)){
              echo "<h3 class='font-weight-bold mb-3'>Hasil Pencarian Untuk : '".$query."'</h3>";
            }
          ?>
          <?php 
            foreach($blogs as $blog){
          ?>
          <div class="blog-block">
            <div class="row">
              <div class="col-5">
                <div class="blog-img">
                  <a href="<?php echo site_url("blog/detail/" . $blog->POSTID . "/" . text2slug($blog->TITLE)); ?>">
                    <img src="<?php echo base_url( image_check($blog->IMAGE) ) ?>" alt="<?php echo $blog->TITLE; ?>">
                  </a>
                </div>
              </div>
              <div class="col-7">
                <div class="blog-text">
                  <div class="row-fluid">
                    <h5 class="blog-tag"><?php echo $blog->CATEGORY ?></h5>
                  </div>
                  <a class="blog-title" href="<?php echo site_url("blog/detail/" . $blog->POSTID . "/" . text2slug($blog->TITLE)); ?>"><?php echo $blog->TITLE; ?></a>
                  <div class="blog-credit">
                    <p class="credit date"><?php echo date_to_ID($blog->CREATEAT); ?></p>
                    <p class="credit author"><?php echo ($blog->AUTHOR == "" ) ? "Admin" : $blog->AUTHOR; ?></p>
                  </div>
                  <p class="blog-describe"><?php echo get_excerpt($blog->CONTENT); ?></p><a class="blog-readmore" href="<?php echo site_url("blog/detail/" . $blog->POSTID . "/" . text2slug($blog->TITLE)); ?>">Selengkapnya<span> <i class="arrow_carrot-2right"></i></span></a>
                </div>
              </div>
            </div>
          </div>
          <?php 
            }
          ?>
        </div>
        <div class="blog-pagination">
          <?php 
            echo $pagination;
          ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view("public/footer"); ?>