// CUSTOM CODE
$(function(){
  getNotif();
  $(".main-menu").click(function(){
    var targeturl = $(this).attr("href");
    if(targeturl != "#"){
      $(".main-menu").parent("li").removeClass("kt-menu__item--active");
      $("#main-content").html("<div class='text-center spin-loading'><img src='" + base_url + "assets/img/spin.gif' /></div>");
      $("#main-content").load(targeturl, function(response,status,xhr){
        if (xhr.status!="200") {
          if(xhr.status=="404"){
            $("#main-content").load(site_url + "/welcome/error404");
          } 
          else {
            var msg = "Halaman Gagal Dimuat : ";
            bootbox.alert( "Pesan : " + msg + "<br />Status : " + xhr.status + "<br />Status Text : " + xhr.statusText + "<br />Response : " + response);
          }
        }
        getNotif();
      });
      // $("#main-menu li").removeClass("active");
      $(this).parent("li").addClass("kt-menu__item--active");
      $("#kt_aside_close_btn").click();
    }
    return false;
  });
});

function loadcontent(container, page_url, toast = false) {
  $("#" + container).html("<div class='text-center spin-loading'><img src='" + base_url + "assets/img/spin.gif' /></div>");
  $("#" + container).load(page_url,function(response,status,xhr){
    if (xhr.status!="200") {
      var msg = "Halaman Gagal Dimuat : ";
      bootbox.alert({
        message: xhr.responseText,
        size: "large"
      });
      showToast('Halaman gagal dimuat',false);
    } else {
      if( toast ){
        showToast();
      }
    }
  });
  getNotif();
}

function loadmodal( page_url, type="none"){
  $("#modalContainer").load(page_url,function(response,status,xhr){
    if (xhr.status!="200") {
      var msg = "Halaman Gagal Dimuat : ";
      bootbox.alert({
        message: xhr.responseText,
        size: "large"
      });
      showToast('Halaman gagal dimuat',false);
    }
  });
  if( type == "print" ){
    $("#modalFooter #btn-print").show();
  }
  if( type == "save" ){
    $("#modalFooter #btn-save").show();
  }
  $("#engineModal").modal("show")
}

function loadpost(container, page_url, datapost) {
  $("#" + container).html("<div class='text-center spin-loading'><img src='" + base_url + "assets/img/spin.gif' /></div>");
  $.post(page_url, datapost, function (e) {
    $("#" + container).html(e, function(response,status,xhr){
      if (xhr.status!="200") {
        var msg = "Halaman Gagal Dimuat : ";
        $("#" + container).html( response );
        showToast('Halaman gagal dimuat',false);
      }
      else {
        showToast();
      }
    });
  }).fail(function(err){
    bootbox.alert({
      message: err.responseText,
      size: "large"
    });
  });
}

function print(container) {
  $("#" + container).printArea({
    "popup" : "close"
  });
}

function deletedata(container, page_url) {
  swal.fire({
      title: "Konfirmasi",
      text: "Apakah Anda akan menghapus Data ini ?",
      type: "warning",
      showCancelButton: !0,
      confirmButtonText: "Ya",
      cancelButtonText: "Tidak",
      reverseButtons: !0
  }).then(function(e) {
    if(e.value) {
        $("#" + container).html("<div class='text-center spin-loading'><img src='" + base_url + "assets/img/spin.gif' /></div>");
        $("#" + container).load(page_url);
        swal.fire({
          type: "success",
          title: "Data berhasil dihapus",
          showConfirmButton: !1,
          timer: 1500
        });
      } 
      else {
        e.dismiss;
      }
  });
}

function printall(container, page_url, datapost){
  $("#" + container).html("<div class='text-center spin-loading'><img src='" + base_url + "assets/img/spin.gif' /></div>");
  var myData = "";
  $.each(datapost, function(){
    if( $(this).prop("checked") == true ){
      var id = $(this).attr("id");
      myData += id+"=true&";
    }
  });
  $.post(page_url, myData, function (e) {
    $("#" + container).html(e, function(response,status,xhr){
      if (xhr.status!="200") {
        var msg = "Halaman Gagal Dimuat : ";
        $("#" + container).html( response );
        showToast('Halaman gagal dimuat',false);
      }
      else {
        showToast();
      }
    });
  }).fail(function(err){
    bootbox.alert({
      message: err.responseText,
      size: "large"
    });
  });
}

function updateall(container, page_url, datapost){
  $("#" + container).html("<div class='text-center spin-loading'><img src='" + base_url + "assets/img/spin.gif' /></div>");
  var myData = "";
  $.each(datapost, function(){
    if( $(this).prop("checked") == true ){
      var id = $(this).attr("id");
      myData += id+"=true&";
    }
  });
  $.post(page_url, myData, function (e) {
    $("#" + container).html(e, function(response,status,xhr){
      if (xhr.status!="200") {
        var msg = "Halaman Gagal Dimuat : ";
        $("#" + container).html( response );
        showToast('Halaman gagal dimuat',false);
      }
      else {
        showToast();
      }
    });
  }).fail(function(err){
    bootbox.alert({
      message: err.responseText,
      size: "large"
    });
  });
}

function deleteall(container, page_url, datapost) {
  swal.fire({
      title: "Konfirmasi",
      text: "Anda akan menghapus SEMUA DATA TERPILIH ?",
      type: "warning",
      showCancelButton: !0,
      confirmButtonText: "Ya",
      cancelButtonText: "Tidak",
      reverseButtons: !0
  }).then(function(e) {
    if(e.value) {
      $("#" + container).html("<div class='text-center spin-loading'><img src='" + base_url + "assets/img/spin.gif' /></div>");
        var myData = "";
        $.each(datapost, function(){
          if( $(this).prop("checked") == true ){
            var id = $(this).attr("id");
            myData += id+"=true&";
          }
        });
        $.post(page_url, myData, function (e) {
          $("#" + container).html(e, function(response,status,xhr){
            if (xhr.status!="200") {
              var msg = "Halaman Gagal Dimuat : ";
              $("#" + container).html( response );
              showToast('Halaman gagal dimuat',false);
            }
            else {
              showToast();
            }
          });
        }).fail(function(err){
          bootbox.alert({
            message: err.responseText,
            size: "large"
          });
        });
        swal.fire({
          type: "success",
          title: "Data berhasil dihapus",
          showConfirmButton: !1,
          timer: 1500
        });
      } else {
        e.dismiss;
      }
  });
}

function opensearch(){
  $(".searchpanel").slideToggle("fast");
}

function showToast(message, success = true){
  toastr.options = {
    "debug": false,
    "newestOnTop": false,
    "positionClass": "toast-bottom-right",
    "closeButton": true,
    "progressBar": true
  };
  if( success ){
    toastr.success(message, 'Sukses');
  } else {
    toastr.error(message, 'Error');
  }
}

function formatInputNumber(){
  webshims.setOptions("forms-ext", {
      replaceUI: "auto",
      types: "number"
  });
  webshims.polyfill("forms forms-ext");
}

function getNotif(){
  // CallBack
  var target = site_url + "admin/welcome/getNotif";
  var datapost = {};
  $.post(target, datapost, function(e){
    $.each(e, function(k, v){
      $("#" + k).html( v );
      if( k != "pendingNotif" ){
        $("#" + k).addClass("kt-badge");
        $("#" + k).addClass("kt-badge--pill");
        $("#" + k).addClass("kt-badge--inline");
        $("#" + k).addClass("kt-badge--danger");
      }
    });
  },"json");
}
